#!/bin/sh

# Get the number for the new issue
var=$(find content/posts/ -maxdepth 3 -mindepth 3 | tail -n 1)
current=${var#*-}
number=$(($current+1))

printf "\nGenerate TWIG #$number ..."

# Build path of the post
year=$(date +"%Y")
month=$(date +"%m")
post=twig-$number
path="./content/posts/$year/$month/$post"

printf "\n\nCreate directory: $path"
mkdir -p $path

markdown=$path/index.md
printf "\nCreate index.md:  $markdown"
touch $markdown

printf "\n\nEnter issue directory..."
cd $path

printf "\n\nPaste the Hebbot curl command here to download all media files, and close the shell again with 'exit'."
printf "\nYou can preview the issue by running 'hugo server -D'.\n\n"

# Enter new shell inside the issue directory
exec bash