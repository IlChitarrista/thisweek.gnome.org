---
title: "#121 Public Interest Infrastructure"
author: Felix
date: 2023-11-10
tags: ["footage", "fractal", "tagger", "gtk-rs", "impression"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from November 03 to November 10.<!--more-->

# Sovereign Tech Fund

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> We are very happy to share that [Sovereign Tech Fund](https://sovereigntechfund.de/en/) is investing €1M into GNOME.
> 
> It will fund the following projects until the end of 2024:
> 
> * Improve the current state of accessibility
> * Design and prototype a new accessibility stack
> * Encrypt user home directories individually
> * Modernize secrets storage
> * Increase the range and quality of hardware support
> * Invest in Quality Assurance and Developer Experience
> * Expand and broaden freedesktop APIs
> * Consolidate and improve platform components
> 
> Among other things, expect contributions to libsecret, oo7, Seahorse, systemd, Linux, Atspi, Orca, WebKitGTK, Shell, Mutter, Flatpak, AccountService, Settings, gdm, initial setup, Gtk, GLib, Online Accounts and language bindings.
> 
> The team, known as "GNOME STF" started on the 2nd of October and already made exciting and impactful changes.
> We look forward sharing more information and our work with everyone.
> 
> See the official announcement on the GNOME website https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/

# GNOME Circle Apps and Libraries

### Impression [↗](https://apps.gnome.org/Impression)

Create bootable drives.

[Khaleel Al-Adhami](https://matrix.to/#/@adhami:matrix.org) reports

> Impression v3 was released! With this major version, we bring a new feature: Internet Download! Just choose your favorite distro and your device, then let it do the magic! Thanks to Brage Fuglseth  for the wonderful design! You can get it from [Flathub](https://flathub.org/apps/io.gitlab.adhami3310.Impression)
> ![](SAUiAFKySyoHTciPxfPZkXkj.png)

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Julian 🍃](https://matrix.to/#/@julianhofer:gnome.org) says

> I've updated the [gtk-rs book](https://gtk-rs.org/gtk4-rs/stable/latest/book/todo_4.html) to:
> * use async dialog API of libadwaita 1.3
> * replace the `Leaflet` with `NavigationSplitView` of libadwaita 1.4
> {{< video src="a6cf1e3af3344313db5e5061d5904ad4f85b356f1722989388897452032.webm" >}}

# Third Party Projects

[ranfdev](https://matrix.to/#/@ranfdev:matrix.org) says

> I'm announcing Notify, a native client for ntfy.sh. Notify will help you get notifications from your DIY projects. Have you built a smart doorbell🚪? Send a POST request to ntfy.sh, and get a notification on your desktop or smartphone every time someone is at your door! Add buttons to your notifications to execute an action as soon as you are notified. There are already hundreds of projects integrating with ntfy.sh: monitoring solutions 👀, download managers, backup managers, fediverse clients... Start receiving notifications from them! Or just wire up your microcontroller to send you a notification everytime your tomato plant is thirsty 🍅.
> Download Notify from [flathub](<https://flathub.org/apps/details/com.ranfdev.Notify>)
> ![](eQvPPVguIsNyebSXbciRwzFu.png)
> ![](dMKxDSnLPDVMeBIgQCmZByNZ.png)

[Felipe Kinoshita](https://matrix.to/#/@fkinoshita:gnome.org) reports

> Kana is now available on [Flathub](https://flathub.org/apps/com.felipekinoshita.Kana)!
> 
> It lets you quickly practice Hiragana and Katakana recognition, all characters also include audio files so you can learn what they sound like as well!
> 
> And thanks to Brage Fuglseth for the awesome icon!
> ![](47acb6fc926752d91e41165175f18ba8cd803ef31721475558124027904.png)
> ![](8f97dd648c0dd29595685a90e7e5b72eb66f01e11721475565430505472.png)

### Tagger [↗](https://flathub.org/apps/details/org.nickvision.tagger)

Tag your music.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Tagger is now at [V2023.11.2](https://github.com/NickvisionApps/Tagger/releases/2023.11.2)! This week Tagger saw many fixes and improvements making your tagging experience better :)
> 
> Here's the full changelog:
> 
> * Added the ability to specify ""/"" in a Tag to File Name format string to move files to a new directory when renaming files
> * Tagger now has the ability to fix corrupted files right from within the app
> * Tagger will now display files with corrupted album art as corrupted files
> * Fixed an issue where specifying the directory separator in Tag to File Name when Limit Filename Characters was enabled caused new directories to not be made
> * Fixed an issue where some custom properties for vorbis and wav files could not be removed
> * Updated translations (Thanks everyone on Weblate!)
> ![](aoSTsswfvhYNdKciJPidHKNL.png)

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) announces

> Fractal 5.rc1 is out!
> 
> Fractal 5.rc1 is the first release candidate since the rewrite of Fractal to take advantage of GTK 4 and the Matrix Rust SDK, an effort that started in March 2021.
> 
> The most notable changes since Fractal 5.beta2, that was released 2 months ago, are:
> 
> * An awesome new look thanks to libadwaita 1.4
> * Read receipts tracking has been largely improved thanks to some upstream work in the Matrix Rust SDK
> * The same upstream work allows to have much better tracking of the activity in the rooms list
> * The full lists of read receipts and reactions on messages can be perused in popovers
> * Destructive actions like removing a message or leaving a room now ask for confirmation
> * The most noticeable performance issues and memory leaks were fixed to make Fractal run as smoothly as ever
> 
> This list is far from complete and hides more enhancements, including bug fixes and new translations thanks to all our contributors, and our upstream projects.
> 
> It is available to install via Flathub Beta, see the [instructions in our README](https://gitlab.gnome.org/GNOME/fractal#beta-version).
> 
> As the version implies, if we don't find any major bug in the next 2 weeks, our next release should be the long-awaited Fractal 5 stable version!
> 
> In the meantime, if you want to fix bugs, implement new features, or any other kind of contribution, you can get inspired by taking a look at our [issues tracker](https://gitlab.gnome.org/GNOME/fractal/-/issues) on GitLab. Any help is greatly appreciated!
> ![](ac2da7ce9b911831f2aa334b0cf64966739303d61722582086814007296.png)

### Footage [↗](https://gitlab.com/adhami3310/Footage)

Polish your videos.

[Khaleel Al-Adhami](https://matrix.to/#/@adhami:matrix.org) announces

> Footage has received a refresh with v1.3.0. This version brings multiple fixes for a variety of bugs and random crashes. It also updates its runtime to GNOME 45! Get it from [Flathub](https://flathub.org/apps/io.gitlab.adhami3310.Footage).

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

