---
title: "#122 Experimenting and Learning"
author: Felix
date: 2023-11-17
tags: ["gir.core", "workbench", "denaro", "parabolic", "fragments"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from November 10 to November 17.<!--more-->

# GNOME Circle Apps and Libraries

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> Workbench is a code playground and Library to learn, experiment and prototype with GNOME development and technologies. [Download on Flathub](https://flathub.org/apps/re.sonny.Workbench)
> 
> The highlights of the new 45.3 update are
> 
> **Python support!** Thanks to [Marco Köpcke (Capypara)](https://guild.pmdcollab.org/@capypara)
> It comes with a whopping 52 Library entries already ported to Python thanks to [Gregor Niehl (gregorni)](https://fosstodon.org/@gregorni) and  Urtsi Santsi.
> 
> **Blueprint formatting** is now supported and makes working with UI code much nicer, no more manual indentation fixes. Thanks to  [Gregor Niehl (gregorni)](https://fosstodon.org/@gregorni) and [James Westman (flyingpimonster)](https://fosstodon.org/@flyingpimonster) for the great work on Blueprint.
> 
> The **Library received great QoL improvements**. It will be faster to launch and Workbench won't quit if the Library is still open.
> It now shows which language are supported for each demo and allows you to open a demo in a specific language directly. Thanks to [Diego Iván](https://mastodon.social/@dimmednerd)
> 
> The **offline documentation viewer** graduated into a standalone application named **Biblioteca** and received a bunch of improvements.
> You can [download Biblioteca](https://flathub.org/apps/app.drey.Biblioteca) on Flathub. Thanks [Akshay Warrier](https://floss.social/@akshaywarrier) for this great addition to the GNOME development toolbox.
> 
> But also:
> 
> * Each Workbench window now has its own title
> * JavaScript linter now complains on unused or undeclared variables
> * 13 Library entries ported to Vala
> * 2 Library entries ported to Rust
> * 8 Library entries were improved
> 
> Newcomers are very welcome to join our chatroom [#workbench:gnome.org](https://matrix.to/#/#workbench:gnome.org) and get help to port a Library entry. It's fun and a great opportunity to learn about GNOME development, a new programming language or even as first programming experience.
> 
> Last but not least, Tobias Bernard is organizing a Local-First workshop next Monday in Berlin where Workbench will be used together with the [p2panda](https://p2panda.org/) Rust SDK. [See the announcement](https://hedgedoc.gnome.org/gnome-p2panda-workshop#).
> ![](be5c948482b6669948a4e8923b6797405938a7d11725224152165515264.png)
> {{< video src="936a93c4f9076a06815ed4280bb76cd90a50831a1725224127167463424.webm" >}}

### Fragments [↗](https://gitlab.gnome.org/World/Fragments)

Easy to use BitTorrent client.

[Felix](https://matrix.to/#/@felix:haecker.io) announces

> Fragments now automatically detects [metered networks](https://gitlab.gnome.org/World/Fragments/-/merge_requests/156), and stops down/uploading data. Thanks Philip Withnall for the initial implementation!
> ![](PTlFMXJnFbnleqTLuovRJMMK.png)

# Third Party Projects

[ranfdev](https://matrix.to/#/@ranfdev:matrix.org) announces

> [Notify](https://flathub.org/apps/details/com.ranfdev.Notify) received various improvements for users which are self hosting ntfy.sh. Support for basic http authentication has been added, so that you can subscribe to private topics and prevent bad actors from trying to snoop your notifications.

[tfuxu](https://matrix.to/#/@tfuxu:matrix.org) says

> [Halftone](https://github.com/tfuxu/Halftone) has received this week a new, refreshed UI based on the new libadwaita 1.4 widgets, as well as a couple of bugfixes and new translations. As always, you can download it from [Flathub](https://flathub.org/apps/io.github.tfuxu.Halftone), or [check](https://github.com/tfuxu/Halftone#how-to-install-halftone) the other installation methods if you don't use Flapaks.
> ![](ZTGjvghIqcGsgNJCQUIyQvwb.png)

[paddis 🌻🐢](https://matrix.to/#/@turtle:turtle.garden) announces

> This week I released a new version of Jellybean! It introduces many new helpful features, such as assigning icons to items! You can get Jellybean [from Flathub](https://flathub.org/apps/garden.turtle.Jellybean).
> ![](bypnRvZ92eLmPGgYBHOwxz46irYLqgXz.png)

[Diego Povliuk](https://matrix.to/#/@diegopvlk:mozilla.org) reports

> Dosage 1.2.0 is out with fixes and a new preference for notification sound - [Download on Flathub](https://flathub.org/apps/io.github.diegopvlk.Dosage)
> ![](6d99793a54f0143a5e66b89bcc1b7b62d0ce1adc1724889085291528192.png)

[Akshay Warrier](https://matrix.to/#/@akshaywarrier:matrix.org) says

> Announcing the first release of Biblioteca 🎉
> 
> [Download on Flathub](https://flathub.org/apps/app.drey.Biblioteca)
> 
> Biblioteca is a documentation viewer for GNOME.
> It includes documentation from the GNOME SDK as well as VTE, libportal, libspelling and libshumate.
> Only gi-docgen documentation is supported at the moment.
> 
> We are planning to add support for other sources and formats in the future.
> 
> Happy hacking!
> ![](EcQDCeygnpnKinPxClICegIa.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Parabolic [V2023.11.0](https://github.com/NickvisionApps/Parabolic/releases/tag/2023.11.0) is here! This update is HUGE, read about all the changes below :)
> 
> Here's the changelog:
> * Parabolic is now available for Windows using Windows App SDK and WinUI 3
> * Added support for auto-generated subtitles from English
> * Added the ability to turn off downloading auto-generated subtitles
> * Added the advanced option to prefer the adv1 codec for video downloads
> * Added the "Best" resolution when downloading videos to allow Parabolic to pick the highest resolution for each video download
> * A URL can now be passed to Parabolic via the command-line or the freedesktop application open protocol to trigger its validation of startup
> * Improved the design of the Preferences dialog to allow for better searching of options
> * The shell notification when a download completes now contains an "Open File" button to open the download directly
> * Fixed an issue where aria's max connections per server preference was allowed to be greater than 16
> * Fixed an issue where enabling the "Download Specific Timeframe" advanced option would cause a crash for certain media downloads
> * Fixed an issue where stopping all downloads would cause the app to crash
> * Fixed an issue where some videos were not validated correctly
> * Updated to GNOME 45 runtime with latest libadwaita design
> * Updated to .NET 8.0
> * Updated translations (Thanks everyone on Weblate!)
> ![](aWjoQjPXuBVgKricODYDRtQg.png)

### Gir.Core [↗](https://gircore.github.io/)

Gir.Core is a project which aims to provide C# bindings for different GObject based libraries.

[badcel](https://matrix.to/#/@badcel:matrix.org) reports

> [Gir.Core](https://github.com/gircore/gir.core) 0.5.0-preview.3 got released. It is the next step to the upcoming 0.5.0 release and updates the bindings to GNOME  SDK version 45. See [the release notes](https://github.com/gircore/gir.core/releases/tag/0.5.0-preview.3) for further details.

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

Manage your personal finances.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Denaro [V2023.11.0](https://github.com/NickvisionApps/Denaro/releases/tag/2023.11.0) is here!
> 
> Here's the changelog:
> * Disallowed whitespace-only group and account names
> * Fixed an issue where exported PDF values were incorrect
> * Fixed an issue where some system cultures were not read properly
> * Fixed an issue where scrolling the sidebar with the mouse over the calendar would scroll the calendar instead
> * Fixed an issue where leading or trailing spaces in group/account names aren't discarded
> * Updated to GNOME 45 runtime with latest libadwaita design
> * Updated and added translations (Thanks to everyone on Weblate)!
> ![](bqniAWafsAaJgmfbsLRbDhpt.png)

# Shell Extensions

[Aryan Kaushik](https://matrix.to/#/@lucifer_rekt:matrix.org) reports

> GNOME Extensions now supports a new metadata key - "version-name", which gives extension developers more control over their Extension versioning.
> For more details visit the [GJS Guide](https://gjs.guide/extensions/overview/anatomy.html#version-name).

[oae](https://matrix.to/#/@oae:matrix.org) reports

> Pano - Clipboard Manager is updated with new features, bug fixes and Gnome 45 support
> 
> * added support for Gnome 45 (A big shout-out to @Totto16 for his fantastic job in bringing the extension over to Gnome 45!)
> * added wiggle animation to indicator. When you copy something indicator will wiggle
> * added font customization for search and title,
> * added window position settings. You can now put Pano at the top, bottom, left, right.
> * You can checkout the Pano on [GitHub](https://github.com/oae/gnome-shell-pano)
> ![](kSzHCqijAnyraKcqAKzJHEOF.png)

# Miscellaneous

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) announces

> We are one step closer to making openQA testing infrastructure available to all GNOME apps, using GNOME OS as a base. Catch up on the latest developments in this [status update](https://discourse.gnome.org/t/openqa-testing-update-2023-11-edition/18078/2).
> ![](KznpSKkMvgMCtXxupFRQaTEZ.png)

# Events

[Kristi Progri](https://matrix.to/#/@kristiprogri:gnome.org) announces

> GNOME Asia is right around the corner and we all ready to kick off another succesful edition. The Local team is organizing the day trip excursion and we are finishing up last minute preparations before everything starts. If you would be interested to know more here's the website: https://events.gnome.org/event/170/
> 
> This week we resumed the DEI meetings. If you would be interested to join please check the discourse for more information or join the engagement team for more details. We are looking for volunteers to help us with meeting notes and other logistic tasks such as adding information on gitlab and create new tickets.
> The next meeting would be on Dec 11th and the meeting link is available here : https://meet.gnome.org/dee-9kq-sfm-nof 
> You can find the meeting notes and more about the structure of the meeting here: https://discourse.gnome.org/t/diversity-and-inclusion-meeting-announcement/17936

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

