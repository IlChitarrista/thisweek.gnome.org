---
title: "#104 Full Text Search"
author: Chris 🌱️
date: 2023-07-14
tags: ["gnome-network-displays", "parabolic", "tracker", "tagger", "cartridges", "iplan", "gtk", "webfont-kit-generator", "flare", "workbench"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from July 07 to July 14.<!--more-->

# GNOME Core Apps and Libraries



### Tracker [↗](https://gitlab.gnome.org/GNOME/tracker/)

A filesystem indexer, metadata storage system and search tool.

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) announces

> The Tracker search engine has a new full text search algorithm arriving in GNOME 45, thanks to Carlos Garnacho.  See https://gitlab.gnome.org/GNOME/tracker/-/merge_requests/611 for more details.

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> Call for volunteers: if you are experienced in maintaining a GitLab CI runner on macOS, and you want to contribute to building and testing GLib and GTK on that platform, please join the GNOME Infrastructure team channel to help maintaining the macOS GitLab CI runner provided by the GNOME Foundation. If nobody volunteers, the CI runner will be retired in September. More details are available [on Discourse](https://discourse.gnome.org/t/potential-retirement-of-the-macos-ci-builder-for-glib-and-gtk/16198).

# GNOME Development Tools

[hergertme](https://matrix.to/#/@hergertme:gnome.org) reports

> In an attempt to ease porting of applications to GTK 4, I've released libspelling which provides inline spellchecking for GTK 4. It is extracted from the spellcheck engine I wrote for GNOME Text Editor and Builder. You can grab a copy from https://gitlab.gnome.org/chergert/libspelling
> ![](0ff774aa5064fb66f1526f1f407173faf46aa5d7.png)

# GNOME Circle Apps and Libraries



### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[jcwasmx86](https://matrix.to/#/@jcwasmx86:matrix.org) reports

> Workbench now supports [GTKCssLanguageServer, a language server providing code intelligence for the GTK CSS flavor](https://github.com/JCWasmx86/GTKCssLanguageServer). This moves the CSS code intelligence out-of-process and allows reusing the
> functionality in other editors. It's planned to include it in GNOME Builder later, when it is a bit more mature. Currently, the language server only provides diagnostics in Workbench, but documentation on hover and auto-completion will follow as soon as it adds support for those features.
> {{< video src="aCPXpVwiERtPPcsjJwfYcfEr.webm" >}}
> {{< video src="epKYUTiLvdrQidsuOdCSowdG.webm" >}}

### Webfont Kit Generator [↗](https://github.com/rafaelmardojai/webfont-kit-generator)

Create @font-face kits easily.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) says

> Webfont Kit Generator has received a small UI refresh with the new widgets from libadwaita. It now has a utility panel for the output options, allowing you to set everything in one view.
> ![](pmGjWOvArLzBZGfLVgEOnrHG.png)

### Cartridges [↗](https://github.com/kra-mo/cartridges)

Launch all your games

[kramo](https://matrix.to/#/@kramo:matrix.org) reports

> Thanks to Libadwaita 1.4, Cartridges has gained a new sidebar allowing you to filter games by source. It will also adapt to smaller screen sizes better thanks to AdwBreakpoint.
> 
> These features will land alongside GNOME 45 in the fall.
> ![](EjLgNIftrjyotPpNeJdFxWYm.png)
> ![](hVOxvJvieLfHZDcjOuewLXjt.png)

# Third Party Projects



### Tagger [↗](https://flathub.org/apps/details/org.nickvision.tagger)

Tag your music.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Tagger is now at [V2023.7.1-beta2](https://github.com/NickvisionApps/Tagger/releases/tag/2023.7.1-beta2) ! 
> V2023.7.1 is shaping up to be a huge release with new features, more customization options, better performance, many bug fixes, and an improved user interface.
> 
> Here's the full changelog so far:
> 
> * Added support for the following tag properties: BPM, Composer, Description, Publisher, ISRC
> * Added help documentation with yelp-tools, accessible from the Help menu action
> * Added more file sorting options
> * Improved album art design and added support for managing back cover art and exporting album art
> * Fixed an issue where corrupted music files would crash the app. Tagger will now display a dialog to warn the user of corrupted files
> * Improved UI
> * Updated translations (Thanks everyone on Weblate!)
> ![](sWyycQKmKJrZHVPhTnHttFyc.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Parabolic [V2023.7.2](https://github.com/NickvisionApps/Parabolic/releases/tag/2023.7.2) is here!
> 
> Here's the full changelog:
> * Added the ability to select a download's audio language if more than one are available
> * Added the option to allow for only Windows-supported characters in filenames
> * Updated translations (Thanks everyone on Weblate!)
> ![](eImjztdDKEHICHrwfFVzATVH.png)

### IPlan [↗](https://github.com/iman-salmani/iplan)

Your plan for improving personal life and workflow

[Iman Salmani](https://matrix.to/#/@imansalmani:matrix.org) announces

> [IPlan 1.9.0](https://github.com/iman-salmani/iplan) is now out!
> What's changed:
> * Search button and Primary menu moved to the sidebar
> * Navigation in the calendar by scroll
> * Calendar Navigation buttons removed
> * Now task changes apply to other parts of UI too. (instead of resetting them)
> * Clickable URL Links in the task description
> * Ctrl + w shortcut for closing window
> * Code refactoring, Bug fixes, and UI improvements
> 
> You can get it from [Flathub](https://flathub.org/apps/ir.imansalmani.IPlan)
> ![](hFZWWAApmCQJIknOriWPLjSt.png)

### GNOME Network Displays [↗](https://gitlab.gnome.org/GNOME/gnome-network-displays)

Stream the desktop to Wi-Fi Display capable devices

[Pedro Sader Azevedo](https://matrix.to/#/@toluene:matrix.org) announces

> Discovery of Chromecast and MICE devices was backported to the work-in-progress headless build of GNOME Network Displays.

### Flare [↗](https://flathub.org/apps/details/de.schmidhuberj.Flare)

An unofficial Signal GTK client.

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) announces

> Flare 0.9.0 was released, which included a lot of overall UI improvements, but also some new functionality. I will just list the most important improvements, a list of all improvements can be seen in [the changelog](https://gitlab.com/Schmiddiii/flare/-/blob/master/CHANGELOG.md#090-2023-07-10):
> 
> * Flare now supports receiving and playing voice messages.
> * The UI of the messages got drastically improved. The message bubbles now also support theming with GNOME accent colors.
> * The dropdown menu on messages got improved. Furthermore, the drop-down menu is now opened by right-click or (when using touch) holding the message instead of just a normal click.
> * Updated to GTK 4.10 including updating from deprecated widgets to their new counterperts.
> * Flare now supports receiving mentions (implemented in 0.8.2).
> 
> I want to thank [@Marc0x](https://gitlab.com/Marc0x) for doing most of the work on this release and bringing the UI of Flare to a new level.
> ![](gBRAjCSsMpmBGesOXCLnYWUP.png)

# GNOME Foundation

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) reports

> This week you can find the Foundation at [FOSSY]([https://2023.fossy.us/), a community-oriented conference hosted by the [Software Freedom Conservancy](https://sfconservancy.org/)! If you’re attending in Portland, make sure to stop by the GNOME booth to grab some stickers or pick up a t-shirt.
> 
> We’ve also been hard at work coordinating all the final details for GUADEC 2023! The conference starts in 12 days and that means we’ve got banners, signage, name badges, lanyards, t-shirts, and more to prepare. If you’re attending remotely or in person remember to [register ahead](https://events.gnome.org/event/101/). This helps us know how many people to expect and lets us have time to print your name on your badge. 
> 
> We’re still looking for GUADEC 2023 sponsors! If you or your company would like to sponsor this year’s conference, take a look at our [sponsorship brochure](https://www.gnome.org/wp-content/uploads/2023/02/SponsorBrochure-2023GUADEC.pdf) and reach out to [mwu@gnome.org](mailto:mwu@gnome.org) for more information.
> 
> Volunteer Opportunity:
> We’re live-streaming the GUADEC talks, but are looking for a volunteer to edit the final footage into individual talk videos. If that sounds like a project you’re interested in let us know by emailing [chenriksen@gnome.org](mailto:chenriksen@gnome.org).

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
