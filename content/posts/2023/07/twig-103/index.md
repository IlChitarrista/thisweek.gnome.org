---
title: "#103 Flowing Information"
author: Felix
date: 2023-07-07
tags: ["parabolic", "iplan", "phosh", "cartridges", "fretboard", "gaphor"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from June 30 to July 07.<!--more-->

# GNOME Circle Apps and Libraries

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) says

> This week Dan Yeaw  released Gaphor 2.19.1. The simple modeling tool has received a lot of small improvements and bug fixes. Parts of the UI have been updated to use new GTK 4 elements. Information Flow for Associations has been added, and Object Flows can now connect to Join/Fork Nodes. Get your copy from [Gaphor's download page](https://gaphor.org/download/) or directly from [Flathub](https://flathub.org/apps/org.gaphor.Gaphor).
> ![](enLfMUqOGmufbNryQfpZLBWu.png)

### Cartridges [↗](https://github.com/kra-mo/cartridges)

Launch all your games

[kramo](https://matrix.to/#/@kramo:matrix.org) announces

> After months of work, Cartridges 2.0 is finally here.
> 
> Two new import sources have been added: Legendary and Flatpak.
> 
> Alongside several UX improvements, the import backed has been completely rewritten, huge thanks to [Geoffrey Coulaud](https://github.com/GeoffreyCoulaud/). This allows for faster imports, much better error handling and makes adding new sources much easier, so a lot more are coming soon!
> 
> Check it out on [Flathub](https://flathub.org/apps/hu.kramo.Cartridges)!
> ![](kOSlceGXEkXPjbLZwpjgqfvm.png)

# Third Party Projects

[Swapnil Devesh](https://matrix.to/#/@sidevesh:matrix.org) reports

> This week I released Luminance, an app that lets you change brightness of displays including external ones that support DDC/CI (almost all non ancient displays support it). The app is intentionally really simple, and integrates very well with GNOME desktop. It also has a CLI interface for adjusting brightness from scripts or keyboard shortcuts, its available to install for Arch based distros in AUR and deb and rpm packages are available on GitHub releases. Would be looking at porting to GTK4 next, would have loved to make it available as a Flatpak but don't think that would be possible due to the app needing permissions to access i2c and backlight devices.
> ![](pMFGKyKLUIRrYsqfQAMPSced.png)

[Vlad Krupinski](https://matrix.to/#/@mrvladus:matrix.org) says

> After two weeks of development I've released [List](https://github.com/mrvladus/List) 44.6.7. The main feature of this release is implementation of Drag and Drop. Now you can easily move tasks around! Moving sub-tasks between tasks now works too.
> 
> This update also includes:
> * Added smooth fade effect on top of the tasks list
> * Tasks list now scrolls when new task added
> * Added button to scroll to the top
> * Minor UI changes and improvements
> ![](IiSLxsQPdrQDBUBjxMKEGucf.png)

[Gianni Rosato](https://matrix.to/#/@computerbustr:matrix.org) announces

> Aviator 0.4.1 is released today with the following changes:
> 
> * The custom SVT-AV1 fork used in Aviator has been updated, promising up to a 40% speed improvement for the slower, higher quality presets
> * A new "Crop" option for switching between cropping & scaling resolution has been added
> * The internal SVT-AV1 parameters have been tweaked for a 0.3-0.8% improvement in perceptual quality
> ![](ggyyMqnmMolaDXVJieQYZAWK.webp)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) announces

> [Phosh](https://gitlab.gnome.org/World/Phosh/phosh) 0.29.0 is out. This release adds improved audio device selection and notifications for ongoing calls. When phosh has information about a device's notches it can avoid placing UI elements there and you can enable suspend from the system menu. Notifications can now take more screen space on the lockscreen and we made a bunch of robustness fixes to the emergency info preferences plugin.
> ![](TrzlPLfKCPFKMngdlJgkblZO.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Fyodor Sobolev](https://matrix.to/#/@fsobolev:matrix.org) reports

> Parabolic [V2023.7.1](https://github.com/NickvisionApps/Parabolic/releases/tag/2023.7.1) is here! In this release we fixed 2 bugs and added 1 new feature:
> 
> * Fixed an issue where CPU usage was high during download due to excessive logging
> * When downloading a playlist with enabled metadata embedding, a media's position will be written to the tag's track property
> * Fixed an issue where Validate button was sensitive during validation, causing error if the button was pressed multiple times
> * Updated translations (Thanks everyone on Weblate!)

### IPlan [↗](https://github.com/iman-salmani/iplan)

Your plan for improving personal life and workflow

[Iman Salmani](https://matrix.to/#/@imansalmani:matrix.org) announces

> [IPlan 1.8.0](https://github.com/iman-salmani/iplan) is now out!
> What's changed:
> * Move up and down option for tasks rows
> * Fade effect for moving between projects
> * Improve date format
> * Popovers aligned and calendar navigation buttons width reduced, to fit in the phone form factor
> * Code refactoring, Bug fixes, and UI improvements
> 
> You can get it from [Flathub](https://flathub.org/apps/ir.imansalmani.IPlan)
> ![](sbhWVsCRSuzowPbOalJNDXEe.png)

### Fretboard [↗](https://github.com/bragefuglseth/fretboard)

Look up guitar chords

[Brage](https://matrix.to/#/@bragefuglseth:matrix.org) reports

> This week I released [Fretboard](https://github.com/bragefuglseth/fretboard), an app that lets you find guitar chords by typing their names or plotting them on an interactive guitar neck. No matter if you are a beginner or an advanced guitarist, you can use Fretboard to practice, learn, and master your favorite songs! 
> 
> I'd appreciate your help with improving the chord data. Please report any weird chord recommendations in the issue tracker, and I'll have a look at it.
> 
> Install Fretboard from [Flathub](https://flathub.org/apps/dev.bragefuglseth.Fretboard) and give it a spin!
> ![](XgysFywEoPVcLOqAqcAtkwkX.png)

# GNOME Foundation

[Rosanna](https://matrix.to/#/@zana:gnome.org) says

> This was a short week because of the holiday, but there still was a lot going on. With GUADEC coming soon at the end of the month, the staff is working towards making sure everything happens as smoothly as possible. If you haven't signed up for the Latvia social trip yet, there are still some spots open: https://events.gnome.org/event/101/page/164-northern-latvia-trip. I am very much looking forward to that!
> 
> I spent a lot of this week catching up on lots of minutiae. There has been an emphasis on taxes and books this past month, as we have been onboarding our bookkeeper and working towards filing our taxes. I also renewed our mailbox, went over our foundation insurance policies, did state-mandated training, etc. In between that, I had meetings on the Executive Director search, travel issues, and finances, along with individual meetings with staff.
> 
> I am also getting ready to attend FOSSY next week where I will be giving a talk on creating crosswords with free software. GNOME will also be running a booth there, so please drop on by and say hi if you are going to be in Portland!

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

