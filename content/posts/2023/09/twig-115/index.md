---
title: "#115 Modern Monitoring"
author: Felix
date: 2023-09-29
tags: ["eyedropper", "chess-clock", "webfont-kit-generator", "gdm-settings", "share-preview", "apostrophe", "ticketbooth", "snoop", "parabolic", "flatseal", "commit", "warp", "eartag"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 22 to September 29.<!--more-->

# GNOME Core Apps and Libraries

[martymichal](https://matrix.to/#/@harrymichal:matrix.org) announces

> The long awaited port of GNOME System Monitor to GTK 4 has finally arrived! The porting effort focused on keeping the UI as close to the original but still minor enhancements can be seen. The port comes early in the new development cycle which gives us the opportunity to find possible bugs and make use of the new stylish widgetry in GTK 4 and libadwaita.
> 
> See the original [merge request](https://gitlab.gnome.org/GNOME/gnome-system-monitor/-/merge_requests/55).
> ![](EClWPKxcBoDPInSFzZNXqTTo.png)
> ![](zKjoUQMFtkyVdLxxbUdfSLLg.png)
> ![](gxDoCzFeNlWHARsDytLdYgtC.png)

# GNOME Circle Apps and Libraries

### Webfont Kit Generator [↗](https://github.com/rafaelmardojai/webfont-kit-generator)

Create @font-face kits easily.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) reports

> Webfont Kit Generator 1.1.0 has been released and is available on [Flathub](https://flathub.org/apps/com.rafaelmardojai.WebfontKitGenerator). 
> 
> It features some design improvements. It has been updated to the GNOME 45 style and the font options are now a utility panel instead of a separate view.
> 
> The application is in maintenance mode, but this release brings some small additions. Its Google Fonts importer now allows the use of v1 CSS API urls, meaning that if you have an old url, you can import it without going back to Google Fonts. Base64 support has also been added, so you can now embed the fonts in the generated CSS file.
> 
> As always, this update also includes minor bug fixes.
> ![](LqmDsSFuCYukldIVdvzxUWaA.png)

### Warp [↗](https://gitlab.gnome.org/World/warp)

Fast and secure file transfer.

[Fina](https://matrix.to/#/@felinira:matrix.org) reports

> Warp 0.6 has been released. It features a redesigned code view and an update to the latest GNOME 45 style and widgets. Now your file transfers will look even better!
> ![](AHhxdvqkNzDFQmnuQVnsWSCN.png)

### Share Preview [↗](https://github.com/rafaelmardojai/share-preview)

Test social media cards locally.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) says

> Share Preview 0.4.0 is out and available on [Flathub](https://flathub.org/apps/com.rafaelmardojai.SharePreview), it adds two new services to the preview, Discourse, the self-hosted forum/discussion platform, and LinkedIn.
> 
> It also adds the ability to open multiple instances of the application, along with some minor cosmetic improvements.
> ![](WiyKJuMGfzRcNvIXgdGubeSZ.png)

### Eyedropper [↗](https://flathub.org/apps/details/com.github.finefindus.eyedropper)

Pick and format colors.

[FineFindus](https://matrix.to/#/@finefindus:matrix.org) says

> I've released Eyedropper version 1.0.0. This is the biggest release for Eyedropper yet, here are some of the major features, including those from the last beta release
> 
> * Visual feedback when entering a color format
> * Support for entering all formats
> * Ability to search for colors in the activity overview
> * Choice of file format when exporting palettes
> * Export palettes for LibreOffice
> * Updated design
> * Show only unique colors (thanks @gregorni)
> * Fixed bugs (and probably added new ones)

### Ear Tag [↗](https://gitlab.gnome.org/knuxify/eartag)

Edit audio file tags.

[knuxify](https://matrix.to/#/@knuxify:cybre.space) says

> [Ear Tag 0.5.0](https://gitlab.gnome.org/World/eartag/-/releases/0.5.0) has been released! This release introduces three main improvements:
> 
> * The cover art button has been expanded to allow for changing both the front and back covers. It also now has an option to remove cover art from the file.
> * The "Identify Selected Files" dialog has been re-worked, and now pulls data from MusicBrainz based on present tags, not just AcoustID fingerprints. It also attempts to find the correct release for a track instead of selecting the first best one.
> * The UI now uses the new libadwaita 1.4 widgets, and thus adapts to the new application style.
> 
> You can get the latest version on [Flathub](https://flathub.org/apps/app.drey.EarTag).
> ![](KjTvdahWAxopxCzVvyCjOYcB.png)
> ![](obqemXZEcdQzJrlhfhGHredx.png)

### Commit [↗](https://github.com/sonnyp/Commit)

An editor that helps you write better Git and Mercurial commit messages.

[Sonny](https://matrix.to/#/@sonny:gnome.org) reports

> Commit 4.1 is [available on Flathub](https://flathub.org/apps/re.sonny.Commit). This release brings spell check back thanks to [libspelling](https://gitlab.gnome.org/chergert/libspelling).
> ![](50062eb115cab8caf1d0a2234f592cf2f56771f8.png)

### Chess Clock [↗](https://gitlab.gnome.org/World/chess-clock)

Time games of over-the-board chess.

[Clara Hobbs (she/they)](https://matrix.to/#/@plum-nutty:matrix.org) says

> Chess Clock version 0.6.0 was released, bringing adaptivity refinements and alert sounds.
> 
> * Thanks to Mariko Ueno, we now have alert sounds when a player's timer runs low.  These can be muted if desired via the menu.
> * The window adapts better to portrait aspect ratios and large screen sizes, enabling improved tablet support.
> 
> By the time of publishing, the release should be available over on [Flathub](https://flathub.org/apps/com.clarahobbs.chessclock).
> ![](HMkJkzUnJmFhiXzorTPXCvmd.png)

### Apostrophe [↗](https://gitlab.gnome.org/World/apostrophe)

A distraction free Markdown editor.

[Manu](https://matrix.to/#/@somas95:gnome.org) announces

> Some weeks ago I resumed the ongoing port to GTK4 of Apostrophe. While doing so I took a little detour and implemented a full fledged toolbar. It allows to format text and input common markdown elements. It is context aware, so for example clicking the "checklist button" will toggle a checklist item if the cursor is in one! Next week I'll be improving the security of the app and sanding any rough edges there are still left
> {{< video src="32d8a85a9b24864ec380812f0cc7d3393e13994c.mp4" >}}

# Third Party Projects

[xjuan](https://matrix.to/#/@xjuan:gnome.org) announces

> Cambalache 0.16.0 released!
> 
> New release targeting GNOME SDK 45 with all the new goodies in Gtk and Adwaita 1.4
> 
> Read more at https://blogs.gnome.org/xjuan/2023/09/28/cambalache-0-16-0-released/

[dabrain34](https://matrix.to/#/@scerveau:igalia.com) reports

> GstPipelineStudio version 0.3.4 is out ! Draw your own pipeline for GStreamer
> 
> This is an *important* bug fix release for first adopters, here is the release notes and the binaries:
> 
> https://dabrain34.pages.freedesktop.org/GstPipelineStudio/
> 
> An update is also available on flathub https://flathub.org/apps/org.freedesktop.dabrain34.GstPipelineStudio
> ![](oiDZaWZhcHYRqqGKtnfvRhNu.png)

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) reports

> My little application Diccionario de la Lengua, which is used to look up words in the Spanish dictionary, has received a small cosmetic update to follow the new GNOME 45 look and feel. It also now has a permanent history sidebar if the window is wide enough. 
> 
> Get it from [Flathub](https://flathub.org/apps/com.mardojai.DiccionarioLengua).
> ![](iAnqdOQTTzdATbYrvVCeiVaT.png)

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> For the people interested in high quality 2D rendering, I am proud to announce the first stable release of the Cairo 2D rendering library after 5 years of development. With the work of 77 committers, the 1.18 stable release introduces new API and features such as:
> * support for COLR fonts
> * support for the DWrite backend on Windows
> * performance improvements on macOS
> * dithering options for image surfaces
> * a whole new build system, using [Meson](https://mesonbuild.com), for increased portability and speed
> on top of many, many build, documentation, and bug fixes. You can get the new release on the [Cairo website](https://cairographics.org).

[Hari Rana (TheEvilSkeleton)](https://matrix.to/#/@theevilskeleton:fedora.im) reports

> Upscaler 1.2.0 is available on [Flathub](https://flathub.org/apps/io.gitlab.theevilskeleton.Upscaler)!
> 
> We've put a lot of effort to improve stability and compliance with the GNOME interface guidelines. We've also made the following changes/additions:
> 
> - New icon
> - Add drag & drop support
> - New keyboard shortcuts
> - Add labels for assistive technologies
> - Improve support for high contrast
> - Port to newer widgets
> - Ellipsize text when file names are too long
> - Add WebP support
> - Transpose image (useful for photos taken by smartphones)
> - New translations
> - Add Vulkan checker
> ![](70cae7e925dafc1427e54a129f944f2fc157a9e7.png)
> ![](b645727e093f0b001648a02b1300b18d9d981ab2.png)

[0xMRTT [envs.net]](https://matrix.to/#/@0xmrtt:envs.net) says

> This week Bavarder 1.0 was released! It's featuring a completely new UI with a multi-chat mode which works on desktop and mobile thanks to the new Libadwaita widgets. It's now possible to use local models, export conversations, change bot and user name, markdown support and more! Thanks to everyone who helped for making it possible!
> 
> Check out the [website](https://bavarder.codeberg.page/) for more and a full changelog!
> ![](c9bd2e467d22aa9223cc869acedfc3e1d61934a61705464522975739904.png)
> ![](119204b2f0526787c76467c524fd2ed5565160db1705464529741152256.png)
> ![](461140940be25e986ba76f05d6c87a91d99e7c241705464537383174144.png)

### Ticket Booth [↗](https://github.com/aleiepure/ticketbooth)

Keep track of your favorite shows.

[Alessandro Iepure](https://matrix.to/#/@aleiepure:matrix.org) says

> Say hello to Ticket Booth! Never again lose track of your favorite movies and TV shows. 
> It leverages the power of TMDB’s API to serve you the latest information in a beautiful interface. It also features a fully offline mode if you prefer to handcraft your library without relying on a third-party service. 
> Check it out on [Flathub](https://flathub.org/it/apps/me.iepure.Ticketbooth).
> ![](eUuUShcAvRsYXxhEsFUqsAbP.png)
> ![](qiyVdfMrqPNfyeXzmBKXJYSB.png)

### Snoop [↗](https://gitlab.gnome.org/philippun1/snoop)

Snoop through your files.

[Philipp](https://matrix.to/#/@philippun:matrix.org) says

> Snoop initially released!
> 
> [Snoop](https://gitlab.gnome.org/philippun1/snoop) is a gui tool to search through your files. It also offers a Nautilus extension to directly start it from a folder and search in it.
> 
> It is available as a [flatpak on flathub](https://flathub.org/apps/de.philippun1.Snoop).
> 
> If you want to use the Nautilus extension with the flatpak, you have to manually get it from the repo and install it in a Nautilus extension folder, see the [readme](https://gitlab.gnome.org/philippun1/snoop/-/blob/main/README.md?ref_type=heads#flatpak).
> ![](JCPWLdeIrsRVHsNwnyTwASXH.png)
> ![](tmWqkPmRZUZlDLTObDOGiofg.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Parabolic [V2023.9.1](https://github.com/NickvisionApps/Parabolic/releases/tag/2023.9.1) is here! This release includes many features and bug fixes that make the subtitle experience a lot better in Parabolic :)
> 
> Here's the changelog:
> * Added an option to disable embedding subtitles in a file and instead download them in a separate file
> * Added the ability to specify ""all"" in the subtitle languages list to download all available languages
> * Fixed an issue where empty subtitles were sometimes embedded
> * Fixed an issue where arte.tv links were not validating thanks to a yt-dlp update
> * If embedding subtitles fails, Parabolic will automatically save them to separate files instead
> * Improved Parabolic's display of progress in taskbar (dock)
> * Updated translations (Thanks everyone on Weblate!)
> ![](hrEjVZzfrWwyjCjKQkMMMfHm.png)

### Login Manager Settings [↗](https://gdm-settings.github.io)

Customize your login screen.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) says

> I released GDM Settings [v4.beta0](https://github.com/gdm-settings/gdm-settings/releases/tag/v4.beta0). This release brings
> 
> * New UI style
> * [GNOME 45 support](https://github.com/gdm-settings/gdm-settings/issues/184)
> * [Background image adjustment option](https://github.com/gdm-settings/gdm-settings/issues/180)
> * Support for high contrast mode in GDM
> * Application Window is dragable from anywhere now
> * [New graphical error message](https://github.com/gdm-settings/gdm-settings/issues/171) (replaces an unhelpful error printed on the terminal)
> 
> Since this is a beta release,
> 
> * Expect bugs and if you do encounter any bug, please [report it](https://github.com/gdm-settings/gdm-settings/issues/new/choose).
> * It is not yet available on the regular (stable) Flathub repo. You can [install it from Flathub beta repo](https://dl.flathub.org/beta-repo/appstream/io.github.realmazharhussain.GdmSettings.flatpakref).
> ![](IBwUqmSdrQAsYyqtsCTcXFNC.png)
> ![](QPYiFEZzSVChisqDJuuByLSe.png)
> ![](OglNFUVjRnTAQlQlcggphlZT.png)

### Flatseal [↗](https://github.com/tchx84/Flatseal)

A graphical utility to review and modify permissions of Flatpak applications.

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) reports

> I am happy to announce the release of Flatseal 2.1.0. This new release comes with refined visuals, improved performance, support for a new permission, quality of life additions, and fixes.
> 
> Read all the details [here](https://blogs.gnome.org/tchx84/2023/09/28/flatseal-2-1-0/).
> ![](zilgPLNOowMDnpfFvlxNiAzC.png)

# Shell Extensions

[glerro](https://matrix.to/#/@glerro:matrix.org) announces

> [Transmission Daemon Indicator NG](https://extensions.gnome.org/extension/6204/transmission-daemon-indicator-ng/) is updated for Gnome Shell 45

[Mateus R. Costa](https://matrix.to/#/@MateusRodCosta:matrix.org) announces

> [Default Workspace](https://extensions.gnome.org/extension/4783/default-workspace/) has been updated to work with GNOME 45.
> This gnome-shell extension allows you to choose a specific workspace to automatically switch to as soon as you login. This is particularly useful if you have a workflow where you use fixed workspaces and don't use the first workspace as the main one.

[Just Perfection](https://matrix.to/#/@justperfection:gnome.org) says

> Thanks to Aryan Kaushik, [extensions.gnome.org](https://extensions.gnome.org/) now supports `liberapay` and `opencollective` in [donations key](https://gjs.guide/extensions/overview/anatomy.html#donations).

# Events

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) reports

> To celebrate the release of GNOME 45 we had a hackfest and release party last week!
> 
> Some hightlights:
> 
> * Lots of productive discussions for how we can get GNOME OS (and image-based OSes more generally) to the next level
> * We discussed in detail how we could do [local-first sync](https://www.inkandswitch.com/local-first) in GNOME, and made a tentative plan for first prototypes
> * Adrian and Jonas resurrected the transparent panel branch
> * Julian looked into fixing [default avatars](https://gitlab.gnome.org/GNOME/gnome-control-center/-/issues/1663)
> * Various discussions about improving developer experience
> * Lots of work on apps, including Evince, Railway, and Dino
> * There was cake! 🎂
> 
> More details in this blog post: https://blogs.gnome.org/tbernard/2023/09/26/gnome-45-release-party-hackfest
> ![](7dc50fd6cfe6bf9a48fff6c753d062bdf1a20491.jpg)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

