---
title: "#128 Bye Bye 2023"
author: Felix
date: 2023-12-29
tags: ["nautilus", "loupe", "graphs", "flare", "fretboard", "phosh", "fragments"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from December 22 to December 29.<!--more-->

# GNOME Core Apps and Libraries

### Files [↗](https://gitlab.gnome.org/GNOME/nautilus)

Providing a simple and integrated way of managing your files and browsing your file system.

[antoniof](https://matrix.to/#/@antoniof:gnome.org) announces

> Khalid Abu Shawarib improved the discoverability of the custom folder icon feature in Files. Now there is an edit button and also, if a custom icon is set, a quick reset button.
> 
> This was a side effect of porting a few dialogs away from deprecated GTK dialog APIs
> ![](477afe6cb314350bdda7c99210a7a0ad4550afc21740825150921637888.png)

### Image Viewer (Loupe) [↗](https://apps.gnome.org/app/org.gnome.Loupe/)

Browse through images and inspect their metadata.

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) reports

> Loupe nightly now uses the first alpha version of [Glycin 1.0](https://gitlab.gnome.org/sophie-h/glycin) to load images. This brings stricter sandboxing for image loaders when used outside of Flatpaks. It also limits the memory usage of loaders to avoid many accidental or malicious out of memory scenarios. The memory limitation is not available in Flatpaks yet.

# GNOME Circle Apps and Libraries

[Vlad Krupinski](https://matrix.to/#/@mrvladus:matrix.org) says

> This week I released [Errands](https://apps.gnome.org/List/) 45.1 with some nice new features:
> 
> * Support for multiple task lists
> * Tasks now have properties: notes, start and due date, priority, percentage of completion, tags
> * Improved synchronization with Nextcloud Tasks and CalDAV with support for multiple calendars and task properties
> * Import and export in ".ics" format
> ![](CwInJkddSPloZiDOwypWsfvC.png)

### Fragments [↗](https://gitlab.gnome.org/World/Fragments)

Easy to use BitTorrent client.

[Felix](https://matrix.to/#/@felix:haecker.io) says

> I added drag-and-drop support to Fragments, and implemented a new (optional) setting which automatically moves the torrent file to trash after adding it.
> {{< video src="yCbJaeXpxjinrHItRIxcdZbI.webm" >}}

# Third Party Projects

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) announces

> Matthew Dennis made [Phosh](https://gitlab.gnome.org/World/Phosh/phosh)'s bottom bar smaller so there's more space for apps like video players on your phone - especially in landscape. The screenshot features [livi](https://gitlab.gnome.org/guidog/livi) a little video player targeting mobile:
> ![](PwGwHuYsPJmLrwHSIIwAbesS.png)

### Graphs [↗](https://graphs.sjoerd.se)

Plot and manipulate data

[Sjoerd Stendahl](https://matrix.to/#/@sjoerdb93:matrix.org) reports

> Since the summer we've been working hard on the next release of Graphs. A full announcement of the next stable release will follow soon, but we want to give it a bit of time to do some more testing and final polish to make sure the next stable release is the best it can be. In the meantime, I'm happy to announce an official beta version which is feature-complete, and encourage anyone to give it a spin!
> 
> Some of the highlights of this upcoming release include, but are not limited to:
> 
> * A major UI overhaul with the new GNOME 45 widgets
> * Brand new style previews
> * Curve fitting
> * Touchpad gestures are now supported on the canvas to zoom and pan the view.
> * Translations are now hosted on [Weblate](https://hosted.weblate.org/projects/graphs/graphs/). Click the link if you want to contribute to translations. Currently we're missing strings for Spanish and German, but help with other languages are always welcome as well.
> 
> To install the beta, follow the instructions on [our Github page](https://github.com/Sjoerd1993/Graphs/). Stay tuned for a full announcement for the stable release somewhere in the upcoming weeks!
> ![](sNrFqRMgwJKNGbahWHogiEgh.png)
> ![](ajuhBIajgRpnvtklRPdmNgme.png)
> ![](wJwDumNCOOxjooCNmHyuoHoM.png)

### Fretboard [↗](https://github.com/bragefuglseth/fretboard)

Look up guitar chords

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) says

> This week I published Fretboard 5.0. It may not arrive in wrapping paper, but this timely release still includes some nice gifts:
> 
> * An enharmonic indicator that lets you see when chords have more than one possible name
> * Automatic chord name formatting, turning your sloppily typed “bb maj7” into a beautiful “B♭maj7”
> * More consistent keyboard shortcuts
> * Enhanced accessibility for screen readers
> * German and Swedish translations, making Fretboard available in a total of 13 languages
> 
> If you would like to come with suggestions, report bugs, translate the app, or contribute otherwise, feel free to reach out! Download Fretboard on [Flathub](https://flathub.org/apps/dev.bragefuglseth.Fretboard).
> ![](7c71d8239d534bacc557b49a46d8647505f1cfce1739090155437817856.png)

### Flare [↗](https://flathub.org/apps/details/de.schmidhuberj.Flare)

Chat with your friends on Signal.

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) reports

> Version 0.11.0 of Flare got now released. This release brings many important fixes for bugs which made previous versions pretty much unusable for new devices. This includes a fix for syncing contacts, but also a fix for linking recently breaking. This release also includes many UI improvements, like an improved setup window which introduces users to Flare, lets them choose between using Flare as a linked device or as a primary device (this is currently still disabled as it still needs more extensive testing) and gives further information on what important bugs are still in Flare and how to get in contact. There were also many more minor UI improvements and also updates to the newest GTK and libadwaita versions.
> 
> You may have noticed that the last release of Flare was quite some time ago (about 4 months). This was due to aforementioned issues we had with contact syncing and linking; those first required upstream fixes and those fixes furthermore introduced some regressions which also needed to be fixed. This required in total eight beta releases for everything to be (hopefully) fixed.
> ![](cPVBbcHutnKthbPuFpKLDMZD.png)
> {{< video src="ktaKmmwRRotzTKKAoRdXqoud.webm" >}}

# That’s all for this week!

See you next year, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

