---
title: "#125 Portalled USB Devices"
author: Felix
date: 2023-12-08
tags: ["tracker", "phosh"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from December 01 to December 08.<!--more-->

# Sovereign Tech Fund

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) reports

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/) project, a number of community members are working on infrastructure related projects.
> 
> * Georges opened pull requests fo his work on the USB portal
>     - [added --usb and --no-usb to flatpak](https://github.com/flatpak/flatpak/pull/5620)
>     - [portal API specification](https://github.com/flatpak/xdg-desktop-portal/pull/1238)
>     - [experimental branch of Boatswain for testing](https://gitlab.gnome.org/World/boatswain/-/commit/c157851ff35300da1b22fd757d745fef0dd90b83)
> * Julian fixed a lot of style and polish issues on the [notification grouping MR](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3012)
> * Jonas [fixed hidden scrollbars taking up space in the layout](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/2190) in GNOME Shell
> * Joanie fixed several issues in Orca related to keygrabs, and some regressions from the giant table refactor
> * Evan Welsh is working on sync, async and finish annotations for GObject introspection
> * With the help of Daiki, Dhanuka [completed porting the PAM module from gnome-keyring to libsecret](https://gitlab.gnome.org/GNOME/libsecret/-/merge_requests/128)
> 
> Areas we're currently investigating:
> 
> * Exploring the constraints and options for fractional scaling
> * We are looking at the state of speech synthetizers on Linux, particularly in relationship with the screen reader
> * We're discussing the technical requirements for [adaptive dialogs (bottom sheets)](https://gitlab.gnome.org/Teams/Design/os-mockups/-/blob/master/dialogs/bottom-sheets.png)
>
> {{< video src="usb" >}}

# GNOME Core Apps and Libraries

### Tracker [↗](https://gitlab.gnome.org/GNOME/tracker/)

A filesystem indexer, metadata storage system and search tool.

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) reports

> Some memory usage fixes landed in the Tracker SPARQL database library this week:
> 
> * Fewer memory allocation operations when processing text ([!637](https://gitlab.gnome.org/GNOME/tracker/-/merge_requests/637)), thanks to Carlos Garnacho
> * Memory leak in `tracker:strip-punctuation()` fixed ([!639](https://gitlab.gnome.org/GNOME/tracker/-/merge_requests/639)), thanks to Lukáš Tyrychtr

# Third Party Projects

[Giant Pink Robots!](https://matrix.to/#/@giantpinkrobots:matrix.org) reports

> I released the v2023.12.7 version of Varia, a new download manager written with GTK4 and Libadwaita. Originally released a week prior, but now it has more essential features built in.
> 
> It's available on Flathub: https://flathub.org/apps/io.github.giantpinkrobots.varia

[Krafting](https://matrix.to/#/@lanseria:matrix.org) says

> I'm pleased to announce the release of [Playlifin](https://flathub.org/apps/net.krafting.Playlifin), a simple tool that aids in importing your YouTube music playlists to your Jellyfin server!
> 
> It's now available on [Flathub](https://flathub.org/apps/net.krafting.Playlifin).
> ![](uwFaTbDnFUsAiVtAETEayqHO.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) announces

> [Phosh](https://gitlab.gnome.org/World/Phosh) 0.34.0 is out. This includes an updated Wayland compositor (phoc) that works with the recently released wlroots 0.17.0 adding support for new Wayland protocols like security-context-v1 (to limit protocol access for flatpaks). We also fixed night light support and drag and drop on touch screens.
> 
> Check the full details [here](https://phosh.mobi/releases/rel-0.34.0/)
> ![](iSIpDGoiQmCrtSDSMLYOiwdT.png)

# Miscellaneous

[Felipe Borges](https://matrix.to/#/@felipeborges:gnome.org) reports

> We are happy to announce that GNOME is sponsoring two [Outreachy ](https://www.outreachy.org/) internship projects for the December 2023 to March 2024 Outreachy internship round where they will be working on [implementing end-to-end tests for GNOME OS using openQA](https://gitlab.gnome.org/GNOME/openqa-tests/-/blob/master/README.md?ref_type=heads).
> 
> [Dorothy Kabarozi](https://gitlab.gnome.org/kizdorothy) and [Tanjuate Achaleke](https://gitlab.gnome.org/acha) will be working with mentors Sam Thursfield and Sonny Piers.
> 
> Stay tuned to [Planet GNOME ](https://planet.gnome.org/) for future updates on the progress of this project!

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

