---
title: "#126 New Apps"
author: Felix
date: 2023-12-15
tags: ["railway", "glib", "paper-clip", "tracker"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from December 08 to December 15.<!--more-->

# Sovereign Tech Fund

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) reports

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/) project, a number of community members are working on infrastructure related projects.
> 
> * Philip landed libgirepository changes in GLib, https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3703
> * Julian's notifications grouping merge request is close to ready https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3012
> * Alice started implementing bottom sheets/adaptive dialogs and using them in Libadwaita https://gitlab.gnome.org/GNOME/libadwaita/-/commits/wip/alice/bottom-sheets
> * Dhanuka released libsecret 0.21.2
> * Dhanuka sent an initial PR to implement server-side dbus secret service interfaces in oo7 https://github.com/bilelmoussaoui/oo7/pull/56
> * Evan is working on updating the sync/async APIs for the girepository migration to GITypeInstance
> * Philip got the MR for "GBytes variants for GSocket receive methods" ready https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3603
> * Over the past few weeks Andy worked on getting WebDAV support in GNOME Online Accounts over the line, and porting to OAuth2.0 and GTK4/Libadwaita https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/137
> * Andy wrote a [blog post about this and more of his work on GNOME Online Accounts](https://andyholmes.ca/posts/goa-and-stf-part-1)
> ![](dbe904881bf4ff5196bfe1eab8be2bfd7a81388f1735719203161243648.png)
> {{< video src="adwaita.webm" >}}

# GNOME Core Apps and Libraries

### Tracker [↗](https://gitlab.gnome.org/GNOME/tracker/)

A filesystem indexer, metadata storage system and search tool.

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) says

> Carlos Garnacho added another layer of protection to the metadata extraction sandbox in Tracker Miners. This sandbox is a defense in case a security issue is found in one of the distro-packaged libraries that tracker-extract uses to parse user content. The sandbox now uses [Landlock](https://docs.kernel.org/userspace-api/landlock.html), a recently-added security API in Linux, to limit filesystem access on top of the existing protections provided by SECCOMP.

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> libgirepository in GLib has just been significantly reworked to use `GTypeInstance`; expect more API breaks from it before its first stable release; see https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3703

# GNOME Circle Apps and Libraries

### Paper Clip [↗](https://apps.gnome.org/PdfMetadataEditor/)

Edit PDF document metadata.

[Diego Iván M.E](https://matrix.to/#/@dimmednerd:matrix.org) says

> Paper Clip v4.0 is here!
> 
> This new release brings some quality of life improvements and new features:
> 
> * Paper Clip now supports XMP metadata! You can (finally) edit XMP properties for documents that support it and keep them in sync with the ordinary metadata.
> * Better thumbnails (again): Thumbnails now use less memory and load faster. This is specially noticeable for large documents, which now use 80-90% less memory. There is still room for improvement, so expect more changes in this lane in upcoming releases.
> * The About Window now shows debug information relevant for bug reports.
> * A Nautilus Extension for Paper Clip is now available in the [Github repository](https://github.com/Diego-Ivan/Paper-Clip/blob/main/data/nautilus-extension/paper-clip.py). Thanks to DodoLeDev for working on this!
> 
> You can grab the latest release from [Flathub](https://flathub.org/apps/io.github.diegoivan.pdf_metadata_editor)
> ![](fPFoVwvXRfKoQVkLOdPzPPPR.png)

# Third Party Projects

[Gianni Rosato](https://matrix.to/#/@computerbustr:matrix.org) reports

> **My GUI for AV1 encoding, Aviator, just got a major speed boost!** Encoding with Aviator 0.4.3 is now around 17-53% faster for presets 0 through 6 and 1-4% better quality for presets 7+. Here are the details:
> * Updated SVT-AV1 to a custom fork by BlueSwordM featuring changes from 1.8.0, including speed-ups for presets 1 through 6 that improve encoding speed by 17-53%. This fork features an adaptive quantization curve that boosts deltaq based on variance within superblocks. This is not available in mainline SVT-AV1
> * SVT-AV1 now features NEON optimization, making encoding up to a whopping 8 times faster than before on ARM-based platforms
> * Updated to GNOME 45 runtime
> * Minor changes in the codebase that should make it easier to add an Open GOP toggle in the future (currently breaks keyframe placement)
> 
> *Download on [Flathub](https://flathub.org/apps/net.natesales.Aviator) !*
> ![](qlidWAZKNzkyaSueXnzPHwug.webp)

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) reports

> #GNOME #Education #LearnToCode #VideoGames 
> 
> If you're interested in those topics you might also be interested in my upcoming "weekend" project.
> 
> [[teaser] Gameeky: A new learning tool to develop STEAM skills](https://blogs.gnome.org/tchx84/2023/12/15/gameeky-a-new-learning-tool-to-develop-steam-skills/)
> ![](tVennFldUfwHcXnSzwlOqhCB.png)
> ![](BFruDPnznpUXBoyWGFNzXapd.png)

[tfuxu](https://matrix.to/#/@tfuxu:matrix.org) reports

> [Halftone](https://github.com/tfuxu/Halftone) 0.5.0 improves on mobile users experience, by moving sidebar to the bottom on mobiles. It also comes with options retention, so now you can quickly dither multiple images using the same settings. Get this new release from [Flathub](https://flathub.org/apps/io.github.tfuxu.Halftone)
> ![](BgKlyDJQIUPDnemMGrvDwySR.png)

[Dave Patrick](https://matrix.to/#/@sedve:matrix.org) reports

> I'm pleased to announce [Dagger](https://github.com/SeaDve/dagger), a new app used to edit and view Graphviz dot files!
> {{< video src="SagSELZDwXtSsbHiTMjPkOMz.webm" >}}

[hergertme](https://matrix.to/#/@hergertme:gnome.org) reports

> Prompt is a new terminal application that focuses on immutable and container-oriented desktops. It can be thought of as a companion terminal to GNOME Builder! You can read about how container technology was lifted from GNOME Builder as well as what it takes to design a robust terminal that can be shipped as a Flatpak at https://blogs.gnome.org/chergert/2023/12/14/prompt/
> ![](003dc6ca50f951ffd10f8b3817535da045dac24e1735736215426039808.png)

[Alain](https://matrix.to/#/@a23_mh:matrix.org) reports

> Planify 4.2 has been released and is available on [Flathub](https://flathub.org/es/apps/io.github.alainm23.planify).
> 
> This release contains numerous fixes and few other changes:
> 
> * Icons size update.
> * Custom decoration layout support.
> * Improved colors in light theme.
> * Sort to-dos by project available.
> * Ability to configure or decrease the size of the sidebar.
> * Available configuration to change the start day of the week in the calendar.
> * Added the functionality to select the home page.
> * Uses new libadwaita widgets and GNOME 45
> * Bugs fixed #1053, #1026, #1042, #1041, #1037, #1035, #1015, #1001, #995, #946, #1055.
> ![](MXmJgbKguEdEJWBMCySkWHFg.png)
> ![](ZBSOlzgfEEnFJUZDVePbvpmP.png)
> ![](oymrACuvsysPUKJOlFPjldsf.png)

### Railway [↗](https://gitlab.com/schmiddi-on-mobile/railway)

Travel with all your train information in one place.

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) announces

> Version 2.2.0 of Railway was released. This release has many minor quality-of-live and UI improvements. It adds a filter for the provider list, and an indicator of when a journey was last refreshed. Railway now also saves the window size between restarts, disables some buttons while content is loading and furthermore does not collapse journey legs anymore when refreshing a journey. Railway can now also be translated with [Weblate](https://hosted.weblate.org/projects/schmiddi-on-mobile/railway/), and is already available in Dutch, French and German.
> 
> The full change log can be viewed in the [release notes](https://gitlab.com/schmiddi-on-mobile/railway/-/releases/2.2.0).
> ![](SVerWFCTezFOiHTMGhWMSUCo.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

