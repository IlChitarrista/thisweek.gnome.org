---
title: "#102 Contextual Back Buttons"
author: Felix
date: 2023-06-30
tags: ["tagger", "iplan", "gnome-network-displays", "libadwaita", "parabolic"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from June 23 to June 30.<!--more-->

# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/they)](https://matrix.to/#/@alexm:gnome.org) says

> `AdwNavigationView` back buttons now support a context menu allowing to pop multiple pages at once. This works with nested navigation views and even with structures like `AdwNavigationSplitView` containing navigation views in both content and sidebar. Additionally, back button tooltips are now more reliable - they previously didn't work in this situation and required the app to manually sync the sidebar page's title with its navigation view's visible page. Now it's automatic like in other cases
> {{< video src="a68bf128669b090e1270e36b28ee72ce2dfd606d.mp4" >}}

# Third Party Projects

[Alain](https://matrix.to/#/@a23_mh:matrix.org) reports

> Hi, Planify 4.1 is out!
> 
> * Planify has a new icon thanks to Tobias Bernard.
> * Quick Add is available, quickly add tasks from anywhere on your desktop with a simple keyboard shortcut, set it from Preferences → Quick Add
> * Preferences page redesigned.
> * The preferences to run in the background and run at startup are available.
> * The error that did not allow to see the calendar events was solved.
> * Added the preference to create tutorial project.
> * Bug fixes and performance improvement.
> 
> https://github.com/alainm23/planify/releases/tag/4.1
> Available on [Flathub](https://flathub.org/en/apps/io.github.alainm23.planify)

[Felipe Kinoshita](https://matrix.to/#/@fkinoshita:gnome.org) says

> This week I release Wildcard 0.2.0, it brings a more streamlined layout and a nice dialog for quickly switching expression flags on/off, [check it out](https://flathub.org/apps/com.felipekinoshita.Wildcard)! :D
> ![](440fee8e689a64efc0be8621b93869c804a7c571.png)

### Tagger [↗](https://flathub.org/apps/details/org.nickvision.tagger)

An easy-to-use music tag (metadata) editor.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Tagger is currently at [V2023.7.0-beta2](https://github.com/NickvisionApps/Tagger/releases/tag/2023.7.0-beta2) !
> 
> After months of no updates, we are proud to bring Tagger back and better than ever. Tagger has received the C# treatment and has been completely rewritten from the ground up with a more stable and performant backend. To signify this great release, we even have a new icon thanks to @daudix-UFO ! Besides being written in C#, we also added some new features and fixed pre-existing bugs that you can read about below. Tagger is also now available to translate [on Weblate](https://hosted.weblate.org/engage/nickvision-tagger/) !
> 
> We urge everyone who uses Tagger / wants to use Tagger to help us test the beta releases so we can iron out any issues before our stable release this Sunday. We hope you enjoy this release as much as we enjoyed making it :)
> 
> Here's the full changelog:
> 
> * Tagger has been completely rewritten in C#! With this new language comes better performance and more stable features. To signify this great change, we also updated the app icon!
> * Added a separate option in Preferences for overwriting album art with MusicBrainz metadata independently of the overwriting tag data setting
> * Added an option in Preferences for controlling how music files are sorted
> * Track values will now be padded into double digits
> * Fixed an issue where some file types were not loading album art correctly
> * Fixed an issue where applying unapplied changes would sometimes not work
> * Improved UI/UX
> * Updated translations (Thanks everyone on Weblate!)
> ![](bjheizaIAEBKCeEhCXdiyiyC.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Parabolic [V2023.6.3](https://github.com/NickvisionApps/Parabolic/releases/tag/2023.6.3) is here! 
> 
> If you haven't noticed, Tube Converter has been renamed to Parabolic! We think this is a much better name for the downloader and thank all that have worked with us to come up with a new name :)
> 
> Here's the full changelog:
> * Tube Converter has been renamed. Introducing, Parabolic!
> * Fixed a large memory leak caused by not disposing logs correctly
> * Updated translations (Thanks everyone on Weblate!)
> ![](uWyhTrNyrtNNaxppZDyszusq.png)

### IPlan [↗](https://github.com/iman-salmani/iplan)

Your plan for improving personal life and workflow

[Iman Salmani](https://matrix.to/#/@imansalmani:matrix.org) announces

> [IPlan 1.7.0](https://github.com/iman-salmani/iplan) is now out!
> What's changed:
> * Chart of Time spent in last 7 days
> * Record delete button moved to record row
> * Improve usability for the phone form factor
> * Option to disable run in background
> * Improve duration format
> * Code refactoring, Bug fixes, and UI improvements
> You can get it from [Flathub](https://flathub.org/apps/ir.imansalmani.IPlan).
> ![](zeTnryhTavHYeizuqTxTOiTx.png)

### GNOME Network Displays [↗](https://gitlab.gnome.org/GNOME/gnome-network-displays)

Stream the desktop to Wi-Fi Display capable devices

[Pedro Sader Azevedo](https://matrix.to/#/@toluene:matrix.org) says

> GNOME Network Displays gained a DBus interface, that exposes the list of screencasting devices (also known as "sinks") discovered in your network. This is part of the ongoing effort to allow GNOME Network Displays to function as a screencasting backend for other projects (e.g. GNOME Shell, GNOME Settings, xdg-portal, etc).
> ![](cVgHlKtBveEutcvkrktNWDUq.png)

# GNOME Foundation

[Kristi Progri](https://matrix.to/#/@kristiprogri:gnome.org) announces

> GUADEC 2023 is coming and the team is working hard to make this experience as pleasant as possible.
> On July 31st we are orgaizing a day trip outside of Riga. If you are interested to join please register here: 
> https://events.gnome.org/event/101/page/164-northern-latvia-trip. 
> We have extended the deadline for everyone who couldn't make it on time.
> 
> On another note, GNOME ASIA is taking place in Katmandhu, Nepal from 1st-3rd of December. 
> We are starting to put together the plans and conference timeline, if you would be interested to join the organising team please send us an email at: asia@gnome.org

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

