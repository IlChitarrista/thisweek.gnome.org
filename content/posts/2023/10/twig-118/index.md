---
title: "#118 Performant Terminals"
author: Felix
date: 2023-10-20
tags: ["tracker", "crosswords", "ashpd", "auto-activities"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from October 13 to October 20.<!--more-->

# GNOME Core Apps and Libraries

[hergertme](https://matrix.to/#/@hergertme:gnome.org) says

> VTE, the library providing a terminal emulator to GTK-based applications, received a number of performance improvements and new drawing abstractions.
> 
> VTE encrypts the scrollback buffer and uses zlib to reduce how much data needs to be encrypted. Now LZ4 is used instead of zlib to significantly speed up that process.
> 
> Performance improvements to how character and attribute arrays are managed has sped up support for bidirectional text. Additionally, many small string operations have been optimized to use faster code-paths in GLib. Many memory allocations have been completely eliminated.
> 
> Many small improvements have combined to improve the compilers ability to inline important code paths. Some code in VTE is C and some is C++ so removing various C wrappers around C++ code was completed. The PTY and UTF-8 parsers were tweaked slightly to allow skipping large chunks of sequential plain text without feeding through either parser.
> 
> A new drawing abstraction has been added which allows for GTK 3 support to continue using Cairo while GTK 4 now uses native render nodes with GtkSnapshot. Text is now rendered similarly to the work we did in GtkTextView which renders glyphs from a texture atlas on the GPU while also supporting color fonts and emoji without having to change shader programs.
> 
> Stylized line drawing is another important feature provided by terminal emulators. VTE now takes advantage of a recently added fast-path in GTK's OpenGL renderer which can render alpha-channel texture masks with a color render node. This works very similarly to how text is rendered internally by GTK and speeds things up by avoiding additional offscreen framebuffers.
> 
> Now that VTE uses native render nodes, GTK can automatically calculate damage regions when submitting a frame to the compositor.
> 
> I still expect more work to be done around frame scheduling so that we can remove the ~40fps cap that predates reliable access to vblank information.

### Tracker [↗](https://gitlab.gnome.org/GNOME/tracker/)

A filesystem indexer, metadata storage system and search tool.

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) reports

> The Tracker Extract metadata extraction tool has seen its SECCOMP sandbox improved in recent weeks. Thanks to Carlos Garnacho for spending significant effort tightening the list of system calls that parsing libraries are allowed to use, and to the various contributors who tested and reviewed changes across various architecture + OS combinations.

# GNOME Circle Apps and Libraries

### ashpd [↗](https://github.com/bilelmoussaoui/ashpd)

Rust wrapper around freedesktop portals.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> Support of the dynamic launcher portal in ASHPD Demo was added to the 0.4.1 release
> ![](013e68dbe55511dc8f77a5f2100a297ee666f0f11712929107773227008.png)

# Third Party Projects

[kaii](https://matrix.to/#/@kaii-lb:matrix.org) announces

> [overskride](https://github.com/kaii-lb/overskride), the bluetooth app just got its first major release!
> 
> overskride is a simple, to the point app designed to make it easier than ever to interact with bluetooth devices. the main features are:
> 
> * Send/Receive files
> * Bluetooth authentication agent
> * Multiple adapter support
> * ...and many more
> ![](sHIcyTevtYIASNdhjZPEGGhI.png)

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> After a year, there's a new release of xdg-desktop-portal-gtk, the GTK portal implementation! Version 1.15.0 brings various changes:
> * Meson replaces Autotools as the build system
> * portal implementations that depend on GNOME private interfaces, like background, screenshot/screencast, and remote desktop, have been dropped to remove dependencies
> * the calendar settings from gsettings-desktop-schemas are now included in the Settings portal implementation
> The versioning policy has also been changed, and odd minor version numbers are not special any more. For more information, you can read [the 1.15.0 release page](https://github.com/flatpak/xdg-desktop-portal-gtk/releases/tag/1.15.0).

### Crosswords [↗](https://gitlab.gnome.org/jrb/crosswords)

A crossword puzzle game and creator.

[jrb](https://matrix.to/#/@jblandford:matrix.org) announces

> A quick development update for GNOME Crossword Editor. I added better selections for crossword grids, and used it to add a feature I've wanted for a long time: clue fragment highlighting.
> 
> The idea motivating this is that cryptic clues often break the answer into parts that are solved separately. In the video example, we give a hint to the word GIANTS being in the answer through the phrase _"SATING DESTRUCTIVE"_. In this instance, it's a _"destructive"_ (aka anagrammed) version of _"sating"_. 
> 
> For the initial implementation of this feature, I'm just showing anagrams of the selected region, but this is a fertile area for other approaches. We are planning on also including headless/tailless words (eg, CAR ⇒ CART, CARD, SCAR, ...) or call out word reversals (BRAG ⇒ GARB). I'd also love to add a dictionary with definitions and synonyms. I can also imagine adding common wordplay indicators (F, FF ⇒ LOUD, etc).
> 
> This work is based on the GSoC Project by Pratham Gupta to add an anagram lookup table to the word list.
> {{< video src="TrrQrUUPoBRoZzTroOtfVUYs.webm" >}}

# Shell Extensions

[glerro](https://matrix.to/#/@glerro:matrix.org) announces

> [Debian Linux Updates Indicator](https://extensions.gnome.org/extension/6322/debian-linux-updates-indicator/) is updated for Gnome Shell45.
>  Updates indicator for Debian Linux based distributions.
> 
> Check for updates and shows also the following packages status (as in Synaptic):
> - Available updates.
> - New packages in repository.
> - Local/Obsolete packages.
> - Residual config files.
> - Autoremovable packages.

### Auto Activities [↗](https://extensions.gnome.org/extension/5500/auto-activities/)

Show activities overview when there are no windows, or hide it when there are new windows.

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) says

> In this new version, **Auto Activities** v19 has been ported to _ESM_, making it compatible with _GNOME Shell 45_. In addition to minor changes, it now makes use of new _Libadwaita_ widgets such as _AdwSpinRow_.
> 
> Check it out on [extensions.gnome.org](https://extensions.gnome.org/extension/5500/auto-activities/)
> ![](KGFvqkZsKwzYHGwQRRqNcPTS.png)

# Events

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) reports

> Registration is now open for #GNOMEAsia2023! This year's summit takes place on Dec 1-3 in person in Kathmandu, Nepal, and online. Learn more and register today: https://foundation.gnome.org/2023/10/20/gnomeasia-2023-registration-open/

# GNOME Foundation

[Rosanna](https://matrix.to/#/@zana:gnome.org) reports

> The GNOME Foundation has had a lot going on this week. The big news is we announced our new Executive Director, Holly Million. While she has only been on the job for a little over a week, her experience with running nonprofits has already made itself apparent. We are very lucky to have her on board!
> 
> We are inviting the community to come meet her on November 14th at 18:00 UTC for a meet-and-greet session. Please register at https://events.gnome.org/e/HollyMillion to come hear from Holly and her thoughts and plans for the Foundation. If you have questions for Holly, please submit them for this session before November 7th! Details are in the link above.
> 
> My work this week has been a lot of virtual paperwork as well as working on the books. I did take advantage of an unplanned power outage yesterday to go through some of the nonvirtual paperwork on my desk. In addition, work on the Code of Conduct committee is ongoing as we met this week to go through outstanding issues and discussed possible areas of improvement.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

