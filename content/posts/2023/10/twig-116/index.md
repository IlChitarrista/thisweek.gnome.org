---
title: "#116 Fragmented Files"
author: Felix
date: 2023-10-06
tags: ["fragments", "turtle", "phosh", "paper-clip", "smile", "flowtime", "workbench", "focus-changer"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 29 to October 06.<!--more-->

# GNOME Circle Apps and Libraries

### Fragments [↗](https://gitlab.gnome.org/World/Fragments)

Easy to use BitTorrent client.

[Felix](https://matrix.to/#/@felix:haecker.io) reports

> I'm happy to announce that Fragments now supports the [most requested feature](https://gitlab.gnome.org/World/Fragments/-/issues/29), namely selective downloading of files within a torrent!
> 
> The development for this took a lot of time, but I'm very pleased with the result. I put a lot of emphasis on scalability, so it doesn't matter if a torrent has 5 files - or 5,000. To make it easier to find individual files within torrents, there is a search feature with the option to adjust the sorting according to your needs.
> 
> I would like to thank Maximiliano 🥑, who [reviewed the huge MR](https://gitlab.gnome.org/World/Fragments/-/merge_requests/133), Tobias Bernard, who helped with the design - and the many people who helped with testing and gave valuable feedback!
> 
> I have some other exciting features planned for the next big release - stay tuned!
> ![](XGvdnRuNVvuapIUUlWFfsJFr.png)
> {{< video src="NXDgOXJBkdFtLtkyZGLnkZue.mp4" >}}

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> Workbench 45.1 is [available on Flathub](https://flathub.org/apps/re.sonny.Workbench)
> 
> Highlights are
> 
> * Reduce download size by relying on runtime extensions
> * Add an “Extensions” window to explain how to install them
> * New Library entry “CSS Gradients”
> * The documentation viewer "Manuals" gained keyboard shortcuts
> * Visual / design refresh
> ![](22b7d2d41983cfe8b0ca283180f5967c58915d28.png)
> ![](75f211feedf4a0ea615c809689dbfcfe29f71e0d.png)
> ![](45d7855540bbd353720fa051531a70ecd53215ed.png)

### Paper Clip [↗](https://apps.gnome.org/PdfMetadataEditor/)

Edit PDF document metadata.

[Diego Iván M.E](https://matrix.to/#/@dimmednerd:matrix.org) reports

> I'm glad to announce that Paper Clip v3.5 is out!
> 
> This is a relatively small release of Paper Clip, but brings some enhancements:
> 
> * Updated and new translations.
> * The app now uses the new Libadwaita widgets, so it follows the look-and-feel of GNOME 45.
> * Paper Clip v3.5 supports multiple windows. Now if you try to open multiple files from the file explorer, it will open all of them in different windows instead of opening just one.
> 
> Get the latest version from [Flathub](https://flathub.org/apps/io.github.diegoivan.pdf_metadata_editor).
> ![](TOXYNfjQJlUirLJAvtHBTvRC.png)

# Third Party Projects

[angeloverlain](https://matrix.to/#/@angeloverlain:matrix.org) announces

> I've released Decibels, an app that lets you play audio files. It has a modern and adaptive interface, with a waveform, simple playback controls, and the ability to control the speed at which the audio is played.
> 
> Install Decibels from [Flathub](https://flathub.org/apps/com.vixalien.decibels) and give it a spin!
> ![](dpGSlISyuXykNKHTHFCEEwvA.png)

[Heliguy](https://matrix.to/#/@heliguy:matrix.org) announces

> Introducing Warehouse, an app that manages installed Flatpaks, their user data, and Flatpak remotes. Warehouse 1.1.1 was recently released on Flathub!
> 
> Some notable features include:
> 
> * Uninstalling Flatpaks in a batch
> * Displaying Flatpaks by what remote they are from
> * Finding unused user data and trashing it, or installing apps that match it
> * Displaying detailed information about apps and runtimes
> * Deleting current remotes and adding new remotes
> 
> https://flathub.org/apps/io.github.flattool.Warehouse
> https://github.com/flattool/warehouse
> ![](PPmQrVGnRFMgYWIkTMqdYwbg.png)
> ![](AbxctUrBPRukJzoxHUPHFHoN.png)
> ![](khiZBQmRTYDlNCaXFSoHIiRz.png)

[دانیال بهزادی](https://matrix.to/#/@danialbehzadi:mozilla.org) reports

> Carburetor 4.1.3 release pushing libadwaita 1.4 and GTK 4.12 for preferences with `AdwSpinRow`, `AdwMessageDialog` and `GtkFileDialog`.
> [Carburetor](https://framagit.org/tractor/carburetor) is a libadwaita app for connecting to the TOR on GNOME mobile phones and desktops.
> ![](13d9d9dfad05bbb27f41cb57fadf41235cb57243.png)

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> I have published a first release of [Snowglobe](https://flathub.org/apps/com.belmoussaoui.snowglobe)! A virtualization viewer built on top libmks and so uses the QEMU over DBus backend.
> ![](a05264c630420404ad14a26791fb701389892fe5.png)

### Turtle [↗](https://gitlab.gnome.org/philippun1/turtle)

Manage git repositories in Nautilus.

[Philipp](https://matrix.to/#/@philippun:matrix.org) says

> Tag it!
> 
> [Turtle 0.6](https://gitlab.gnome.org/philippun1/turtle/-/releases/0.6) has been released and it now supports tags.
> Tags will be shown in the log dialog and can also be created directly in the log or via the Nautilus context menu. There is also new references dialog, which let's you inspect references (including tags).
> 
> It is now possible to directly compare modified files with the index version of it from the Nautilus context menu.
> 
> Additionally colors for the graph and branch/tag labels have been optimized for light theme and many little bugs have been fixed.
> ![](jAtQfDtBUbctIRHfUkdgcQIW.png)
> ![](jIqqZtQJWluWvuYgnvPCIxzv.png)

### Smile [↗](https://github.com/mijorus/smile)

An emoji picker with custom tags.

[Pedro Sader Azevedo](https://matrix.to/#/@toluene:matrix.org) reports

> The companion extension to Smile, which automatically pastes selected emoji, now supports GNOME 45. Install the app from [Flathub](https://flathub.org/apps/it.mijorus.smile) and the extension from [EGO](https://extensions.gnome.org/extension/6096/smile-complementary-extension/) and beam! 😁
> {{< video src="YplkxBzqTDLdCIDtJrRQrPuI.webm" >}}

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) says

> Version 0.32.0 of [Phosh](https://gitlab.gnome.org/World/Phosh) is out with
> 
> * a bunch of bug fixes especially around odly placed or rendered popups on mobile
> * a way for applications to suppress haptic/led feedback when they put out notifications
> * a way to reorder the lock screen plugins
> * support for more devices with notches
> * support for the tablet mode switch as found on some convertibles
> ![](nMyzRWJVyYkMMUwICeJqOUib.png)

### Flowtime [↗](https://github.com/Diego-Ivan/Flowtime)

Spend your time wisely.

[Diego Iván M.E](https://matrix.to/#/@dimmednerd:matrix.org) says

> Flowtime v5.0 is out now!
> 
> This update brings significant cosmetic and functional changes, as well as small bugfixes:
> 
> * The app has been updated to the GNOME 45 runtime, so it now can use the new fancy Libadwaita widgets.
> * The old "Picture in Picture" mode has been replaced by the "Distraction Free Mode". It is virtually the same, but instead of forcing you to a small window size, it will hide all the window clutter so you can focus on the timer.
> * Thanks to the replacement of the *Picture in Picture* mode, the window now shows the top/bottom view switchers depending on its width.
> * The old Statistics view has been replaced by graphs! It provides a nicer way to see how you spend your time.
> * Exporting the statistics to CSV now properly formats the work and break time.
> 
> Get it from [Flathub](https://flathub.org/apps/io.github.diegoivanme.flowtime)
> ![](SqBfwqWMKQRPxCtYVZJpBzRQ.png)
> ![](wJJRbeWHbgTPTMewhVgDTzok.png)

# Shell Extensions

[glerro](https://matrix.to/#/@glerro:matrix.org) announces

> Just released on ego [Debian Linux Updates Indicator](https://extensions.gnome.org/extension/6322/debian-linux-updates-indicator/) for Gnome Shell 40-44. In a few days i release a new version for Gnome Shell 45. 
> 
> Updates indicator for Debian Linux based distributions.
> 
> Check for updates and shows also the following packages status (as in Synaptic):
> * Available updates.
> * New packages in repository.
> * Local/Obsolete packages.
> * Residual config files.
> * Autoremovable packages.

### Focus changer [↗](https://extensions.gnome.org/extension/4627/focus-changer/)

Change focus between windows in all directions using your keyboard.

[Pedro Sader Azevedo](https://matrix.to/#/@toluene:matrix.org) reports

> The extension "Focus changer" has been made compatible with GNOME 45! In addition, it's preferences window has been ported to libadwaita and received support for translations.
> ![](BsnLEiVXJtbitwFqIrKbcnzu.png)

# Miscellaneous

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) reports

> GNOME is once again participating in Outreachy. The application period has begun and potential interns are active in the [#gnome-os:gnome.org](https://matrix.to/#/#gnome-os:gnome.org) channel, learning about openQA and our project to extend the GNOME OS end-to-end tests.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

