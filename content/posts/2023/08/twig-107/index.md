---
title: "#107 Reduced Overheads"
author: Felix
date: 2023-08-04
tags: ["daikhan", "parabolic", "cavalier", "gnome-shell", "workbench", "tagger"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from July 28 to August 04.<!--more-->

# GNOME Core Apps and Libraries

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[hergertme](https://matrix.to/#/@hergertme:gnome.org) says

> Lots of GNOME Shell search providers are reducing runtime and startup overhead thanks to Sysprof Nightly. See my various blog posts about how we find and triage such issues. https://blogs.gnome.org/chergert/2023/07/28/how-to-use-sysprof-again/ https://blogs.gnome.org/chergert/2023/08/02/writing-fast-search/ and https://blogs.gnome.org/chergert/2023/08/04/more-sysprofing/

# GNOME Circle Apps and Libraries

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> Workbench gained the ability to save and load on-disk projects. In addition, we've made it easier to contribute to the library of demos and examples, checkout the [contributing guide](https://github.com/sonnyp/Workbench/blob/main/CONTRIBUTING.md).
> ![](1002282439dedcc79685f478d9a0abee52bf031f.png)

# Third Party Projects

[mibi88](https://matrix.to/#/@mibi88:matrix.org) reports

> I just finished rewriting entirely my Markdown editor, [MibiMdEditor](https://github.com/mibi88/MibiMdEditor), and released the first beta version of it (since I've rewritten it), but it is not just a random Markdown editor, it works by adding scripts that will be called to generate the HTML code, so you can use it to write MarkDown, BBcode, Asciidoc, just by coding a simple script! I'll make a Flatpak soon!
> Stay tuned!

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Evolution `main` just [gained support](https://gitlab.gnome.org/GNOME/evolution/-/issues/375) for [Autocrypt](https://autocrypt.org), a trust-on-first-use protocol for easily enabling encrypted e-mail

### Tagger [↗](https://flathub.org/apps/details/org.nickvision.tagger)

Tag your music.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Tagger is now at [V2023.8.1](https://github.com/NickvisionApps/Tagger/releases/tag/2023.8.1)!
> 
> This release and the previous V2023.8.0 are packed with many bug fixes and some new features as well. Read about them below :)
> 
> Here's the full changelog:
> * Added support for the TrackTotal and BeatsPerMinute tag property
> * Added more sorting options 
> * Improved the advanced search algorithm, supporting accented characters better
> * The apply button will now be shown only when a file has unsaved changes
> * Web Services will now get enabled and disabled depending on the system's internet connection
> * Empty Year, Track, and BPM fields will show an empty string instead of 0
> * Fixed an issue where clearing a tag did not clear all fields
> * Fixed an issue where single album art from other programs was not read by Tagger
> * Fixed an issue where docs were not available when running Tagger via snap
> * Fixed an issue where advanced search may sometimes crash
> * Fixed an issue where submitting to AcoustID would crash
> * Updated translations (Thanks everyone on Weblate!)
> ![](BYoLLbxrsHoKJlnyYAESFdLp.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Parabolic [V2023.8.0](https://github.com/NickvisionApps/Parabolic/releases/tag/2023.8.0) is here!
> 
> In this release we've added select and deselect all buttons to a download's playlist page and added a proxy url setting in Preferences. We've also added a network connection monitor to Parabolic. On the side of bugs, we fixed an issue where docs were not available when running via snap and an issue where History wasn't saved correctly in json.
> 
> Here's the full changelog:
> * Added buttons to select or deselect all items in a playlist
> * Added a proxy setting in Preferences
> * Parabolic will now check for an active network connection
> * Fixed an issue where docs were not available when running Parabolic via snap
> * Fixed an issue where History wasn't saved correctly in json
> * Updated translations (Thanks everyone on Weblate!)
> ![](esSfclFMtYxtXlifwacieLpa.png)

### Daikhan [↗](https://flathub.org/apps/io.gitlab.daikhan.stable)

Play Videos/Music with style.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) says

> I have been working on a new media player named [Daikhan](https://gitlab.com/daikhan/daikhan) (previously Envision Media Player) for some time. I have now released an early access version [as  a flatpak on Flathub](https://flathub.org/apps/io.gitlab.daikhan.stable).
> 
> Unfortunately, the flatpak does not yet support hardware accelerated playback. The player itself however does support it if GStreamer is set up properly.
> 
> Due to the early access nature of the app, there may be some other issues and there **will** be significant changes occurring in the future.
> ![](lulDBgeisjKeBqfQnvwfkWXK.png)
> ![](uwaEQQtEQlxBrTRMOfEGEfhh.png)

### Cavalier [↗](https://flathub.org/apps/details/org.nickvision.cavalier)

Visualize audio with CAVA.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Cavalier [V2023.8.0](https://github.com/NickvisionApps/Cavalier/releases/tag/2023.8.0) is here!
> 
> This release is packed with new features! We added a new drawing mode and the ability to set a background image in the window. We've also added a CLI to Cavalier in which you can change its settings right from the terminal!
> 
> Here's the full changelog:
> * Cavalier can now be controlled from command line. Run the app with --help option to see full list of available options.
> * Reverse mirror option is now available with full mirror.
> * It's now possible to set frames per second to 144 or other custom value.
> * Added anti-aliasing, so rounded items now look less pixelated.
> * Added ability to set background image.
> * New drawing mode - Splitter.
> * Bars limit was increased to 100.
> * Updated translations (Thanks everyone on Weblate!)
> ![](NOpyHttZcZzYynfqZgXOpXcx.png)

# Shell Extensions

[Swapnil Devesh](https://matrix.to/#/@sidevesh:matrix.org) reports

> I released Solaar Extension for GNOME https://extensions.gnome.org/extension/6162/solaar-extension/ , this allows Solaar to support features on Wayland desktops which were previously unsupported, namely the process and mouse process conditions, which allow specific actions to happen on events from Logi keyboards and mice based on which application is currently in focus or currently has the mouse over it respectively. These should start working automatically on Wayland desktops with this extension installed with the next update of Solaar, or you can try the building the latest code from Solaar's repository or from git or nightly releases if available for your distro.

# Miscellaneous

[Jordan Petridis](https://matrix.to/#/@alatiera:matrix.org) announces

> I wrote a blogpost about the magic of systemd-sysext and how it can be used to develop system components
>  https://blogs.gnome.org/alatiera/2023/08/04/developing-gnome-os-systemd-sysext/
> {{< video src="HKWPuYmudGFWfqKxGBSbcpCI.webm" >}}

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

