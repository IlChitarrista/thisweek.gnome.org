---
title: "#109 Managing Repositories"
author: Felix
date: 2023-08-18
tags: ["gradience", "denaro", "gdm-settings", "cartridges", "fractal", "glib", "turtle", "daikhan"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from August 11 to August 18.<!--more-->

# GNOME Core Apps and Libraries

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> macOS CI has been re-enabled for GLib, thanks to some welcome help and support from René de Hesselle

# GNOME Circle Apps and Libraries

[Alan](https://matrix.to/#/@adbeveridge:matrix.org) reports

> File Shredder [v2.0.0](https://github.com/ADBeveridge/raider/releases/tag/v2.0.0) was finally released, the last version being released in October of last year. 
>  - The preferences dialog has been removed.
>  - File Shredder now uses custom code that is included directly in the source.

### Cartridges [↗](https://github.com/kra-mo/cartridges)

Launch all your games

[kramo](https://matrix.to/#/@kramo:matrix.org) reports

> This week, I released Cartridges 2.2!
> 
> * RetroArch support has been added, huge thanks to [Rilic](https://github.com/RilicTheFox)
> * Added the option to automatically clean up uninstalled games on import
> * Added the ability to undo an import, just to be safe :)
> 
> Check it out on [Flathub](https://flathub.org/apps/hu.kramo.Cartridges)!
> ![](GOxgIpdxNoIIesXtMrShDEdJ.png)

# Third Party Projects

### Turtle [↗](https://gitlab.gnome.org/philippun1/turtle)

Manage git repositories in Nautilus.

[Philipp](https://matrix.to/#/@philippun:matrix.org) says

> Turtle 0.4 released!
> 
> [Turtle](https://gitlab.gnome.org/philippun1/turtle) is a tool to manage git repositories within Nautilus by providing emblems and context menus. For more complex operations it provides specific dialogs, i.e. commit, sync, log, settings, etc.
> 
> [Version 0.4](https://gitlab.gnome.org/philippun1/turtle/-/releases/0.4) marks a huge leap forward in its development: Almost all git operations can now be performed by Turtle in a convenient and graphical way.
> ![](ELyJHWRMuqHHvTuBfieLGKRP.png)

### Gradience [↗](https://github.com/GradienceTeam/Gradience)

Change the look of Adwaita, with ease.

[Daudix UFO](https://matrix.to/#/@daudix_ufo:matrix.org) says

> Gradience 0.8.0-beta1 is finally available for download from Flathub Beta! it took a bit longer than we expected, but everything should be good now.
> 
> To install it, add `flathub-beta` remote and install Gradience from it:
> 
> ```
> flatpak remote-add --if-not-exists flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo
> ```
> 
> ```
> flatpak install flathub-beta com.github.GradienceTeam.Gradience
> ```
> 
> To run the beta you might need to use this command:
> 
> ```
> flatpak run --branch=beta com.github.GradienceTeam.Gradience
> ```
> 
> See [previous TWIG update](https://thisweek.gnome.org/posts/2023/06/twig-100/#gradience) for more info about 0.8.0-beta1
> ![](VJJOrRCfyvxpgKgVJUEHWjgk.png)

### Login Manager Settings [↗](https://gdm-settings.github.io)

Customize your login screen.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) announces

> GDM Settings [v3.3](https://github.com/gdm-settings/gdm-settings/releases/tag/v3.3) was released with fixes for the following two bugs.
> 
> * Changing background color didn't work on GNOME 44
> * Background image was broken on widescreen/dual monitor setups with GNOME 44

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) announces

> Fractal 5.beta2 is out!
> 
> Fractal 5.beta2 is the second beta release 🎉 since the rewrite of Fractal to take advantage of GTK 4 and the Matrix Rust SDK, an effort that started in March 2021.
> 
> The most visible changes since Fractal 5.beta1 are:
> 
> * Editing text messages ✏️
> * Logging in with homeservers that don’t support auto-discovery 🧐
> * A refactor of the login flow should avoid crashes when going back at any step 🔙
> * Sometimes two day dividers 📅 would appear next to each other without messages between them, this is now fixed
> 
> Of course, there are a also a lot of less visible changes, notably a lot of refactoring, 🐛 fixes and translations thanks to all our contributors, and our upstream projects.
> 
> As the version implies, this is still considered beta stage and might trigger crashes or other bugs 😔 but overall should be pretty stable 👍. It is available to install via Flathub Beta 📦, see the [instructions in our README](https://gitlab.gnome.org/GNOME/fractal#beta-version).
> 
> A list of blocking issues for the release of version 5 can be found in the [Fractal 5 milestone](https://gitlab.gnome.org/GNOME/fractal/-/milestones/18) on GitLab. All contributions are welcome!
> ![](8857273ee97ca951e90d8661d5351ff3e01a7cf2.png)

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

Manage your personal finances.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Denaro [V2023.8.0](https://github.com/NickvisionApps/Denaro/releases/tag/2023.8.0) is here! This release comes after months of hard work and includes many new features, **including graphs!**, and many fixes that make Denaro an even better money manager! Read about all the changes below :)
> 
> Here's the full changelog:
> * Added graph visuals to the Account view as well as in exported PDFs
> * Added tags that can be used with transactions for finer management and filtering
>     - Thanks @fsobolev  
> * Added the option to select the entire current month as the date range filter
>     - Thanks @CoffeeIsLife87 !
> * Added reminders for upcoming transactions
> * Improved the transaction description suggestion algorithm with fuzzy search
> * Fixed an issue where the help button in the import toast was not working
> * Fixed an issue where Denaro would crash if an account had incorrect formatted metadata
> * Fixed an issue where docs were not available when running Denaro via snap
> * Updated translations (Thanks to everyone on Weblate)!
> ![](GgdbYwcqUCoTFocHfAXshuEf.png)
> ![](lCbgYXTUkMhuIeIZCNnzaeZm.png)

### Daikhan [↗](https://flathub.org/apps/io.gitlab.daikhan.stable)

Play Videos/Music with style.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) announces

> TWIG-Bot 
> * Daikhan received translation support and has already been translated into Turkish thanks to Sabri Ünal. Translations are done [through Hosted Weblate](https://hosted.weblate.org/projects/daikhan).
> * Dropping multiple files into the player window works as expected now. Previously, only the last file would play.
> * The app shows a dialog explaining the situation if the user tries to open an unsupported file type e.g. a document or a supported file type (video or audio) with unsupported codec.
> 
> The flatpak received support for more codecs but it won't/can't be released [until the Flathub build bot is fixed](https://github.com/flathub/flathub/issues/4427).
> ![](MpZOVKRkUTiTdIaytyRagwjh.png)

# GNOME Foundation

[Sonny](https://matrix.to/#/@sonny:gnome.org) reports

> The following GNOME Circle requirement has been removed "No existing non-profit fiscal sponsor". It came from an ambiguity in our software policy that is now solved.
> GNOME Circle developers are welcome to do fundraising how they see fit. Thanks to Sophie (she/her) for raising this issue with the board.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

