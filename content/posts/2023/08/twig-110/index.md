---
title: "#110 Nailing Down Perfomance Issues"
author: Felix
date: 2023-08-25
tags: ["gnome-shell", "workbench", "sysprof", "gtk", "parabolic", "denaro"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from August 18 to August 25.<!--more-->

# GNOME Core Apps and Libraries

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> Thanks to René de Hesselle from the Inkscape community, GTK has a new macOS CI runner

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Just Perfection](https://matrix.to/#/@justperfection:gnome.org) announces

> [GNOME Shell 45 port guide](https://gjs.guide/extensions/upgrading/gnome-shell-45.html) is out.
> Since GNOME Shell 45 moved to ESM, we want all developers to port their extensions before 45.0 release (September 20th) so they can report to us about the elements needed to be exported (in case they are not exported already).
> 
> We also offer our help to all developers on [GNOME Discourse](https://discourse.gnome.org/) and GNOME Extensions Matrix channel:
> [GNOME Matrix Channel](https://matrix.to/#/#extensions:gnome.org)
> IRC Bridge: irc://irc.gimpnet.org/shell-extensions

# GNOME Development Tools

### Sysprof [↗](https://gitlab.gnome.org/GNOME/sysprof)

A profiling tool that helps in finding the functions in which a program uses most of its time.

[hergertme](https://matrix.to/#/@hergertme:gnome.org) says

> Sysprof gained the ability to show you what processes were scheduled per-CPU. You can use this to more effectively nail down performance issues on the desktop.
> ![](dd21333e050f2379c6a69d0b0d4644356d7fa08d.png)

# GNOME Circle Apps and Libraries

[Clara Hobbs (she/they)](https://matrix.to/#/@plum-nutty:matrix.org) announces

> Thanks to the new AdwBreakpoint API in libadwaita 1.4, Chess Clock just merged improved support for larger screen sizes.  The clock text is larger when the window is big enough to support it, and portrait mode is enabled whenever the window has a portrait aspect ratio (rather than requiring it to be as narrow as a portrait phone screen).  These updates should make the app nicer to use on tablets and laptops.
> ![](cxWIMnRLNZbtfEHcwXBEyDbl.png)

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) reports

> Rust support landed in Workbench 🛠️ ❤️ 🦀 Thanks to Julian 🍃 !
> We will work on formatter and language server soon.
> 
> We are approaching 100 Library demos. Porting them is a great opportunity to learn or work with Rust, specially if you are coming from JavaScript or Vala. Checkout [our guide](https://github.com/sonnyp/Workbench/blob/main/CONTRIBUTING.md#update-library-entry) and don't hesitate to come by [#workbench:gnome.org](https://matrix.to/#/#workbench:gnome.org)
>
> ![](b49206abaf1bfe3f.png)

# Third Party Projects

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> A new release of [libmks](https://gitlab.gnome.org/GNOME/libmks) is out! Adding support for touch events, improving the rendering which would avoid frame drops/incorrect regions updates. Details at https://gitlab.gnome.org/GNOME/libmks/-/releases/0.1.1. I also wrote a blog post recently about my adventures to get damage area propagated from the guest to the host which you can read at https://belmoussaoui.com/blog/16-damage-areas-across-the-virtio-space/
> {{< video src="354bf2da8a0e1cf1b7e16e391316d7e110777cf4.webm" >}}

[Nokse](https://matrix.to/#/@nokse22:matrix.org) announces

> This week, I've released [ASCII Draw](https://github.com/Nokse22/ascii-draw), an app that lets you draw diagrams, flowcharts, or anything else using nothing but characters. It has many lines styles to choose from.
> 
> There are multiple tools available:
> * **Straight Lines** and **Arrows** to connect points on your graphs
> * **Rectangle** to construct outlines effortlessly
> * **Freehand Lines** to draws connected lines following your mouse movement
> * **Text** to easily preview and write text on your canvas
> * **Eraser**, **Picker**, **Filled Rectangle** and **Freehand Brush**
> ![](EzqvxUpSVhSntQcSfHeEHmTa.png)
> ![](wmOrHQFagPgajtZECVFvydkO.png)
> ![](DinyxulVFgMXXiTYLwyLdHQb.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Parabolic [V2023.8.3](https://github.com/NickvisionApps/Parabolic/releases/tag/2023.8.3) is here!
> 
> This release includes some new feature to make Parabolic even more configurable. Read about them below :)
> 
> Here's the changelog:
> 
> * Added a new advanced download option to split chapters
> * Added a new preference to enable SponsorBlock for YouTube downloads
> * Updated translations (Thanks everyone on Weblate!)
> ![](ffbuUGaTqlwrZFscPbtzyqKY.png)

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

Manage your personal finances.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Denaro [V2023.8.1](https://github.com/NickvisionApps/Denaro/releases/2023.8.1) is here!
> 
> This release includes some new features and fixes that you can read about below :)
> 
> Here's the changelog:
> 
> * Added a password strength indicator when creating an account password
> * Added an Amount Display Style option to custom currency settings
> * Fixed an issue where selecting the current month on an empty account in the calendar would cause a crash
> * Fixed an issue where adding receipts to a new transaction would cause a crash
> * Improved UI/UX
> * Updated translations (Thanks to everyone on Weblate)!
> ![](cBJSFzQsweMkTNOHDqtwFxZJ.png)

# Miscellaneous

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) announces

> GNOME 45 is reaching its next step in [release preparation](https://wiki.gnome.org/FortyFive). After three weeks of UI freeze, on Saturday, Aug 26, GNOME 45 will also enter string freeze. After 23:59 UTC that day, all string changes in Core components will require approval from the i18n team.

# GNOME Foundation

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) announces

> With GUADEC 2023 completed Foundation staff have been busy wrapping up final conference tasks while moving forward with planning GNOME.Asia 2023, LAS 2024, and GUADEC 2024. Some of these tasks include collecting feedback from our [post-event surveys](https://events.gnome.org/event/101/surveys/), sorting through conference photos, and providing updates to our event sponsors. We’re currently working on finding the best solution for sharing the conference photos with everyone and will provide more updates on that soon! We’re also looking for community feedback on how we structure GUADEC 2024 talk and BoF days. Let us know if you would prefer to attend GUADEC talks on weekdays or over the weekend by answering our poll [here](https://discourse.gnome.org/t/poll-should-we-move-guadec-talks-to-the-weekend-or-keep-them-on-weekdays/16772).
> 
> Outside of events, we’ve been wrapping up the 2021-2022 Annual Report which is now available on the [Foundation website](https://foundation.gnome.org/reports/), and getting started on a new video for the upcoming GNOME 45 release.
> 
> Reminder:
> The GNOME.Asia 2023 Call for Participation is open! If you would like to submit a talk to this year’s summit please read [our news post](https://foundation.gnome.org/2023/08/08/gnomeasia-2023-cfp/) for more details. All applications are due by September 1, 2023.
> 
> Volunteer Opportunity:
> We’re live-streaming the GUADEC talks, but are looking for a volunteer to edit the final footage into individual talk videos. If that sounds like a project you’re interested in let us know by emailing [chenriksen@gnome.org](mailto:chenriksen@gnome.org).

[feborges](https://matrix.to/#/@felipeborges:gnome.org) reports

> The GNOME Foundation is looking for Mentors and projects for the Outreachy December-March internships. If you are interested in mentoring, please visit https://discourse.gnome.org/t/deadline-sept-20-2023-call-for-mentors-for-outreachy-december-23-march-24-cohort/16748 for more info.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
