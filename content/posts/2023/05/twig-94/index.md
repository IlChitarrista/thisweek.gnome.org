---
title: "#94 Configuring Columns"
author: Felix
date: 2023-05-05
tags: ["gtk-rs", "flare", "newsflash", "loupe", "nautilus"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from April 28 to May 05.<!--more-->

# GNOME Core Apps and Libraries

### Files [↗](https://gitlab.gnome.org/GNOME/nautilus)

Providing a simple and integrated way of managing your files and browsing your file system.

[antoniof](https://matrix.to/#/@antoniof:gnome.org) reports

> There is a new interface for configuring the columns of Files list! After almost 20 years of the same column chooser, the deprecation of GtkTreeView encouraged Corey Berla to replace the column chooser with one containing modern widgets and design. This enhancement, with additional contributions by Peter Eisenmann,
> also allows for changing visible columns either globally or only for the current folder, without the old misleading interface duplication in the Preferences.
> ![](e4674f4993afa2b8bb1b81bbd0e1c57f38a39881.png)
> ![](c27a744146b7d5d8218cfd7c93e6a2cbb427b065.png)

# GNOME Incubating Apps

### Loupe [↗](https://gitlab.gnome.org/Incubator/loupe)

A simple and modern image viewer.

[Sophie 🏳️‍🌈 🏳️‍⚧️ ✊](https://matrix.to/#/@sophieherold:gnome.org) reports

> While it hasn't been a priority for this cycle, Loupe should already deliver a solid experience on mobile devices with this week's update. Apart from the complete interface being now adaptive, typical features like one-finger swipe, double-tap to zoom in and out, pinch zoom, and panning are already supported.
> 
> This week's update includes:
> 
> * Adopt the properties view for smaller form factors using the latest libadwaita [Breakpoint](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.Breakpoint.html) feature.
> * Hide the HeaderBar and mouse cursor in fullscreen after a moment.
> * Skip unsupported image formats when browsing images.
> * Many more minor tweaks and fixes.
> ![](b12209e7c0237798e9fe94a285f7a9f3d664a0ea.png)

# GNOME Circle Apps and Libraries

### NewsFlash feed reader [↗](https://gitlab.com/news-flash/news_flash_gtk)

Follow your favorite blogs & news sites.

[Jan Lukas](https://matrix.to/#/@jangernert:matrix.org) announces

> NewsFlash 2.3.0 was released today. The only visible change is the fancy new icon by Tobias Bernard. Under the hood WebKitGTK was upgraded to the latest version, which should fix a lot of problems.
> But the most work went into the new content grabber. It should be a lot faster than the javascript library used before. It is better [documented](https://gitlab.com/news-flash/article_scraper) and it is easier to [provide custom extraction rules](https://gitlab.com/news-flash/news_flash_gtk/-/blob/master/README.md#grab-full-articles).
> ![](IsRXLxmLjzPKiEduBTWBNvIH.png)

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> The Rust bindings generator [gir](https://github.com/gtk-rs/gir) is now capable of embedding docs for [virtual methods](https://gtk-rs.org/gtk4-rs/git/docs/gtk4/subclass/widget/trait.WidgetImpl.html#method.grab_focus) and [class methods](https://gtk-rs.org/gtk4-rs/git/docs/gtk4/subclass/widget/trait.WidgetClassExt.html#method.css_name) from the corresponding GIR files.

# Third Party Projects

[0xMRTT](https://matrix.to/#/@0xmrtt:envs.net) reports

> I've released Bavarder, an app for chatting with AI. With Bavarder, you can ask a question to differents providers like Hugging Chat, BAI Chat, OpenAI GPT-3.5-turbo, etc. It has been designed to be minimalist for having access to a chatbot without a browser or an account. 
> 
> Remember that AI can produce fake contents and should not be used in a fraudulous way.
> 
> You can download Bavarder from [Flathub](https://flathub.org/apps/io.github.Bavarder.Bavarder) or from either [Github](https://github.com/Bavarder/Bavarder) or [Codeberg](https://codeberg.org/Bavarder/bavarder)
> ![](a3cc6e76975e0209ba5aa0a6de8209fe89d48974.png)

### Flare [↗](https://flathub.org/apps/details/de.schmidhuberj.Flare)

An unofficial Signal GTK client.

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) reports

> Flare 0.8.0 was released which brings big improvements to the message list. Instead of the previous "Load More"-button, content now gets dynamically loaded if needed leading to an improved experience using Flare with longer chats. Since the last update, message deletion has now also been implemented. And, as always, many bug fixes and minor features have also been developed to make sure Flare works as expected.
> ![](KNsUtLBtdjlKeVZynhGZQrov.png)

# GNOME Foundation

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) says

> This week, the GNOME Foundation has been wrapping up LAS 2023 tasks and focusing on GUADEC organization. We recently shared the full schedule of talks, which can be viewed on [guadec.org](https://www.guadec.org/), and hope to share more details about social events and keynote speakers soon. One fun event item I’ve been working on is the design for GUADEC 2023 t-shirts! We’ll share that on [shop.gnome.org](https://shop.gnome.org/) as soon as it’s ready.
> 
> Registration is now open for GUADEC 2023! Let us know you’re attending, either in-person in Riga, Latvia, or remotely by signing up online. More details and links can be found on [GNOME Foundation News](https://foundation.gnome.org/2023/05/04/guadec-2023-registration-is-open/).
> 
> We’re still looking for GUADEC 2023 sponsors! If you or your company would like to sponsor this year’s conference, you can find our brochure and learn more on [guadec.org](https://events.gnome.org/event/101/page/167-sponsors).

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

