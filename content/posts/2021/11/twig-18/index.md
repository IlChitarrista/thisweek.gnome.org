---
title: "#18 Delicious toasts"
author: Felix
date: 2021-11-12
tags: ["gtk-rs", "nautilus", "gnome-shell", "libadwaita", "telegrand", "gnome-control-center", "tracker", "gnome-builder", "health"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from November 05 to November 12.<!--more-->

# Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> Maximiliano has [implemented](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/235) toasts in libadwaita - an easier to use and sleeker replacement for the old in-app notifications pattern that never got a specialized widget
> ![](481d96eee8e7f42bcb30ba75a0c37000f3eb128a.png)

### Files [↗](https://gitlab.gnome.org/GNOME/nautilus)

Providing a  simple and integrated way of managing your files and browsing your file system.

[antoniof](https://matrix.to/#/@antoniof:gnome.org) says

> Joshua Lee has ported the file conflict dialog to GtkBuilder templates. This is part of the effort to get the Files application ready for GTK 4, but also gets us one step closer to the proposed redesign of this dialog.
> ![](ba8139b2b426019ca321dd3db4bad553ce3b0bae.png)

> Peter Eisenmann and Allan Day have enhanced the Rename popover with a new design. Now the popover can grow wider for long file names.
> ![](a94ff9a17f83d49accd741a19cc8eafb97a131eb.png)

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) says

> The work-in-progress [new GNOME Shell screenshot UI](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/1954) had a bit of polish done this week.
> 
> The corner handles for resizing are back with a cleaner look thanks to guidance from Tobias Bernard.
> 
> The screenshot area is now preserved between screenshots, making it easy to capture the same window in different configurations (e.g. light vs. dark style). Left-click-dragging anywhere outside of the screenshot area now lets you draw a new area, which is indicated with a crosshair cursor. To accommodate this, the starting screenshot area has been made smaller. It will also appear correctly on the primary monitor, even if it's not the leftmost one.
> 
> The screenshot cursor will no longer sometimes change when opening the UI, and it will no longer appear blurry.
> 
> Finally, when there are no open windows to screenshot, the window selection button will be inactive.
> {{< video src="015b84bf51c54274c1144f640de4a179d8920cb0.webm" >}}

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) reports

> A few weeks ago, I started porting GNOME Settings - and its dependencies - to GTK4. I'm porting each panel individually. This week I managed to port 3 more panels: the Color, Display, and Background panels. With that, 27 out of 32 panels are ported.
> ![](f56f7e79cca1f2b6dc5e2b69c73391c7bcb8d32c.png)
> ![](4614d99ef93b6ebeebd41e1d987090d85033284c.png)
> ![](97cadeffbe634e80bca45e80ec11b504fdbbc87b.png)

### GNOME Builder [↗](https://gitlab.gnome.org/GNOME/gnome-builder)

IDE for writing GNOME-based software.

[gwagner](https://matrix.to/#/@gwagner:gnome.org) says

> GNOME Builder contains now Gtk4 templates for Rust, Python and Vala thanks to Marco, Tim and Yotam. 
> 
> Georg Vienna landed fantastic features for our language servers this week: 
> * a typescript language server plugin using tsls (testing appreciated. Especially container usage is being worked on so we are happy about any user reports)
> * code actions and workspace edit. Code Actions allow language specific refactorings or specific fixes to the codebase computed by the language server and the later allows projectwide renaming of symbols. (Details and Previews can be found in the merge request https://gitlab.gnome.org/GNOME/gnome-builder/-/merge_requests/461) Rust, Python and the TypeScript Plugin already use this facility.
> 
> Thanks to Tomi GNOME Builder offers now a clang-format powered "format on save" option in order to properly format your code with a dedicated .clang-format file. Also the build output pane can now be configured to clean before a build. So you can easily inspect only the build informations from one build.
> 
> The mono gtk3 template got a bit modernized and aligned to our other templates.

### Tracker [↗](https://gitlab.gnome.org/GNOME/tracker/)

A filesystem indexer, metadata storage system and search tool.

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) reports

> In Tracker, Carlos Garnacho fixed a  query cancellation issue which will improve performance when searching in Nautilus. Thanks to everyone who helped triage https://gitlab.gnome.org/GNOME/tracker/-/issues/264

# Circle Apps and Libraries

### Health [↗](https://gitlab.gnome.org/World/Health)

Collect, store and visualise metrics about yourself.

[Cogitri](https://matrix.to/#/@cogitri:cogitri.dev) says

> Health 0.93.3 has been released today with some bug fixes. This release of Health makes it adaptive to smaller screens (again), has nicer colours for its calories overview (thanks to Color Palette's nice colour picker)  and features some updated and new translations

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Julian Hofer](https://matrix.to/#/@julianhofer:gnome.org) says

> Aaron Erhardt and I added [installation instructions](https://gtk-rs.org/gtk4-rs/stable/latest/book/installation.html) for Linux, macOS and Windows to the gtk4-rs book.
> 
> If you are a macOS or Windows developer and have suggestions on how to make the instructions more idiomatic feel free to open an [issue](https://github.com/gtk-rs/gtk4-rs/issues/new/choose).
> 
> Especially for macOS it would already help to simply follow the instructions and check if everything works as expected.
> ![](0d61db43bcb3afc0208acbb05d376a73ec3cc218.png)

# Third Party Projects

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) says

> Thanks to the Endless Orange Week, this week I could work on various portal-related tasks. Here are a few highlights:
> 
>  * [Added introspection](https://github.com/flatpak/libportal/pull/54) to `libportal`, and [introduced `libportal-gtk3`, `libportal-gtk4`, and `libportal-qt5`](https://github.com/flatpak/libportal/pull/53), with toolkit-specific functions.
>  * [I'm extending the "Device" portal](https://github.com/flatpak/xdg-desktop-portal/pull/659) to allow opening PipeWire connections that filter all but the allowed devices. This allows sandboxed applications to not expose the `xdg-run/pipewire-0` socket unfiltered, which gives access to many devices like cameras and microphones without the corresponding permissions.
>  * Revisited my implementation of [screencast session restore](https://github.com/flatpak/xdg-desktop-portal/pull/638). This introduces a mechanism for applications to restore previous screencast sessions without querying the user again. I also [implemented support for it in GNOME portals](https://gitlab.gnome.org/GNOME/xdg-desktop-portal-gnome/-/merge_requests/14), and a proof of concept in OBS Studio.
> {{< video src="7435dae9f85d39d670cd592c193c556cb7651f27.webm" >}}

### Telegrand [↗](https://github.com/melix99/telegrand/)

A Telegram client optimized for the GNOME desktop.

[Marco Melorio](https://matrix.to/#/@melix99:gnome.org) reports

> Another week of good progress has passed in Telegrand! Marcus Behrendt continued his streak of contributions: he [made the unread count badges gray](https://github.com/melix99/telegrand/pull/137) for muted chats, added previews for new chat types, like [calls](https://github.com/melix99/telegrand/pull/128) and [join/leave statuses from a group](https://github.com/melix99/telegrand/pull/130) and also added a [cool indicator](https://github.com/melix99/telegrand/pull/123) to the avatars to show when an user is online.
> 
> On the other hand, I have made fixes to the default window size and have also implemented a preferences window with a setting to enable the dark theme. It also has an option to follow the system-wide dark style preference that will come in GNOME 42!
> ![](98277a7b576bae6258f9b641c69e78303f6ed76e.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

