---
title: "#24 Future of Files"
author: Felix
date: 2021-12-30
tags: ["gnome-shell", "gnome-builder", "health", "tangram", "kgx", "junction", "nautilus"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the last two weeks from December 17 to December 30.<!--more-->

# Core Apps and Libraries

### Files [↗](https://gitlab.gnome.org/GNOME/nautilus)

Providing a  simple and integrated way of managing your files and browsing your file system.

[antoniof](https://matrix.to/#/@antoniof:gnome.org) reports

> The plan of porting Files to GTK4 is moving through the gears.
> 
> With the valuable help of new contributor Matt Jakeman, the application no longer uses the `gtk_dialog_run()` blocking function.
> 
> Dependency on libgd has been dropped. Event controllers have been further adopted.
> 
> And a new, simpler yet more featureful, GTK4-ready pathbar has been prepared. Its current look is not final, because it's bound to change profoundly with the new Adwaita style.
> ![](95047908204f10d24415b6721e984a30d5230cdd.png)

[antoniof](https://matrix.to/#/@antoniof:gnome.org) says

> Newcomer Manny has enhanced the "Compress..." feature in Files. The newly created compressed archives are now added to the list of Recent Files. Finding that .zip file to attach to an email has just become a lot easier!

### GNOME Console [↗](https://gitlab.gnome.org/GNOME/console)

A simple user-friendly terminal emulator

[Zander Brown](https://matrix.to/#/@zbrown:matrix.org) reports

> KGX is now branded as Console and has moved to the [GNOME/ namespace](https://gitlab.gnome.org/GNOME/console)

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) reports

> The [new GNOME Shell screenshot UI](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/1954) got a few more improvements over the past few weeks.
>   
> I've added a search entry for taking a screenshot, making it a little more discoverable. Window selection now highlights the selected window more prominently with a tick and a light-blue background, so that it's easier to see. Additionally, windows on the primary monitor are now positioned slightly higher to reduce overlap with the screenshot UI panel. Clicking on the screenshot notification will open the screenshot in the default image viewer.
> 
> The Shift+Ctrl+Alt+R key combination now opens the screenshot UI in the screen recording mode. Upon finishing a screencast, you'll get a notification like with screenshots. Finally, screencasts are now stored in the Videos/Screencasts/ sub-folder so as not to pollute the Videos directory.
> 
> I've thus finished all screenshot UI functionality planned for the GNOME 42 release, making it ready for code review.
> {{< video src="2dad63389993cce1a30aec489a65f16b40d92050.webm" >}}

### GNOME Builder [↗](https://gitlab.gnome.org/GNOME/gnome-builder)

IDE for writing GNOME-based software.

[James Westman](https://matrix.to/#/@flyingpimonster:matrix.org) reports

> Blueprint, a new markup language for creating user interfaces with GTK, now has syntax highlighting and completion support in Builder. For more information you can find the language's documentation [here](https://jwestman.pages.gitlab.gnome.org/blueprint-compiler/).

# Circle Apps and Libraries

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) reports

> This week [Junction](https://apps.gnome.org/app/re.sonny.Junction/) entered GNOME Circle. Junction lets you always choose the application to open certain file types and links. Congratulations!
> ![](07720bf8989d6bd8727f03110210ed382ead4db0.png)

### Junction [↗](https://github.com/sonnyp/Junction)

Lets you choose the application to open files and links.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) announces

> [Junction](https://apps.gnome.org/app/re.sonny.Junction/) 1.4.0 was released with desktop actions and snap applications support.
> ![](vzcJarSaHNJlDmKNnHqAtATi.png)

### Tangram [↗](https://github.com/sonnyp/Tangram)

A browser for your pinned tabs.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) announces

> [Tangram](https://apps.gnome.org/app/re.sonny.Tangram/) was ported to GTK 4 / libadwaita and [available to translate on Weblate](https://hosted.weblate.org/engage/tangram/).
> ![](wyTkyXfGCzMFMStSsXPfiYAh.png)

### Health [↗](https://gitlab.gnome.org/World/Health)

Collect, store and visualise metrics about yourself.

[Cogitri](https://matrix.to/#/@cogitri:cogitri.dev) says

> [Health](https://gitlab.gnome.org/World/Health/) has received a major rework in its handling of data sources. Each data sources (like current weight, steps taken, activities done, calories burned) has been split into an individual plugin so the user can decide what data is relevant to them and only enable that subset of data sources. This should allow for Health to add more specific data sources soon, like blood pressure measurements. The UI of the individual plugins hasn't been finalized yet, but the implementation itself is working pretty well already.
> ![](XUESFhwgihfeMItOZwIFrutL.png)

# Documentation

[Maximiliano](https://matrix.to/#/@msandova:gnome.org) announces

> I ported the documentation of [libportal](https://github.com/flatpak/libportal) to gi-docgen, and added Vala bindings on CI!, you can find the docs [here](https://flatpak.github.io/libportal/index.html).
> ![](0e97441676c46427f91aae11e4737a39c3260c2f.png)

# That’s all for this ~~week~~ year!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects! We wish everyone a good start to the new year 2022 🎆!
