---
title: "#23 Modernized Settings"
author: Felix
date: 2021-12-17
tags: ["gtk-rs", "libadwaita", "gnome-control-center", "cawbird", "glib"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from December 10 to December 17.<!--more-->

# Core Apps and Libraries

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) says

> This week, the GTK4 / libadwaita port of GNOME Settings landed. This was a massive port, with more than 330 files rewritten or adjusted to GTK4. All but 3 panels were ported - we'll be porting the remaining panels and their dependencies soon.
> ![](1087c6af75214d1de6c62e70000d312839f7eaeb.png)
> ![](8ceafeee336eea011ac5329d5d027cfa411e5544.png)
> ![](47bddf5d44285be3d869f98c97cbc88a59bea7f4.png)
> ![](ed9efe4e9729481dc90529bc8eac47b9e7e2db3c.png)

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) reports

> libadwaita finally documents the [style classes](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/style-classes.html) and [named colors](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/named-colors.html) its stylesheet provides

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> Michael Catanzaro has [fixed a devious bug with FD remapping](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/1968) in `g_spawn_*()` in GLib, and included full unit tests for a very satisfying fix

# Circle Apps and Libraries

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Julian Hofer](https://matrix.to/#/@julianhofer:gnome.org) reports

> I have added a new [chapter](https://gtk-rs.org/gtk4-rs/stable/latest/book/css.html) in the gtk4-rs book. It explains how to style your application with CSS. The chapter has been reviewed by Ivan Molodetskikh, Bilal Elmoussaoui, Alexander Mikhaylenko and Sabrina.
> ![](5436a3ab24bc9b36d122f45ebe26ed31de1a88f3.png)
> {{< video src="865cdaec33c7cc2e730dbe9054489ca4d18640ac.webm" >}}

# Documentation

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> The GNOME developer documentation website has a new style guide for writing consistent developer documents like API references and tutorials: https://developer.gnome.org/documentation/guidelines/devel-docs.html

# Miscellaneous

[federico](https://matrix.to/#/@federicomena:matrix.org) reports

> at-spi2-core now has a [CI pipeline](https://gitlab.gnome.org/GNOME/at-spi2-core/-/merge_requests/63); expect many updates to the accessibility stack soon.

# Third Party Projects

### Cawbird [↗](https://ibboard.co.uk/cawbird/)

A native Twitter client for your Linux desktop.

[CodedOre](https://matrix.to/#/@coded_ore:matrix.org) announces

> The rewrite of Cawbird, a native Twitter Client, has gone one step forward with the inclusion of the user overview display!
> 
> This is another step towards completing the complete rewrite of backend and frontend for GTK4 and libadwaita.
> 
> You can check it out here: https://github.com/CodedOre/NewCaw
> ![](UuUvienfeiDVoeCJjdZfmEHS.png)

# GNOME Shell Extensions

[Simon Schneegans](https://matrix.to/#/@schneegans:matrix.org) reports

> The new [Burn-My-Windows extension](https://extensions.gnome.org/extension/4679/burn-my-windows) lets you disintegrate your apps the old-school way. When I released the [Desktop Cube Extension](https://extensions.gnome.org/extension/4648/desktop-cube), many people requested to revive one of the most useless features of Linux desktop history: Setting windows on fire! Here you go...
> {{< video src="ALcnSDcxfvbhnPwywqQdQIlD.mp4" >}}

# That’s all for this week!

See you again on December 30, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects! We wish everyone happy holidays! 🎄
