---
title: "#22 Spring Time...?"
author: Felix
date: 2021-12-10
tags: ["mutter", "libadwaita", "squeekboard", "gnome-shell", "phosh", "fragments", "newsflash", "video-trimmer"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from December 03 to December 10.<!--more-->

# Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> just in time for beta, the second part of Manu 's work has landed and libadwaita now has physics-based spring animations. Leaflet, flap and carousel have all been updated to use them, and swipe tracker now provides velocity to pass into a spring animation, rather than a duration for a timed animation.
> {{< video src="3692f2cf4c0a3dbb0ebfb5969c17e6d1ba3fdb73.webm" >}}

### Mutter [↗](https://gitlab.gnome.org/GNOME/mutter/)

A Wayland display server and X11 window manager and compositor library.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) announces

> Thanks to Carlos Garnacho, Mutter now sends input events at the device rate to applications. This should significantly increase perceived responsiveness for games and artistic applications. You can read more here: https://blogs.gnome.org/shell-dev/2021/12/08/an-eventful-instant/

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[YaLTeR](https://matrix.to/#/@yalter:gnome.org) reports

> The work-in-progress new GNOME Shell [screenshot UI](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/1954) is slowly but surely making its way to completion.
> 
> I added an area indicator during screencast recording making the boundaries easy to see. Also, screencasts no longer produce files with broken length.
> 
> The screenshot/screencast switch and the preferences button got keyboard hotkeys, and window selection got arrow navigation, making the screenshot UI fully keyboard-accessible. New tooltips show the hotkeys, making them easier to learn.
> 
> Finally, the screenshot UI mode is now preserved across screenshots letting you quickly take multiple window screenshots for example.
> {{< video src="b143621941775e0bd0c000a77799b12a06848231.webm" >}}

# Circle Apps and Libraries

### Video Trimmer [↗](https://gitlab.gnome.org/YaLTeR/video-trimmer)

Trim videos quickly

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) announces

> [Video Trimmer](https://flathub.org/apps/details/org.gnome.gitlab.YaLTeR.VideoTrimmer) v0.7 is out, featuring Adwaita updates, dark style & high contrast support, optional re-encoding for accurate trimming, "Show in Files" button and new translations.
> ![](StztnmnbAlp2lXhi6Jf71lFkJphvB478pZZ68phQL7w.png)

### NewsFlash feed reader [↗](https://gitlab.com/news-flash/news_flash_gtk)

Follow your favorite blogs & news sites.

[jangernert](https://matrix.to/#/@jangernert:matrix.org) says

> NewsFlash 2.0 (aka the Gtk4 port) now has functional CI flatpaks. It was blocked for a long time by the Gtk4 version of webkit. 🥳 to the maintainers of the org.gnome.Sdk runtime. More details about whats new will follow as we're inching closer to a release.
> In addition Felix Bühler (aka @Stunkymonkey) added FreshRSS support to NewsFlash.
> ![](MjBjtgdgIWugaFbwGtgrVtnj.png)
> ![](mTSoNQiBPcUxAtGfkKUjUvHj.png)

### Fragments [↗](https://gitlab.gnome.org/World/Fragments)

Easy to use BitTorrent client.

[Felix](https://matrix.to/#/@felix:haecker.io) says

> Earlier this week I released the first beta version of Fragments 2.0. To make the final version as bug-free as possible, please download the [beta version](https://flathub.org/beta-repo/flathub-beta.flatpakrepo) and test it as much as possible (feedback is welcome!). I showcased many of the new features in a [Twitter thread](https://twitter.com/haeckerfelix/status/1467942368250839043).
> ![](KZfobceYldEmuCygrNQWqRpA.png)

# Third Party Projects

[Maximiliano](https://matrix.to/#/@msandova:gnome.org) reports

> Now App Icon Preview, Emblem and Icon Library count with support for drag and drop!
> 
> You can drag icons from icon library into emblem, nautilus, or any svg editor. Both Emblem and App Icon Preview can now open SVG files by dragging them directly into the app.
> {{< video src="62f046df3b2fa468ab6ef2489d32ab15cbe923ec.webm" >}}

### Squeekboard [↗](https://gitlab.gnome.org/World/Phosh/squeekboard)

An on-screen-keyboard input method for Wayland

[Guido](https://matrix.to/#/@guido.gunther:talk.puri.sm) reports

> Squeekboard now features PIN, url and email layouts. We also reworked the theme handling so squeekboard defaults to dark-mode when run with [phosh](https://gitlab.gnome.org/World/Phosh/phosh) and matches its high contrast setting. Translations are now done via regular po files.
> ![](TegTLCPtPKgwdoIQziDtSsnd.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@guido.gunther:talk.puri.sm) announces

> We've released phosh 0.14.1 featuring
> 
> * avatars during phone calls on lockscreen
> * DTMF during phone calls on lockscreen
> * an initial "Run command" prompt (Alt-F2)
> * better thumbnails in the overview
> * docked mode improvements
> * plenty of bug fixes and cleanups
> * updated translations
> 
> and much more. Check the full [release notes](https://gitlab.gnome.org/World/Phosh/phosh/-/tags/v0.14.1)
> for more details and contributors.
> ![](snNdMjsBrtQCiHblUgKgUczw.png)

# GNOME Shell Extensions

[Simon Schneegans](https://matrix.to/#/@schneegans:matrix.org) says

> The new [Desktop Cube Extension](https://extensions.gnome.org/extension/4648/desktop-cube/) does not increase your productivity but gives some good old Compiz vibes!
> {{< video src="FSLmYaPIROTLPSCWXNWKLPiM.mp4" >}}

[heartmire](https://matrix.to/#/@heartmire:matrix.org) reports

> New extension - Focus changer. Change focus between windows in all directions using your keyboard. The extension will first try to find a suitable window within the same monitor. If there is none, it will try to find one on the next monitor in that direction (in a multi-monitor setup).
> 
> Available for GNOME Shell 3.6 - 41
> 
> https://extensions.gnome.org/extension/4627/focus-changer/

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

