---
title: "#21 Software Cleanup"
author: Felix
date: 2021-12-03
tags: ["obfuscate", "glib", "junction", "webkitgtk", "gnome-software", "gaphor", "fragments", "gnome-calls"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from November 26 to December 03.<!--more-->

# Core Apps and Libraries

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> Also in gnome-software, Phaedrus Leeds has [fixed a long-standing issue where installing a flatpakref file](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/1087) wouldn’t use flatpak transactions (the modern flatpak API). This should fix a few bugs with flatpakrefs in gnome-software, and behaviour differences from the command line `flatpak` command.

> Adrien Plazas has [been refreshing the appearance of app reviews](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/1101) in gnome-software, implementing a design by Tobias Bernard

### Calls [↗](https://gitlab.gnome.org/GNOME/calls)

A phone dialer and call handler.

[Evangelos](https://matrix.to/#/@evangelos.tzaras:talk.puri.sm) announces

> You can now use the keypad to send DTMF while on phosh's [lockscreen](https://gitlab.gnome.org/World/Phosh/phosh/-/merge_requests/944). This needed some DBus work on the [Calls](https://gitlab.gnome.org/GNOME/calls/-/merge_requests/448) side and UI tweaks in [libcall-ui](https://gitlab.gnome.org/World/Phosh/libcall-ui/-/merge_requests/20).
> So if you have to interact with an automated calling assistant ("Please press 1 to buy a washing machine, please press 2 …") this should work too now.
> ![](KswuthJkfYywmdSPrTmkSQFK.png)

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> Nishal Kulkarni and Emmanuel Fleury have been [tidying up some old tests in GLib](https://gitlab.gnome.org/GNOME/glib/-/issues/1434), ensuring those test results are machine readable, and making the code more maintainable in future

# Circle Apps and Libraries

### Obfuscate [↗](https://gitlab.gnome.org/World/obfuscate)

Censor your private information on any image.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> [Obfuscate](https://flathub.org/apps/details/com.belmoussaoui.Obfuscate) 0.0.4 got released, it includes fixes of various bugs that were introduced during the GTK4 port and some improvements brought to you by Alexander Mikhaylenko. This also means the next TWIG entry of the built-in Shell screenshot/screencast won't feature a development version of Obfuscate.
> ![](984f62896d972a8027787ecee0165d5000aa2d76.png)

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[danyeaw](https://matrix.to/#/@Yeaw:matrix.org) says

> Gaphor feature release version 2.7.0 is now out! We have added an extension for Sphinx to generate diagrams for your docs, support for Information Flows on connectors, lots of improvement to influence diagrams, improved autocomplete in our Python console, and usability updates.

### Fragments [↗](https://gitlab.gnome.org/World/Fragments)

Easy to use BitTorrent client.

[Felix](https://matrix.to/#/@felix:haecker.io) announces

> The new sessions stats dialog got merged. It displays information about the current session, e.g. how much data has been transferred in total, the current network load and more. I re-implemented desktop notification supports in the `fragments-v2` branch. Fragments also automatically restores the last used remote connection now. This is useful if you're using Fragments mainly as a client to control a Transmission server. And last but not least Maximiliano and I added support for the remove-all / pause-all torrents actions.
> ![](xepySfhvAZbFtpxjhQBYqaHN.png)

# Third Party Projects

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> During the same week, we managed to finish up an other GTK 4 / libadwaita port of [Icon Library](https://flathub.org/apps/details/org.gnome.design.IconLibrary). The port was done by Maximiliano as part of the last Google Summer of Code as well. It also includes the traditional updated icons.
> ![](d81940b40b3c573c6764ada7d2d1f603bfaaea36.png)

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> The GTK 4 & libadwaita port of [App Icon Preview](https://flathub.org/apps/details/org.gnome.design.AppIconPreview) done by Maximiliano during the last Google Summer of Code got released.
> ![](3443197f9d423dbe9d924f055a9535a6dfae267f.png)

### Junction [↗](https://github.com/sonnyp/Junction)

Lets you choose the application to open files and links.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) announces

> [Junction](https://github.com/sonnyp/Junction/) got a UI refresh and a new option to open the location with any application.
>
> ![](MLbIHihaMeURcQVjKJngZpfd.png)

# Documentation

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> The documentation of GtkSourceView 5 was ported to [gi-docgen](https://gnome.pages.gitlab.gnome.org/gi-docgen/) and is available at [gtksourceview5](https://gnome.pages.gitlab.gnome.org/gtksourceview/gtksourceview5/).

# GNOME Shell Extensions

[Simon Schneegans](https://matrix.to/#/@schneegans:matrix.org) says

> Fly-Pie, the marking-menu extension for GNOME Shell, has received a major update including proper support for touch screens and Wacom tablets, as well as a new clipboard menu. Watch the trailer: https://www.youtube.com/watch?v=BGXtckqhEIk
> ![](fNWeAAdKgGzoRIjapaTMwXGL.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

