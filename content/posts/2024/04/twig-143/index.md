---
title: "#143 Circle Updates"
author: Felix
date: 2024-04-12
tags: ["fragments", "blanket", "glib", "phosh", "pika-backup"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from April 05 to April 12.<!--more-->

# GNOME Core Apps and Libraries

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> After a lot of preparation, GLib has finally achieved an OpenSSF Best Practices ‘passing’ badge, which certifies that it follows a number of development and security best practices — see https://www.bestpractices.dev/en/projects/6011

# GNOME Circle Apps and Libraries

### Fragments [↗](https://gitlab.gnome.org/World/Fragments)

Easy to use BitTorrent client.

[Felix](https://matrix.to/#/@felix:haecker.io) reports

> It has finally happened! The long awaited major update of Fragments is now available, which includes many exciting new features.
> 
> The most important addition is support for torrent files. It is now possible to select the files you want to download from a torrent. The files can be searched and sorted, individual files can be opened directly from Fragments.
> 
> Further new features:
> * Added torrents can now be searched
> * In addition to magnet links, *.torrent links in the clipboard are now also recognized
> * Prevent system from going to sleep when torrents are active
> * New torrents can be added via drag and drop
> * Automatic trashing of *.torrent files after adding them
> * Stop downloads when a metered network gets detected
> 
> Improvements:
> * When controlling remote sessions, the local Transmission daemon no longer gets started
> * Torrents are automatically restarted if an incorrect location has been fixed
> * Torrents can now also be added via CLI
> * Clipboard toast notification is no longer displayed multiple times
> * Reduced CPU/resource consumption through adaptive polling interval
> * Improved accessibility of the user interface
> * Modernized user interface through the use of new Adwaita widgets
> * Update from Transmission 3.0.5 to 4.0.5
> 
> More information can be found in the [announcement blog post](https://blogs.gnome.org/haeckerfelix/2024/04/07/fragments-3-0/).
> {{< video src="okPPUdGXaUeZtCAlEYuWwfUV.webm" >}}

### Pika Backup [↗](https://apps.gnome.org/app/org.gnome.World.PikaBackup/)

Keep your data safe.

[Fina](https://matrix.to/#/@felinira:matrix.org) reports

> Pika Backup 0.7.2 was released. It fixes a crash on the schedule page. It also resolves an issue with pre- and post-backup scripts being unable to send desktop notifications.
> 
> We have already made lots of progress towards 0.8 which will focus on code maintainability and UI refinements. You can support Pika’s development on [Open Collective](https://opencollective.com/pika-backup).

### Blanket [↗](https://github.com/rafaelmardojai/blanket)

Improve focus and increase your productivity by listening to different sounds.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) announces

> Blanket 0.7.0 has been released and is available on [Flathub](https://flathub.org/apps/com.rafaelmardojai.Blanket).
> 
> This new release features a redesigned user interface and uses the latest GNOME design patterns.
> 
> Other changes include:
> 
> * Inhibit suspension when playing
> * Pause playback if the system enters power saver mode
> * MPRIS: Implement Play, Pause and Stop methods in addition to PlayPause
> * MPRIS: Implement Next and Prev methods for navigating presets
> * Updated Pink Noise sample
> * Changed train sound
> * Added preference to always start on pause
> ![](IwYsquXSxjIaXIIVzHVvwaik.png)

# Third Party Projects

[Krafting](https://matrix.to/#/@lanseria:matrix.org) announces

> Hello! This week I released [SemantiK](https://flathub.org/apps/net.krafting.SemantiK). It's a word-game where you need to find a secret word, similar to Cémantix or Semantle. The default language model is in French, but you can use and import your own ! 
> 
> It is available on [Flathub](https://flathub.org/apps/net.krafting.SemantiK)
> ![](TZUHiCbPxymWRYyowitFprew.png)

[xjuan](https://matrix.to/#/@xjuan:gnome.org) says

> New Cambalache stable version released!
> The app ui is now made with Cambablache and Gtk 4!
> Read more about v 0.90.0 at https://blogs.gnome.org/xjuan/2024/04/06/cambalache-0-90-0-released/
> ![](c862bfa3398434cc095750a10a7c8271a482b0fe1778749500895002624.jpeg)

[alextee](https://matrix.to/#/@alextee:matrix.org) announces

> The GTK4/libadwaita-based digital audio workstation Zrythm has released its last beta version v1.0.0-beta.6.7.32 in preparation for a release candidate!
> 
> More info on [our website](https://www.zrythm.org). Zrythm is also available on [Flathub](https://flathub.org/apps/org.zrythm.Zrythm)
> ![](TyMBifUSrEdxPtdBxWBOQazS.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) reports

> Phosh 0.38.0 is out:
> 
> [Phosh](https://gitlab.gnome.org/World/Phosh/phosh) now handles devices with rounded display corners better and supports count and progress indicators in lock screen launcher entries. On the [compositor](https://gitlab.gnome.org/World/Phosh/phoc) side we now handle always-on-top and move-to-corner keybindings and the on screen keyboard [squeekboard](https://gitlab.gnome.org/World/Phosh/squeekboard) got a whole bunch of layout improvements.
> 
> There's more. Check the full details [here](https://phosh.mobi/releases/rel-0.38.0/)
> ![](ecexIXgLqSXVrFYIfHpOCUzQ.png)

# Events

[mwu](https://matrix.to/#/@mwu:gnome.org) reports

> TWIG-Bot We are ramping up for GUADEC 2024 and sponsorships are still available. We made it easier this year and put the different opportunities online! Click here for more info: https://events.gnome.org/event/209/registrations/212/

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

