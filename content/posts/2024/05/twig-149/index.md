---
title: "#149 Installer Installment"
author: Thib
date: 2024-05-24
tags: ["morphosis"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from May 17 to May 24.<!--more-->

# Sovereign Tech Fund

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) says

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure) initiative, a number of community members are working on infrastructure related projects.
> 
> We're currently facing a major issue from the GNOME Foundation side. We hope it will be resolved before it impacts the coordination of the STF project, but if not, the future of parts of the project is uncertain.
> 
> Here are the highlights for the last week:
> 
> Felix completed the the [refactor of the Flatpak side of Key Rack](https://gitlab.gnome.org/sophie-h/key-rack/-/merge_requests/15). This is an important basis for the next steps (automatic updating of Flatpak items on changes, adding new items, etc.).
> 
> Adrien continued [porting Baobab away from GtkTreeView](https://gitlab.gnome.org/GNOME/baobab/-/merge_requests/74) and to GtkColumnView and ported Baobab to [use CSS variables](https://gitlab.gnome.org/GNOME/baobab/-/merge_requests/76) for Adwaita's named colors.
> 
> Dorota continued work on her [prototype portal implementation for testing](https://gitlab.gnome.org/dcz/gsimpl/-/commits/master), [updated the portal trigger](https://gitlab.gnome.org/dcz/portal-poker/-/commits/optionallll), and [fixed the MR](https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/2566#note_2115198) to shuffle around things in settings before the globalshortcuts part can go in, among other smaller things.
> 
> Joanie continued working on [getting rid of formatting.py in Orca](https://gitlab.gnome.org/GNOME/orca/-/issues/496), and a number of other minor Orca issues and cleanups.
> 
> Abderrahim [moved sysupdate configuration](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/2883) to the main sysupdate directory (as part of the extensions) and reviewed and merged an MR to [split out debug to a sysext](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/2389) and tarball for debuginfod, which should help reduce image size.
> 
> Martín worked on various aspects of tooling for development and testing on immutable OSes, including putting together a developer story to solicit feedback, and writing a minimalist [snippet for generating sysext images](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/2886).
> 
> Neill worked on tooling for checking CVEs in GNOME OS, landing MRs to [allow generate\_cve\_report.py and update\_local\_cve\_database.py to be used by other builds](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/merge_requests/19389) and [adding tables of unversioned elements of git/archive sources](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/merge_requests/19371) to generate\_cve\_report.py. He also wrote a script and modified CI to populate Gitlab Pages with the CVE reports for master and all stable branches.
> 
> Hub worked on [adding USB portal support](https://github.com/bilelmoussaoui/ashpd/pull/210) to ashpd. It can now exercise the portal, including obtain a device that can then then be opened with libusb (rusb). This allowed [fixing many issues in the portal implementation](https://github.com/flatpak/xdg-desktop-portal/pull/1354), including assert on API misuses, and state handling.
> 
> Sam fixed several small issues introduced in the GNOME Shell high contrast refactor, including [extraneous padding in icon-shadows](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3310), [menu item background hover fixes](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3311), an [app grid margin issue](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3312) in Large Text mode, and a [symbolic themed icon contrast fix](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3315) in notifications.
> 
> Sam also continued work on the File Chooser Portal Open and Save dialog [mockups](https://gitlab.gnome.org/Teams/Design/os-mockups/-/issues/250#note_2115074), and updated mic sensitivity symbolic icons in Adwaita to fix visibility issues in the Shell.
> 
> Julian continued to work on the [notification grouping in GNOME Shell](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3012) and looked into focus stealing prevention, filing issues and implementing fixes for some of them.
> 
> Andy worked on Online Accounts, including [porting to AdwDialog/AdwAlertDialog](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/232) and [separating OAuth2 browser process](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/233).
> 
> Dhanuka continued working on implementing the oo7-daemon Secret Prompt. They implemented [base64 payload parsing for SecretExchange](https://github.com/bilelmoussaoui/oo7/pull/73/commits/eb1e0f8a88583bb585e15b406b42d9c50e305b8a), implemented [org.gnome.keyring.Prompter interface based on SecretExchange](https://github.com/bilelmoussaoui/oo7/pull/73/commits/0dcc323769379202042cb949a7d9bfc0b64c1f77), and [integrated SecretExchange into the prompt as a test](https://github.com/bilelmoussaoui/oo7/pull/73/commits/922d5b2baebf5ba6bf514c57dad3dcad482d89a2).
> 
> Adrian worked on the [new installer for GNOME OS](https://gitlab.gnome.org/Teams/STF/Setup), settling on the way the internals (navigation, deciding what page to show next, etc) will work and implementing most of the basic page layouts following the [designs](https://gitlab.gnome.org/Teams/Design/os-mockups/-/blob/master/installer/os-installer.png?ref_type=heads) (though many of the pages don't fully work yet).
> 
> {{< video src="38fba19d61bcd62b92a1863c032756b50b22057c1794098266204274688.webm" >}}
> {{< video src="27375979f8ef551e3e5370548e0cae1a2c4fe9bc1794098268225929216.webm" >}}
> 
> Alice followed up on her GTK CSS variables work by adding better color functions ([rgb() and hsl()](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/7278), [color()](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/7286)). This will allow handling colors in a more flexible way, e.g. deriving colors from other colors and having them look good
> and replace GTK-specific css functions like alpha() and mix().
> 
> ![](69cfd70f106cf51865400bdbcb98b1d8d1a56c4f1794098297007243264.png)
> 
> Alice also landed a very nice [performance fix for GTK spinners](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/7280), which should make them smoother in apps.
> 
> On the Libadwaita front Alice landed a number of [style](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1118) [fixes](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1119) and [other](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1122) [bugfixes](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1126), and [made progress](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1123) on making the bottom sheets used as part of AdwDialog a standalone public widget.
> 
> Sam [updated many of the assets throughout Software](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/2029), refreshing many icons and designing new ones.
> 
> ![](ea4cd2d65f3e4a1fd085ec53fa6c0674f793cdfd1794098348332941312.png)
> ![](e144d299deacfa55b76c2a1af4f29b6da62daa461794098342049873920.png)
> 
> Sam also fixed an [issue with calendar text readability](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/445)

# Third Party Projects

[Philipp](https://matrix.to/#/@philippun:matrix.org) announces

> Snoop [0.3](https://gitlab.gnome.org/philippun1/snoop/-/releases/0.3) was released!
> 
> It is now possible to install the Nautilus plugin with a button in the preferences dialog, if you installed it from flathub. A new preview dialog now allows to open a preview of the selected file at the specific line. This comes in handy especially for the flatpak version, which until now used the file launcher portal via Gtk.FileLauncher.
> 
> Also the threading and memory management was overhauled, which fixed some random but regular crashes.
> 
> You can find the latest version directly on [flathub](https://flathub.org/apps/de.philippun1.Snoop).
> ![](CiPQuMHVDctoZqdioAnzrlwa.png)
> ![](PWdMrnwTECROJCvdpcIXDKhi.png)

[DaKnig](https://matrix.to/#/@daknig:matrix.org) says

> A new YouTube player has appeared! [DewDuct](https://github.com/DaKnig/DewDuct) is a mobile privacy aware YouTube player for the GNOME platform! Search and play videos, import subscriptions from NewPipe and more 😄 Currently packaged on Alpine Linux (`sudo apk add dewduct`).
> This was made for phones in mind! Linux mobile ahoy!
> 
> * Please note that this is beta software. despite that, it's quite usable for my personal use and I work on it actively!
> * Icon designs will be [greatly appreciated](https://gitlab.gnome.org/Teams/Design/app-icon-requests/-/issues/50)!
> ![](HfyMPDqjVthTKfJHYOYJsiQM.png)
> ![](KUCoVuBNLahOZIZCmCyvoFnt.png)
> ![](FxDiNgLDVRlgaejMNfhAGKPc.png)

### Morphosis [↗](https://gitlab.gnome.org/Monster/morphosis)

Convert your documents

[Jamie Gravendeel](https://matrix.to/#/@monster:gnome.org) says

> I have been working on Morphosis, an app that can convert documents from one format to another. It supports Markdown, DOCX, PDF, and a lot more. It has just been released on [Flathub](https://flathub.org/apps/garden.jamie.Morphosis)!
> ![](5d92b76103197a9e770e859856da763cf2b8d7651793937079525179392.png)

# GNOME Foundation

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) says

> : After six months of dedicated work and invaluable input from our Board, Staff, and community members, The GNOME Foundation is thrilled to share the draft of our Five-Year Strategic Plan!
> 
> This plan is a collaborative effort designed to guide the future of the GNOME Foundation, and we want Your feedback to ensure it truly reflects our collective vision.
> 
> Check out the draft and share your thoughts through our survey. Your feedback is crucial to shaping the direction of the Foundation: https://foundation.gnome.org/2024/05/23/introducing-the-gnome-foundations-five-year-strategic-plan-draft/

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
