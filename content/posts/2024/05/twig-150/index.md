---
title: "#150 Multiple Layouts"
author: Felix
date: 2024-05-31
tags: ["fretboard", "crosswords", "turtle", "workbench", "libadwaita", "newsflash"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from May 24 to May 31.<!--more-->

# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) announces

> libadwaita now has [`AdwMultiLayoutView`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.MultiLayoutView.html), allowing to define multiple layouts and reparent children between them. This allows to completely reorganize UI (say, turn sidebar into a bottom bar, or a grid into a vertical box) by changing a single property, e.g. via a breakpoint setter

# GNOME Circle Apps and Libraries

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> Workbench is now available on the GNOME Nightly repository, and we welcomed 2 GSoC students. You can read about it here https://blog.sonny.re/workbench-news

### NewsFlash feed reader [↗](https://gitlab.com/news-flash/news_flash_gtk)

Follow your favorite blogs & news sites.

[Jan Lukas](https://matrix.to/#/@jangernert:matrix.org) says

> Newsflash can now play video attachments like those in video podcasts or youtube subscriptions. This is thanks to the amazing video player [clapper](https://github.com/Rafostar/clapper), which is now also available as a library. In the process of integrating libclapper I generated rust bindings for it, that are available [here](https://gitlab.gnome.org/JanGernert/clapper-rs).
> {{< video src="bRhfjuYURukvmniAbDcsCKvr.webm" >}}

### Fretboard [↗](https://apps.gnome.org/Fretboard)

Look up guitar chords

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) announces

> The 1st of June is right around the corner, so why not pick up the guitar and learn some catchy tunes for the summer? I've just published version 7.0 of Fretboard, which brings more accurate chord name prediction, note names on hover for the neck top toggles, and a couple of fixes for various small issues encountered since last release.
> 
> Get Fretboard on [Flathub](https://flathub.org/apps/dev.bragefuglseth.Fretboard)!
> ![](d0730947f2d3f0d3d7f122eb2cbf0142be98f2d91795898214159745024.png)

# Third Party Projects

[Alain](https://matrix.to/#/@a23_mh:matrix.org) announces

> Planify 4.8: New Features to Enhance Your Task and Project Management!
> We are excited to announce the new update for Planify, our task and project management app. With version 4.8, we've added features that will help you organize and visualize your tasks more efficiently. Here are the most notable updates:
> 
> **1. Markdown Support in Task Descriptions**
> Bring your descriptions to life! You can now use Markdown to format text in your task descriptions. This means you can add bold, italics, links, lists, and much more, allowing for greater customization and clarity in your descriptions.
> 
> **2. Markdown Support in Task Titles**
> Want to make your titles more striking and descriptive? With the new Markdown support in task titles, you can do just that. Use Markdown to highlight important parts of the title, making your tasks easier to identify at a glance.
> 
> **3. New All Tasks View**
> We know managing multiple projects and tasks can be challenging. That's why we've introduced a new view that lets you see all your tasks and projects in one place. This feature provides you with a comprehensive overview of all your work, making it easier to plan and track your progress.
> 
> **4. Expand or Collapse Tasks Functionality**
> For better visual management, you can now expand or collapse tasks. This functionality allows you to quickly show or hide task details, helping you focus on what's most important at any given moment without getting lost in the information.
> 
> What to Expect from These Improvements?
> With these new features, Planify 4.8 becomes an even more powerful tool for managing your tasks and projects. Markdown support offers flexibility in how you present information, while the new view and the ability to expand or collapse tasks enhance the app's usability and efficiency.
> 
> Update to version 4.8 and discover how these new features can boost your productivity and organization!
> ![](QlMQLHnPIffsPXUohikJIxYp.png)
> ![](osRolNFlZxonwedoxRVIgglC.png)

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) says

> Introducing **Aurea,** an app for _Flatpak_ application developers.
> 
> Aurea is a simple banner previewer that reads metainfo files and displays them as they will appear in _Flathub_ banners, making app publication easier.
> 
> Kudos to Tobias Bernard for the design.
> 
> Get it on [Flathub](https://flathub.org/apps/io.github.cleomenezesjr.aurea)
> ![](IhQskcufMVdykGlggNaSmsst.png)

[DaKnig](https://matrix.to/#/@daknig:matrix.org) says

> DewDuct 0.2.2: Wow! Couldn't imagine I would get so many users! Thanks for all the feedback!
> New features this week:
> * Import your NewPipe subscriptions (but better)!
> * Or maybe manually subscribe to channels!
> * The subscription list is saved and reloaded automatically
> 
> Regarding flatpak, will work on it once the main feature is implemented: recent videos from your subscriptions. Please help via PR so that the flatpak will come out sooner.
> 
> The new version is available in alpine, on the testing repo, or on postmarketos edge. Check it out if you have a linux phone!

### Turtle [↗](https://gitlab.gnome.org/philippun1/turtle)

Manage git repositories in Nautilus.

[Philipp](https://matrix.to/#/@philippun:matrix.org) says

> Overcoming flatpak limitations
> 
> [Turtle 0.9](https://gitlab.gnome.org/philippun1/turtle/-/releases/0.9) has been released, which finally brings better flatpak support. 
> 
> The turtle service is now available in the flatpak version and uses dbus to communicate with the Nautilus extension. A new plugin installer dialog, which can be opened directly from the Settings dialog, allows you to install the Nautilus plugin file with a single click.
> 
> Additionally it is now possible to sign commits via the seahorse dbus interface.
> ![](QDCxyOtKAcjiBACefxYxIivg.png)

### Crosswords [↗](https://gitlab.gnome.org/jrb/crosswords)

A crossword puzzle game and creator.

[jrb](https://matrix.to/#/@jblandford:matrix.org) announces

> Crosswords 0.3.13 was released!
> 
> This version features several keyboard behavior cleanups for the main
> game providing a more natural feel when entering puzzles.
> 
> For the editor, we moved the autofill functionality moved in-line
> instead of being in a modal dialog. Autofilling has been made faster
> and more correct, as well.
> 
> Check out the [release announcement](https://blogs.gnome.org/jrb/2024/05/23/crosswords-0-3-13-side-quests/) for more information and download it from [flathub](https://flathub.org/apps/org.gnome.Crosswords)!
> ![](aHkUgQrpnHyxzdeQoipSDlQD.png)
> {{< video src="FIHoRkZbGuizUHPxDZMoCBUQ.webm" >}}

# GNOME Foundation

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) announces

> Final location proposals for GUADEC 2025 are due today! Make sure to submit yours by end-of-day or contact the Foundation if you have additional questions or need more time. More details and links to the submission form can be found here: https://foundation.gnome.org/2024/04/18/call-for-guadec-2025-location-proposals/

[Holly Million](https://matrix.to/#/@hmillion:gnome.org) announces

> We have some exciting updates from the GNOME Foundation.
> 
> Executive Director Holly Million had a call this week with Tara
> Tarakiyee, our program manager at Sovereign Tech Fund, providing him with an update on the project work taking place under the Foundation’s current contract with STF and the Foundation's plans to continue and expand the work. The updates included:
> 
> * The contracted work continues to progress, and the Sovereign Tech Fund is very encouraged by what has been accomplished to date.
> 
> * The areas of work currently being funded by STF are planned to
> continue and to be strengthened and expanded as part of our new, permanent GNOME Development Initiative, as described in our draft
> strategic plan – https://foundation.gnome.org/strategicplan/
> 
> * The Foundation is reorganizing the project and hiring an additional
> program manager to work with current managers on the new Initiative.
> 
> * We are finalizing a contract for transitional work with the new
> manager and will make a formal announcement next week.
> 
> * We hope to significantly increase the amount of development work
> happening through the Initiative with a process that allows community
> suggestions for needed work and an application process for grants for
> proposed work.
> 
> * The Foundation recently applied to the Open Tech Fund to strengthen the Initiative, including proposing to hire a permanent full-time program manager and to invest in other important work to support our community
> 
> * The Foundation will apply for a new round of contract funding when the Sovereign Tech Fund reopens for applications in mid June.
> 
> * We have launched the GNOME Development Fund, which will raise
> additional support from the community to fuel the development work
> possible through the Initiative. Starting immediately, donations made through the Fund will build the Initiative –
> https://www.gnome.org/donate/ – This Fund page will continue to
> develop, with counters, a backer list, and tiered benefits for backers
> at differing levels, and badges coming next. Donate today to support
> the future of GNOME.
> 
> In other exciting news, the Foundation has new professional bookkeeping systems in place, completed a financial review in
> preparation for a required financial audit next year, and at the
> completion of the second quarter of this fiscal year, the Foundation is performing under budget and is on track in our commitment to having a non-deficit year. We will share more details, including graphs and financial details soon.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!