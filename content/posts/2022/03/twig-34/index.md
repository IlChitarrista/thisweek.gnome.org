---
title: "#34 No Backup No Mercy"
author: Felix
date: 2022-03-11
tags: ["vala", "pika-backup"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from March 04 to March 11.<!--more-->

# Circle Apps and Libraries

### Pika Backup [↗](https://wiki.gnome.org/Apps/PikaBackup)

Simple backups based on borg.

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) says

> Pika Backup gained an improved sense of what's going on with its running BorgBackup processes. In turn, this allows Pika to show more information in the user interface about what's currently going on.
> 
> The mechanism also provides the basis for implementing missing features like stopping scheduled backups if the used connection becomes metered or the computer runs on battery for a while. Parts of those features are already implemented.

### Audio Sharing [↗](https://apps.gnome.org/app/de.haeckerfelix.AudioSharing)

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) reports

> This week, [Audio Sharing](https://apps.gnome.org/app/de.haeckerfelix.AudioSharing/) joined GNOME Circle. Audio Sharing can stream your PC's audio to your phone and therefore can, for example, allow you to use audio hardware that only connects to your phone. Congratulations!
> ![](6e4c3df9e26d821e7a81f054f91558fc9e024b89.png)

# Core Apps and Libraries

### Vala [↗](https://gitlab.gnome.org/GNOME/vala)

An object-oriented programming language with a self-hosting compiler that generates C code and uses the GObject system

[lwildberg](https://matrix.to/#/@lw64:gnome.org) reports

> There is now a [Sdk extension](https://github.com/flathub/org.freedesktop.Sdk.Extension.vala) with the Vala compiler, language server and other tooling on flathub! You can compile your Vala app with it, or use the tools and libraries in your application. Thanks to Bilal Elmoussaoui for help.

# GNOME Shell Extensions

[Simon Schneegans](https://matrix.to/#/@schneegans:matrix.org) announces

> The [Desktop-Cube extension for GNOME Shell](https://extensions.gnome.org/extension/4648/desktop-cube/) has been updated once more!
> You can now set panoramic background images and directly drag windows to adjacent workspaces. Watch the trailer: https://www.youtube.com/watch?v=j1oN5PiBjjE
> ![](OzMYKBjIUqgPYCmJYAyDmlkI.jpg)

# GNOME Foundation

[Neil McGovern](https://matrix.to/#/@nmcgovern:matrix.org) announces

> Registration for Linux App Summit, which will be held at the end of April, is now open: https://linuxappsummit.org/

[Neil McGovern](https://matrix.to/#/@nmcgovern:matrix.org) announces

> GNOME is participating in Google Summer of Code again! Find more details and info on how you can participate at https://gsoc.gnome.org

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

