---
title: "#74 Decades Later"
author: Felix
date: 2022-12-16
tags: ["gaphor", "pods", "loupe", "workbench", "gtk", "gnome-control-center", "gdm-settings", "libadwaita"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from December 09 to December 16.<!--more-->

# Core Apps and Libraries

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) reports

> The GTK4 file chooser widget has received a grid view, with bigger thumbnails. This is the culmination of more than a decade of work, and was only made possible by GTK4's complete rewrite of its rendering system, and the introduction of highly performant and scalable list & grid widgets.
> 
> This will be available in the next release, numbered 4.10 and scheduled for early 2023. You can read more [here](https://blog.gtk.org/2022/12/15/a-grid-for-the-file-chooser/).
> 
> We hope you enjoy it ❤️
> ![](b8fc5fac8555c455cd1b7627179538275ec353a4.png)

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> Not a lot is happening in libadwaita land atm, but one small addition: [`adw_message_dialog_choose()`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/method.MessageDialog.choose.html) - a way to use `AdwMessageDialog` with a GIO async func, same as the new dialog APIs in GTK 4.9.x:
> 
> ```vala
>     [GtkCallback]
>     private async void clicked_cb () {
>         var dialog = new Adw.MessageDialog (this,
>                                             "Replace File?",
>                                             "A file named “example.png” already exists. Do you want to replace it?");
> 
>         dialog.add_response ("cancel", "_Cancel");
>         dialog.add_response ("replace", "_Replace");
> 
>         dialog.set_response_appearance ("replace", DESTRUCTIVE);
> 
>         print ("Response: %s\n", yield dialog.choose (null));
>     }
> ```

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Allan Day](https://matrix.to/#/@aday:gnome.org) reports

> In Settings, Felipe Borges landed a collection of polish changes. The Thunderbolt panel will now only show when there is Thunderbolt hardware, the About panel now uses AdwEntryRow for the hostname, and the Printer panel now uses AdwStatusPage when it is empty. sunflowerskater also added a description to the battery percentage switch.

# Circle Apps and Libraries

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) announces

> This week, [Emblem](https://apps.gnome.org/app/org.gnome.design.Emblem/) joined GNOME Circle. Emblem allows you to generate project avatars for your Matrix rooms and git forges. Congratulations!
> ![](4e339175f224480975534f643c4d3f2b4b8edad5.png)

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) reports

> Workbench has seen many improvements since the last update on TWIG, here are the highlights:
> 
> Available in 43.2:
> 
> * Show Vala diagnostics (powered by Vala Language Server)
> * Reset window preview on close
> * Add a warning about Blueprint being experimental technology
> 
> Available in the next release:
> 
> * Show JavaScript diagnostics (powered by Rome Language Server)
> * Fix preview of non GtkBuildable
> * Prevent UI crashes
> * Switching between XML and Blueprint will now convert between the two
> ![](uUljGOWjhshSwfiWdxxoMEHa.png)

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) reports

> I wrote a small blog on how to set up [macOS keyboard shortcuts with GTK4](https://gaphor.org/2022/12/10/gtk4-macos-keybindings/). 
> GTK 4 does no longer automatically substitute `Ctrl` for `Command` on macOS, but we still want to provide our macOS users with a pleasant user experience.

# Third Party Projects

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) announces

> XDG portals 1.16.0 was released this week. The major highlights of this release are:
>  * Background monitoring service, a new service that detects sandboxed applications running in the background without a user visible window. This information can be consumed by desktop environments to provide richer control over these apps, [for example like this](https://gitlab.gnome.org/Teams/Design/os-mockups/uploads/e119b5d1edd7ec26c80b2a2a8d7066f9/background-portal.png).
>  * The new Global Shortcuts portal, which allows applications to be notified of shortcuts being activated even when they're unfocused. So far only the KDE backend implements this portal, but hopefully more backends will implement it in the future.

[alexx](https://matrix.to/#/@alexx:tchncs.de) says

> This week I published my new app called Live Captions to Flathub. It captions your desktop audio or mic in realtime locally. It only supports English for now.
> 
> In the future, I hope to update it with more accurate models, more languages, and more features. I hope some people will find this app useful. https://flathub.org/apps/details/net.sapples.LiveCaptions
> {{< video src="c703e3e164c1cedfb981c1e9a5e63fc12c47bb71.mp4" >}}

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) says

> This week I released "Diccionario de la Lengua" a small app to luck up words from the online dictionary of the Royal Spanish Academy. Spanish-speaking GNOME users might find it of interest. You can get it from [Flathub](https://flathub.org/apps/details/com.mardojai.DiccionarioLengua).
> ![](rpmUCyhFUsUaBcwsrlGuMNlL.png)

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) announces

> Since the last time I wrote about [nautilus-code](https://github.com/realmazharhussain/nautilus-code) on This Week in GNOME,
> 
> * It has been ported to Python which makes it easy to
>     - Support Nautilus version 43 and earlier at the same time
>     - Install to $HOME directory
> * Default install location has changed to $XDG\_DATA\_HOME. So, installation does not require sudo privileges now.
> * Support for VSCode Insiders **Flatpak** has been added.
> * I have added a new Issue Form for Editor/IDE Support Request so it is even easier to submit a request to add support for an IDE or code editor.

### Pods [↗](https://github.com/marhkb/pods)

A podman desktop application.

[marhkb](https://matrix.to/#/@marcusb86:matrix.org) reports

> Pods now has all the features for the first stable version and is now in the release candidate phase. Among the features are:
> 
> * uploading/downloading files to/from a container
> * container terminal interaction
> * lots of visual improvements
> * etc.
> 
> The plan now is to fix as many bugs as possible, complete translations and finish this [Design Task](https://github.com/marhkb/pods/issues/465).
> ![](ZTzFfqhlwAbuLRivGkmclhLU.png)
> ![](wOhllwYljbcWSjXyJAmKbjxe.png)

### Loupe [↗](https://gitlab.gnome.org/BrainBlasted/loupe)

A simple and modern image viewer.

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) reports

> Since our last update, there have been many new features and bug fixes for [Loupe](https://gitlab.gnome.org/BrainBlasted/loupe). When opening an image, the window now appears in the correct aspect ratio and shows a spinner until the image is loaded. The properties now show several details about photos from the [Exif](https://en.wikipedia.org/wiki/Exif) data, including the nearest town if the GPS location is available. The location can also be opened in apps like Maps. Drag and drop out of the Loupe window now works as well. Zooming via scroll-wheel should feel even more natural, and zoom is now limited to 2000%.
> ![](78d5593f81356fdfda4ab0d46ffece8b95cc4c95.png)

### Login Manager Settings [↗](https://realmazharhussain.github.io/gdm-settings/)

A settings app for the login manager GDM.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) announces

> I missed a few of This Week in GNOME postings. In that time, [Login Manager Settings](https://realmazharhussain.github.io/gdm-settings) [v2.0](https://github.com/realmazharhussain/gdm-settings/releases/tag/v2.0) was released with added power options, import/export mechanism, adaptive ui, and some more features and bug fixes. Full release notes are available [here](https://github.com/realmazharhussain/gdm-settings/releases/tag/v2.0).
> 
> This week, I
> 
> * added the option to change cursor size.
> * uploaded pre-built packages (AppImage & Flatpak) with the new feature to [my Patreon](https://www.patreon.com/posts/cursor-size-in-75940461?utm_medium=clipboard_copy&utm_source=copyLink&utm_campaign=postshare_creator&utm_content=join_link) if anyone is interested in that.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

