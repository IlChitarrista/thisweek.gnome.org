---
title: "#76 Last Fragments of 2022"
author: Felix
date: 2022-12-30
tags: ["money", "authenticator", "fragments", "gaphor"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from December 23 to December 30.<!--more-->

# Circle Apps and Libraries

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) reports

> This week, [Lorem](https://apps.gnome.org/app/org.gnome.design.Lorem/) joined GNOME Circle. Lorem allows you to generate placeholder texts. Congratulations!
> ![](a481797384e254d3c1eb45314a23db54c7d08dc3.png)

### Fragments [↗](https://gitlab.gnome.org/World/Fragments)

Easy to use BitTorrent client.

[Felix](https://matrix.to/#/@felix:haecker.io) announces

> Fragments 2.1 is now available on Flathub. It brings many new features and improvements, like...
> 
> * Allow changing the location of a single torrent
> * New menu option to resume all torrents
> * Torrent errors are now displayed instead of ignoring them silently
> * Display message when the configured download/incomplete directory is not available
> * Transmission daemon is only started when necessary, and no longer runs continuously in the background
> * Application window can now be closed with CTRL+W
> * Fixed issue that already added magnet links were not recognised
> * Fixed a crash related to the "Automatically Start Torrents" setting
> * User interface improvements by using new Libadwaita widgets
> 
> In summary, the update makes Fragments much more user-friendly, as errors are now displayed directly, rather than being quietly ignored, which has led to many confusions in the past.
> 
> In addition, my other apps such as [Shortwave](https://flathub.org/apps/details/de.haeckerfelix.Shortwave) and [Audio Sharing](https://flathub.org/apps/details/de.haeckerfelix.AudioSharing) have also received updates in the last few weeks, which include small improvements and bug fixes, and upgrades them to the new GNOME 43 platform with the latest Libadwaita widgets.
> ![](ouNElEZATCagJdaXlHAIdHhs.png)

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Dan Yeaw](https://matrix.to/#/@Yeaw:matrix.org) says

> [Gaphor](https;//gaphor.org), the simple modeling tool, version 2.14.0 is out! Among numerous other improvements, Arjan Molenaar and Tobias Bernard have been quite the duo improving the UI and UX with updates to the welcome screen, keyboard shortcuts, tabs, and toolbars. For example, the welcome screen went from a grid of logos to a list of options that add much better guidance for users. Other improvements include:
> * New element handle and toolbox styles
> * Use system fonts by default for diagrams
> * Add tooltips to the application header icons
> * Make sequence diagram messages horizontal by default
> * Make keyboard shortcuts more standard especially on macOS
> * macOS: cursor shortcuts for text entry widgets
> * Load template as part of CI self-test
> * Update docs to make it more clear how to edit CSS
> * Switch doc style to Furo
> * Add custom style sheet language
> * Support non-standard Sphinx directory structures
> * Continue to make model loading and saving more reliable
> * Move Control Flow line style to CSS
> * Do not auto-layout sequence diagrams
> * Use new actions/cache/(save|restore)
> * Remove querymixin from modeling lists
> * Improve Windows build reliability by limiting cores to 2
> * Croatian, Hungarian, Czech, Swedish, and Finnish translation updates
> ![](dZclWKWLrWhhmONuIhYwEOoZ.png)

### Authenticator [↗](https://gitlab.gnome.org/World/Authenticator)

Simple application for generating Two-Factor Authentication Codes.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> A new release of [Authenticator](https://flathub.org/apps/details/com.belmoussaoui.Authenticator) is out! The release contains various bug fixes and improvements:
> 
> * Mutli Camera support when scanning a QR code
> * Camera: Use GL when possible
> * Fix an issue when restoring an AEGIS backup file
> * Avoid duplicated items when restoring a backup
> * Allow disabling favicons download in general or on a metered connection
> * Updated providers list
> * Use new libadwaita widgets

# Third Party Projects

[Khaleel Al-Adhami](https://matrix.to/#/@adhami:matrix.org) reports

> The new release of [Converter](https://gitlab.com/adhami3310/Converter) is out! With this release, you can now convert multiple images (with preview!) at the same time to the same format. It now supports HEIF/HEIC, BMP, AVIF, JXL, and TIFF. It also supports exporting PDF pages into images. It can convert animated GIF into WEBP as well as split all of the GIF frames into individual images. It can also split ICO files into individual images. Other than supporting more formats, now you can drag and drop to the application and use "Open With" from your file browser. Get it from [Flathub](https://flathub.org/apps/details/io.gitlab.adhami3310.Converter).
> ![](KvpqjBaSETlSVGwisVDortBK.png)

[knuxify](https://matrix.to/#/@knuxify:cybre.space) reports

> [Ear Tag 0.3.0](https://github.com/knuxify/eartag/releases/tag/0.3.0) has been released! This release introduces **support for more tags**, plenty of **performance improvements** and various bugfixes. You can get the latest version from [Flathub](https://flathub.org/apps/details/app.drey.EarTag).
> ![](enOgdlcwmtLjCrOpGstCJKFJ.png)

### Money [↗](https://flathub.org/apps/details/org.nickvision.money)

A personal finance manager.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Money [V2023.1.0-beta1](https://github.com/nlogozzo/NickvisionMoney/releases/tag/2023.1.0-beta1) is here! This is a HUGE update (although not including any new features yet) as Money has been completely rewritten in C#!  Besides a GNOME version, since everything is now in C#, we are also introducing Money for Windows!
> 
> The GNOME version is written using the [gir.core](https://github.com/gircore/gir.core) library. The `.nmoney` files used in the GNOME version are fully-compatible with the WinUI version and vice-versa. Money is also receiving some design tweaks to be more usable on mobile devices.
> 
> New features, including PDF reports, a new account setup dialog, and more, are planned before releasing the stable version :)
> 
> The beta is available in `flathub-beta`:
> 
> ```
> flatpak remote-add --if-not-exists flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo
> flatpak install flathub-beta org.nickvision.money
> flatpak run org.nickvision.money//beta
> ```
> ![](OevRSSiwWNgSJrUKwgBPdUSv.png)
> ![](CoVMUxxusQdTxAcFQALGTfiy.png)

# GNOME Shell Extensions

[Simon Schneegans](https://matrix.to/#/@schneegans:matrix.org) reports

> The latest release of the [Burn-My-Windows](https://extensions.gnome.org/extension/4679/burn-my-windows/) GNOME Shell extension includes a ridiculous portal effect inspired by the iconic Rick and Morty series! In total, the extension now offers 18 effects, each of which has plenty of customization options.
> {{< video src="WQvfvWiDgEcSAdIrkhlRuSsM.mp4" >}}

# Miscellaneous

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> After more than a year, we finally did a new release of `flatpak-github-actions`, which are a set of Github Actions that allows building and publishing a Flatpak from Github CI. Details can be found at https://github.com/flatpak/flatpak-github-actions/releases/tag/v5

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

