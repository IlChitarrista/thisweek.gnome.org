---
title: "#75 Redesigned Sound"
author: Felix
date: 2022-12-23
tags: ["libadwaita", "gnome-control-center", "phosh"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from December 16 to December 23.<!--more-->

# Core Apps and Libraries

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) reports

> Marco Melorio has been working on implementing the redesign of the Sound panel of GNOME Settings. This week, a more modern version of the output test dialog was merged! There are more changes in the pipeline, so keep an eye for next updates.
> ![](cdb7a7b4f796bb615b4e9c58c33f8415baf8317d.png)

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Jamie](https://matrix.to/#/@itsjamie9494:matrix.org) reports

> AdwBanner, a simple bar with contextual information, has landed in LibAdwaita
> ![](YrWGInbArtUsCOzrFxwlbxda.png)

# Third Party Projects

[Sjoerd Broekhuijsen](https://matrix.to/#/@sjoerdb93:matrix.org) says

> Last week I launched Graphs, a new libadwaita tool I've been working on for plotting and manipulation of data. Graphs allows you to import and plot data from two-column data files (or any amount of columns really), and transform and manipulate these data sets in a breeze. Alternatively, it is possible to easily generate data from equations. You can transform and plot multiple data files at the same time, both imported and generated data is treated the same and can be saved as text files for further use.  I've been using Graphs to assist with my PhD project and I hope it can be useful for others as well. It is still in the early stages of development, but most of the core features are implemented and I am very open to feedback of any kind. The latest version of Graphs is [available on Flathub](https://flathub.org/apps/details/se.sjoerd.DatMan).
> ![](rmhfRAOuAVCKuXbtPiUXBcfH.png)
> ![](XurlKbhteFapBJMASnxFJsLH.png)

[abb128](https://matrix.to/#/@alexx:tchncs.de) announces

> Live Captions v0.2.0 has been released on Flathub, which adds an option for a transparent window, customizable window width, and the issue of text expanding the window has been fixed. It also now shows a warning if captions aren't being generated fast enough to keep up with realtime.
> ![](32a8e0d476362381f4a2cb72f1e3f144e93a2a17.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido (away)](https://matrix.to/#/@guido.gunther:talk.puri.sm) announces

> Chris Talbot contributed a new plugin for [phosh](https://gitlab.gnome.org/World/Phosh/phosh) that allows to display emergency contact information on the lock screen. We also added the ability for plugins to have preferences:
> ![](zkszgVAaXGnMAMueyFlgseql.png)
> ![](AzcLnItybVONIJPxbnQCDtzM.png)

# Miscellaneous

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) announces

> Since the file chooser icon view work was merged on GTK4, we have received information from trusted sources that the Linux usage on the desktop took over all other OSes combined. We did not expect that it to happen so soon, but as it seems, this missing feature was really the one and only blocker of massive adoption. Our sources also mentioned that we crossed the Year of Linux Desktop user count threshold. This can all be verified in this important graph that was shared with us:
> ![](23f63ac3b5b06f5b8ade03f2f05f637fccf59993.png)

(Note from TWIG author: 😉)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

