---
title: "#50 Extend the Web"
author: Felix
date: 2022-07-01
tags: ["gnome-control-center", "glib", "epiphany", "gnome-software", "libadwaita"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from June 24 to July 01.<!--more-->

# Core Apps and Libraries

### Web [↗](https://gitlab.gnome.org/GNOME/epiphany)

Web browser for the GNOME desktop.

[patrick](https://matrix.to/#/@pgriffis:igalia.com) reports

> Epiphany has received [numerous improvements to WebExtension support](https://blog.tingping.se/2022/06/29/WebExtensions-Epiphany.html).
> ![](xntzaUQiquSWztbutIKLjDrT.png)

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) announces

> Libadwaita now has [`AdwMessageDialog`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.MessageDialog.html) as an adaptive replacement for `GtkMessageDialog`.
> ![](c639ec4df7493c3cbe33e7f0b80a45ad2f4dbb51.png)
> ![](3347c6332d237f7fb1fde8e355c16acaae6bfd27.png)

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> Milan Crha has [improved support for displaying flatpak permissions in gnome-software](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/1404/diffs#note_1480290)

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) reports

> Thanks to the fantastic work of Kate Hsuan and Richard Hughes, device security information is now available in Settings. The security information is provided by the Fwupd project.
> ![](fd55c09ac7b0eb10b7c3d3cceeb0ccfaf03e6ef0.png)
> ![](0274135a300d62af5d19b23864394d8dd8ae89cd.png)
> ![](06aa1b4e7c093f9ebef39a17f7b4c79109766e9a.png)

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> GLib 2.74 will require an additional part of the C99 specification: variadic arguments in macros using `__VA_ARGS__`. All supported toolchains (GCC, Clang, MSVC) already comply to the standard, so if you use a different compiler make sure it supports C99: https://gitlab.gnome.org/GNOME/glib/-/merge_requests/2791

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> Two new macros to define enumeration and flags types directly in your C code without using glib-mkenums are going to be available in the next stable release of GLib: https://gitlab.gnome.org/GNOME/glib/-/merge_requests/2788

# Third Party Projects

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) reports

> Loupe has gained a brand new gallery view, with smooth image loading, swipe navigation support, and more.
> {{< video src="4e74b0ea517e3762002557460ce8e03a859ac323.webm" >}}

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
