---
title: "#62 Forty-three!"
author: Felix
date: 2022-09-23
tags: ["gradience", "telegrand", "gdm-settings", "dialect", "newsflash", "apostrophe"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 16 to September 23.<!--more-->

**This week we released GNOME 43!**

![](43_banner.png)

This new major version of GNOME is full of exciting new features like a redesigned Shell quick settings menu, a modernized file manager, new device security settings - and of course much more. More information can be found in the [GNOME 43 release notes](https://release.gnome.org/43/).

Readers who have been following this site for a few weeks will already know some of the new features. If you want to follow the development of GNOME 44 (Spring 2023), keep an eye on this page - we'll be posting exciting news every week!

# Circle Apps and Libraries

### NewsFlash feed reader [↗](https://gitlab.com/news-flash/news_flash_gtk)

Follow your favorite blogs & news sites.

[Jan Lukas](https://matrix.to/#/@jangernert:matrix.org) says

> NewsFlash 2.0 has just been released on flathub. It has been ported to Gtk4 and can now sync with Nextcloud News & FreshRSS. For more details take a look at issue 55, 56 & 57 of "This Week in GNOME".

### Dialect [↗](https://github.com/dialect-app/dialect/)

Translate between languages.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) says

> [Dialect was updated](https://github.com/dialect-app/dialect/pull/267) to use new widgets from libadwaita 1.2 like `AdwAboutWindow` and `AdwEntryRow`, it also received an style update to have a more flat look. These changes will be released in an upcoming version targeting GNOME 43 along with other minor improvements.
> ![](NtQSLFKoojOCAOoPUEUuPxBN.png)
> ![](DtThMNeuNKmAUrKAPqQgiHye.png)

### Apostrophe [↗](https://gitlab.gnome.org/World/apostrophe)

A distraction free Markdown editor.

[Manu](https://matrix.to/#/@somas95:gnome.org) reports

> I've implemented some basic autocompletion for Apostrophe. It completes parenthesis, brackets, unordered lists, ordered lists and nested lists
> {{< video src="f4468c71711f78639b7ba81e5040e96280ca32ef.mp4" >}}

# Third Party Projects

[FineFindus](https://matrix.to/#/@finefindus:matrix.org) reports

> I've released version 0.3.0 of [Eyedropper](https://github.com/FineFindus/eyedropper). This release features a basic color shades generation and the ability to customize the order of the shown color formats. The app has been translated into French by [rene-coty](https://github.com/rene-coty) and German. It is now available on [Flathub](https://flathub.org/apps/details/com.github.finefindus.eyedropper).
> ![](akxtnckBxndzdcJKAYSaZOXK.png)

[alexhuntley](https://matrix.to/#/@alexhuntley:matrix.org) announces

> I released version 0.7.0 of Plots, a simple graphing app for GNOME. It introduces a new color picker, a preferences dialog, and support for the system dark theme.
>
> Plots was then ported from GTK 3 to GTK 4 and Libadwaita, with the current version 0.8.1 available on Flathub now.
> ![](PDiaFyoSpGckOEocjkScNasc.png)
> ![](aZjBLjBoIHDagEWTHhPLPSLe.png)
> ![](BtKKMnNiaWSLRMuefyfvlXXB.png)

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) announces

> I'm announcing my small new project [Key Rack](https://gitlab.gnome.org/sophie-h/key-rack). Key Rack will allow you to browse and edit Passwords, Tokens, and similar things that Flatpak apps store encrypted. The app is intended for developers for debugging and for everyone for looking up the password that you forgot again. Key Rack currently has alpha quality.
>
> Why a new app? The TL;DR is: Currently, for technical reasons, your passwords might appear only in the Passwords app you are used to, or only in Key Rack. Maybe Key Rack will show all of them one day.
>
> A more details explanation is the following: Existing apps like [Passwords and Keys (Seahorse)](https://wiki.gnome.org/Apps/Seahorse) allow access to the global key storage (accessible via [Secret Service](https://specifications.freedesktop.org/secret-service/latest/).) Since the global storage has no access control for Flatpaks, the recommended way to store keys in Flatpaks is for apps to use a local [Keyring](https://wiki.gnome.org/action/show/Projects/GnomeKeyring) encrypted with a key derived from the secret obtained from the [secret portal](https://flatpak.github.io/xdg-desktop-portal/#gdbus-org.freedesktop.portal.Secret). Both [libsecret](https://developer-old.gnome.org/libsecret/0.18/) and [oo7](https://docs.rs/oo7) provide convenience APIs for your app, that automatically use a local Keyring when used inside a Flatpak.
> ![](a28470488f546ffcabb01e24e363fc044d3c67e9.png)
> ![](575d611c36b66d8079cbe90b27359f6f3b4242b2.png)

### Telegrand [↗](https://github.com/melix99/telegrand/)

A Telegram client optimized for the GNOME desktop.

[Marco Melorio](https://matrix.to/#/@melix99:gnome.org) reports

> Summer is officially over, so now it's a good time to wrap up what has been done in Telegrand in the meantime. Here's a quick summary of the nearly 180 commits that happened since the last update:
>
> * Reimplemented chat search in a new panel, which can now also search for global chats
> * Show a list of recently found chats in the new search panel when no query is set
> * Added timestamp to the messages in the chat history
> * Added "sending status" and "edited" indicators to the messages in the chat history, by Marcus Behrendt
> * Added a scroll to bottom button in the chat history, by Marcus Behrendt
> * Show mini-thumbnails for media messages in the chat list, by Marcus Behrendt
> * Added sending status indicator for last messages in the chat list, by Marcus Behrendt
> * Added the ability to mark chats as read or unread in the chat list, by Marcus Behrendt
> * Use new libadwaita widgets like AdwEntryRow and AdwMessageDialog, by Marcus Behrendt
> * Show when a chat is from a deleted user account, by Carlod
>
> More really exciting things are in the works, so stay tuned for the next updates!
> ![](156c2584fbc29db65d507695add0dc9ef1e6ba25.png)
> ![](0fb8a4e8204612edcfccf1934927557585375216.png)
> ![](adf9bd1b6e02ee808b6254750b1dfe599d32e0f0.png)

### Gradience [↗](https://github.com/GradienceTeam/Gradience)

Change the look of Adwaita, with ease.

[0xMRTT](https://matrix.to/#/@0xmrtt:envs.net) announces

> Gradience Team is happy to announce new version of Gradience 0.3.0. This release introduces many new features and improvements.
>
> * Added plugins support, this allows creating plugins for customizing other apps
> * Preset Manager performance are significantly enhanced, presets are downloading much faster and app don't freeze on preset removal
> * Added search to Preset Manager
> * Community presets refactor
> * Preset Manager is attached to the main window
> * Added _Quick Preset Switcher_ back, with it you can switch presets with less clicks
> * Save dialog now shows up when you close app with unsaved preset
> * Currently applied preset now auto-loads on app start-up
> * Toasts now less annoying
> * Added theming warning to Welcome screen
> * Added Mini Welcome screen on update from previous version
> * Added aarch64 builds
> ![](aecd3f14554ffe8f7f32782d477dd7abbb5a8d68.png)
> ![](67a6bc1b6daccafe676fe16f1d6dd6646571b2b8.png)

### Login Manager Settings [↗](https://realmazharhussain.github.io/gdm-settings/)

A settings app for the login manager GDM.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) says

> [Login Manager Settings](https://realmazharhussain.github.io/gdm-settings) v1.0 (stable) has been released.
>
> There are not many changes compared to v1.0-beta.4. One significant change is that it utilizes [blueprint-compiler](https://gitlab.gnome.org/jwestman/blueprint-compiler) v0.4.0 now instead of v0.2.0.
>
> Changes in v1.0-beta.4 and previous beta versions for v1.0 have already been posted to TWIG in [#58](https://thisweek.gnome.org/posts/2022/08/twig-58), [#59](https://thisweek.gnome.org/posts/2022/09/twig-59), and [#61](https://thisweek.gnome.org/posts/2022/09/twig-61).
>
> If you would like see all changes since the previous stable release either go to [GitHub Releases](https://github.com/realmazharhussain/gdm-settings/releases) or [Flathub page](https://flathub.org/apps/details/io.github.realmazharhussain.GdmSettings) for the app.
> ![](ywFouQIyRymfZOXSnLJWgXlB.png)

# Miscellaneous

[jjardon](https://matrix.to/#/@jjardon:matrix.org) announces

> A preview of GNOME OS images for some mobile devices are available (at the moment pine64's PinePhone and PinePhone Pro. They both rely on the GNOME OS infrastructure so we have working atomic updates thanks to ostree! A preview of this work is already available, read more about it here: https://www.codethink.co.uk/articles/2022/gnome-os-mobile/

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
