---
title: "#60 Demo Day"
author: Felix
date: 2022-09-09
tags: ["gnome-shell", "crosswords", "bottles", "gtk"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 02 to September 09.<!--more-->

# Core Apps and Libraries

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[verdre](https://matrix.to/#/@verdre:gnome.org) announces

> There's [a new post about the mobile GNOME Shell](https://blogs.gnome.org/shell-dev/2022/09/09/gnome-shell-on-mobile-an-update/) with updates and lots of videos on the GNOME Shell blog.
> {{< video src="gestures.webm" >}}

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> GTK 4.8.0, the new stable release of GTK is out! Relevant changes since the previous development snapshot are:
> * various bugs related to input handling in GtkTreeView were fixed; app developers are still encouraged to port their code to the [list widgets](https://docs.gtk.org/gtk4/section-list-widget.html)
> * support for more font features has been added in the font selection dialog
> * various accessibility fixes, including the high contrast theme
> * GTK now supports high resolution scroll events and color picking on Windows
> * GTK now generates introspection data on Windows

# Circle Apps and Libraries

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) announces

> GNOME Circle got a larger update on its app criteria and review procedures. We now have a more detailed and updated [checklist](https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/blob/main/AppCriteria.md) that also allows maintainers to check their apps before submitting them to GNOME Circle.
>
> In the about two years of existence of the GNOME Circle initiative, the review process developed more and more into a cooperative process of maintainers, reviewers, and the broader community working together to get good apps into a awesome shape for joining the GNOME Circle. We hope that the new procedures will help us in continuing this process.

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) says

> We finally made a public Matrix channel for GNOME Circle available under [#circle:gnome.org](https://matrix.to/#/#circle:gnome.org). The channel will receive sporadic `@room` notifications for important updates and changes within Circle. But it is also open to general questions and discussions about GNOME Circle. All GNOME Circle maintainers are asked to join the room to receive the notifications.

# Third Party Projects

[James Westman](https://matrix.to/#/@flyingpimonster:matrix.org) announces

> Blueprint 0.4.0 is out! This release changes the compiler to use .typelib rather than XML .gir files, which speeds up compile times. Also, multi-step lookup expressions are now supported.

[mirkobrombin](https://matrix.to/#/@mirkobrombin:matrix.org) says

> The first stable release of [Atoms (1.0.2)](https://flathub.org/apps/details/pm.mirko.Atoms ) is available on Flathub!  Atoms is an application that allows you to access multiple distributions at the same time without actually installing them. Under the hood uses proot, a user-space implementation of chroot and others.  The following distributions are currently supported in Atoms: Ubuntu, Fedora, openSUSE, AlmaLinux, AlpineLinux, Centos, Debian, Gentoo, RockyLinux.
> ![](dNZEWzZpvXMDZJtfZCxTdAAY.png)

[knuxify](https://matrix.to/#/@knuxify:cybre.space) announces

> Ear Tag 0.2.0 has been released!
>
> This release includes:
> * **Support for opening multiple files**. You can now open multiple files at once and select multiple files to edit. Useful for mass-applying tags to albums, etc.
> * **Support for OGG/FLAC cover art** with mutagen's vorbiscomment support.
> * ...as well as some new translations and various other minor fixes and improvements.
>
> You can get it [from Flathub](https://flathub.org/apps/details/app.drey.EarTag) or grab the source code [on the release page](https://github.com/knuxify/eartag/releases/tag/0.2.0).
> ![](sbHfPsWhmHjCtmWzZSizFQNa.png)

[FineFindus](https://matrix.to/#/@finefindus:matrix.org) announces

> I'm announcing the second release of Eyedropper. Eyedropper is an easy-to-use color picker and formatter. Just pick a color from the screen and view it in different formats. This release adds a new history list, the option to hide unwanted formats, as well as support for XYZ and CIELAB models. You can download the latest release from [github page](https://github.com/FineFindus/eyedropper/releases).
> ![](dzXCNcBBzonLHqfPmmczQbmk.png)

### Crosswords [↗](https://gitlab.gnome.org/jrb/crosswords)

A simple Crossword player and Editor.

[jrb](https://matrix.to/#/@jblandford:matrix.org) reports

> GNOME Crosswords 0.3.5 was released, and is available on [flathub](https://beta.flathub.org/apps/details/org.gnome.Crosswords). You can read the release announcement [here](https://blogs.gnome.org/jrb/2022/09/05/crosswords-0-3-5-border-styles/). New in this release:
>
> * Color styling support for borders and corners
> * Multi-character input for rebus puzzles
> * Barred crosswords are fully supported
> * Acrostic puzzle improvements
> * Enumerations are rendered
> * French Language support
> * New game preference to skip completed entries
> * Browse mode to look at puzzles when complete
> * Numerous playability and style improvements, and many bug fixes
>
> If you're interested in playing or writing crosswords, feel free to drop by [#crosswords:gnome.org](https://matrix.to/#/#crosswords:gnome.org) and say hi!
> ![](YaiyERHYbSjeAHPGxnmkhapq.png)
> ![](yKUVHTozMrwZidApMTStIuyz.png)

### Bottles [↗](https://usebottles.com/)

Easily run Windows software on Linux with Bottles!

[Hari Rana (TheEvilSkeleton)](https://matrix.to/#/@theevilskeleton:fedora.im) announces

> Bottles 2022.8.28 was released!
>
> We are introducing Library Mode. Library Mode is a new way to access programs installed in your bottles.
>
> At first your library will appear empty. Open a bottle and choose the program you want. Then, from the context menu, press “Add to My Library” to add the game in the library. Covers are automatically identified and downloaded from [SteamGridDB](https://www.steamgriddb.com/). We are still working on the ability to add covers manually.
>
> The Legacy Tools section has been re-organized with more specific and intuitive names, eliminating the noise that more advanced users (who this section is aimed at) might have encountered.
>
> There are many other changes and improvements in this release. Below are some noteworthy ones, for all others, please refer to the [GitHub organization](https://github.com/bottlesdevs).
>
> * Library mode is now a stable feature and enabled by default
> * Dependencies “copy_file” action now creates the path if not exists (see maintainers docs.)
> * Opening a bottle, a dialog is shown if the runner is not installed
> * The C: drive is now marked as persistend in the Drives section and cannot be edited by the user
> * All dialogs can now be closed pressing Escape
> * The dark mode switcher is now available only for system which doesn’t support the standard
> * Simplification of “Legacy Tools” section, thanks to @Knebergish
> * Minor UI improvements, thanks to @TheEvilSkeleton, @orowith2os, @marhkb
> * Fix a bug in the template system, was trying to unpack a partial one
> * Fix a bug in setting overrides for old-structured program entries
> * Fix a bug in vmtouch management
> * Fix a bug in the WineCommand interface which was causing a crash if the executable path is not accessible
> * Fix a bug in the Steam Manager, was generating wrong shortcuts when the program name has spaces in it
> * Fix long names in library mode
> * Fix a bug in bottle creation, sometimes it created a symlinks loop in the userdir
> * Fix a bug in the crash dialog, the similarity check was set too high resulting in no similar reports
> ![](a0cf050709eafa85d1838e8925a1fd669584f4a3.png)

# GNOME Shell Extensions

[Simon Schneegans](https://matrix.to/#/@schneegans:matrix.org) reports

> Version 20 of the [Burn-My-Windows](https://extensions.gnome.org/extension/4679/burn-my-windows/) GNOME Shell extension has been released. It includes four new retro-styled pixel effects! One of them is inspired by the legendary screen transitions of the original Doom video game. Watch the full trailer https://youtu.be/MLM7iFdGqvM
> {{< video src="qdorviUkZBzAAiXFuZPTqNhv.mp4" >}}

[Romain](https://matrix.to/#/@romainvigier:matrix.org) says

> A new version of [Night Theme Switcher](https://nightthemeswitcher.romainvigier.fr/), my GNOME Shell extension for automatically switching the desktop’s color scheme at sunset and sunrise, has been released! It brings support with GNOME 43 and integrates with its new Dark Mode quick setting, and has new Czech, Greek and Japanese translations thanks to the community.
>
> [Read the full release notes on the repository](https://gitlab.com/rmnvgr/nightthemeswitcher-gnome-shell-extension/-/releases/66) and [install it from extensions.gnome.org](https://extensions.gnome.org/extension/2236/night-theme-switcher/).
> ![](kQqKJgemSCCrnksqUMyUIGOs.png)

# Miscellaneous

[Felix](https://matrix.to/#/@felix:haecker.io) says

> The design of the [This Week in GNOME](https://thisweek.gnome.org) website got updated to match the libadwaita `card` styling.
>
> If you read TWIG via an RSS reader, I recommend you take a look at thisweek.gnome.org. I'm really happy with the updated look!

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!