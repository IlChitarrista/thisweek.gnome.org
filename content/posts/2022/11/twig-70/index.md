---
title: "#70 Useful Progressbars"
author: Felix
date: 2022-11-18
tags: ["bottles", "gdm-settings", "money", "boatswain", "gnome-software"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from November 11 to November 18.<!--more-->

# Core Apps and Libraries

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Milan Crha has [added progress reporting](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/1552) to rpm-ostree downloads in GNOME Software, so updates in Silverblue should now have a more useful progress bar

# Third Party Projects

[Dušan Simić](https://matrix.to/#/@dsnsmc:matrix.org) reports

> Dynamic Wallpaper [v0.1.0](https://github.com/dusansimic/dynamic-wallpaper/releases/tag/0.1.0) has landed! It's a simple app that helps you make a dynamic wallpaper that changes if you use light or dark mode in GNOME (the type of wallpaper has been released with GNOME 42).
> 
> Thanks to over 20 contributors, this new release is finally ready. Version 0.1.0 brings a new clean design that nicely blends into rest of modern GNOME apps. It lets you give a name to your new wallpaper, select which file would be used for light mode and which for dark mode and by clicking the "Create" button, you're ready to go 🚀.
> 
> I've first released this app in April this year but have not been able to bring it to a point that I'm satisfied enough with the ui that I'd be happy to share it here. Thanks to Hacktoberfest, there have been many contributions to the project an so far it has been translated to 13 languages 😁.
> 
> The app is so far available on FlatHub and AUR.
> ![](NdQFLzPFooFRdVEyHeaaAHEI.png)

[Paulo](https://matrix.to/#/@raggesilver:matrix.org) says

> [Black Box 0.12.2](https://flathub.org/apps/details/com.raggesilver.BlackBox) is out. This patch fixes some annoying bugs related to selecting and pasting text. It has been a while since the last release, but you can expect more updates in the following months, starting with customizable keyboard shortcuts.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) says

> [nautilus-code](https://github.com/realmazharhussain/nautilus-code) [v0.5](https://github.com/realmazharhussain/nautilus-code/releases/tag/v0.5) was released.
> 
> `nautilus-code` is a Files (nautilus) extension that adds right-click menu options to open current/selected folder in a code editor or IDE e.g. VSCode, GNOME Builder, etc. Click [here](https://github.com/realmazharhussain/nautilus-code#currently-supported) to see a full list of supported code editors and IDEs or [here]() to request support for a code editor or IDE.
> 
> This version
> * adds support for Nautilus 43
> * fixes an issue where folder paths with whitespaces in them were not handled properly
> 
> Unfortunately, to add support for Nautilus 43, support for 42 and earlier had to be dropped. So, an accompanying legacy version ([v.5.0.legacy](https://github.com/realmazharhussain/nautilus-code/releases/tag/legacy%2Fv0.5)) which supports Nautilus 42 and earlier, was also released.

[Hari Rana (TheEvilSkeleton)](https://matrix.to/#/@theevilskeleton:fedora.im) announces

> Introducing Upscaler, an application that allows you to upscale (increase the resolution) and enhance images! Upscaler is a front-end for [Real-ESRGAN ncnn Vulkan](https://github.com/xinntao/Real-ESRGAN-ncnn-vulkan) and uses its algorithms to upscale and enhance images. I'd like to thank them for making this amazing project!
> 
> Upscaler 1.0.0 was recently released, and it's available on Flathub! https://flathub.org/apps/details/io.gitlab.theevilskeleton.Upscaler
> ![](ca5d3c9895f137febda2bba7698ae274b4866131.png)

### Money [↗](https://flathub.org/apps/details/org.nickvision.money)

A personal finance manager.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Money [V2022.11.0](https://github.com/nlogozzo/NickvisionMoney/releases/tag/2022.11.0) is here! We have spent the past few weeks implementing many new user-requested features, greatly improving the experience of Money, and fixing many bugs while polishing the release. I couldn't be more thankful to all the contributors who have provided translations, feedback, and endless testing and patience (especially @zothma and @fsobolev). Money wouldn't be how it is today without all of your help :)
> 
> Here's the changelog:
> * Introducing Groups: Add groups to an account and associate transactions with groups for a more precise finance management system
> * Money will automatically obtain the user's currency symbol, money format, and date format from their locale
> * Money will remember up to 3 recently opened accounts for quick and easy access
> * Fixed an issue where a new account was not created if overwriting an old one
> * Added translation support
> ![](LWLKDvpspwmuakuuFRdbpoIM.png)
> ![](PKGCDlwjlkFnOEsYYbTNevqD.png)

### Login Manager Settings [↗](https://realmazharhussain.github.io/gdm-settings/)

A settings app for the login manager GDM.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) says

> [Login Manager Settings](https://realmazharhussain.github.io/gdm-settings) [v2.beta.1](https://github.com/realmazharhussain/gdm-settings/releases/tag/v2.beta.1) was released.
> 
> Bugs Fixed
> * Developer name in about window not being translated
> * Could not change logo image sometimes
> * "Apply Current Display Settings" feature not working on some Ubuntu-based systems (proper fix)
> 
> Updated
> * Screenshots
> * Translations
> ![](EvUdqgyUoiMjTqtRSHKjiYBD.png)

### Bottles [↗](https://usebottles.com/)

Easily run Windows software on Linux with Bottles!

[Hari Rana (TheEvilSkeleton)](https://matrix.to/#/@theevilskeleton:fedora.im) announces

> Bottles 2022.11.14 was released!
> 
> In this update, the details view has been completely redesigned to improve the user friendliness of Bottles. The sidebar has been removed and the pages have been moved into the details view itself.
> 
> The option to run an executable is much more prevalent and launch options are neatly placed in their own dropdown next to it. Options to add and install programs have been moved after the programs list.
> 
> Settings page has also received an overhaul. Similar settings have been rearranged into groups, making it easier to navigate and find options.
> 
> A lot of quality of life improvements have been made to Bottles to improve usability. “States” have been renamed to “Snapshots”, several dialogs have been rephrased, “Documentation” has been renamed to “Help”, "kill" was renamed to "force stop" to avoid using violent terms, several mnemonics were added, along with a few more improvements that bring Bottles closer to GNOME Human Interface Guidelines.
> 
> For more information about the new update, check out our [release page](https://usebottles.com/blog/release-2022.11.14/)!
> ![](9c1d291165a4486909fc484938bc4cc0a24cfbbf.png)
> ![](7137864f21b07e02960844913b217d4452682116.png)
> ![](8726d7eb12eddced2ddec67308324bdc83b55c20.png)

### Boatswain [↗](https://gitlab.gnome.org/World/boatswain/)

A guiding hand when navigating through streams.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) reports

> Boatswain 0.2.2 is out! This new release comes with drag n' drop support to move button actions around, as well as a bunch of interface improvements, more icons to pick, better handling of light background colors on buttons, and more! 
> 
> This latest release is available on Flathub. You can read more at https://feaneron.com/2022/11/18/boatswain-0-2-2-is-out/
> ![](df4c204dd362b36e319937a6f6c014d5f70a0937.png)
> {{< youtube PovvlgxctRo >}}

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

