---
title: "#69 Zapping Through Videos"
author: Felix
date: 2022-11-11
tags: ["newsflash", "tagger", "gaphor", "komikku", "glib", "crosswords", "tubeconverter"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from November 04 to November 11.<!--more-->

# Circle Apps and Libraries

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) says

> This week, Zap joined GNOME Circle. Zap allows you to play all your favorite sounds from a sound board to make your livestreams more entertaining. Congratulations!
> ![](98958bb87c64448a727be4ae36b812914f924589.png)

### NewsFlash feed reader [↗](https://gitlab.com/news-flash/news_flash_gtk)

Follow your favorite blogs & news sites.

[Jan Lukas](https://matrix.to/#/@jangernert:matrix.org) says

> NewsFlash 2.1.3 was released this week. It mostly features improvements for Google Reader API based implementations e.g. FreshRSS & Inoreader. For existing users of these implementations I suggest to reset your account in NewsFlash and log back in for the best experience.

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Dan Yeaw](https://matrix.to/#/@Yeaw:matrix.org) says

> [Gaphor](https://gaphor.org/), the simple modeling tool, recently completed some modernization of our UI using libadwaita. Our next release will feature a new about dialog, message dialogs, and tab view. ✨
> ![](hMcSJCBmXVHcUfqErcnAhMot.png)
> ![](kMjupqzSaAFxtdmXbAaWEUoV.png)
> ![](UIABAhUiyMRJlUiNbslGnNxy.png)

# Core Apps and Libraries

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Patrick Monnerat has fixed an [insidious bug in `glib-mkenums`](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3043) where private enum members wouldn’t be used for calculating subsequent enum values. This is their first contribution, thank you Patrick!

# Third Party Projects

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) says

> Loupe gained support for showing images in the correct orientation based on metadata, as well as manually rotating images via buttons and touch gestures.
> {{< video src="83d1332ba0ed401fa8ee10c4b432c9814d82819b.webm" >}}


### OS-Installer

A generic third-party OS-Installer that can be customized by distributions.

[Peter Eisenmann](https://matrix.to/#/@p3732:matrix.org) says

> OS-installer has reached version 0.3. Here are the bigger changes in the half a year since 0.2:
> 
> * Lists scroll more intuitively and look nicer
> * Optional welcome page with distro-customizable text added
> * Summary page to confirm selection added
> * Optional feature page added with which distributions can provide additional options
> * Added and updated translations (Croatian, Dutch, Estonian, French, Georgian, German, Italian, Occitan, Polish, Portuguese (Brazil), Spanish, Swedish, Ukrainian)
> * Keyboard page got overhauled to be simpler
> * Many new config options for distributions, e.g. skipping the user and timezone page when using gnome-initial-setup
> 
> The first distributions started to experiment using OS-installer, which is great to see :)
> Also, I want to thank all the translators for their great contributions! ♥️
> {{< video src="JQepaRRJtOkbaGfaGKVTmnGq.webm" >}}

### Tube Converter [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

An easy-to-use video downloader (yt-dlp frontend).

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Tube Converter [V2022.11.0](https://github.com/nlogozzo/NickvisionTubeConverter/releases/tag/2022.11.0) has been released! In this release we introduced support for translations (with some language translations already available) and fixed some other issues.
> Here's a full changelog:
> 
> * Fixed an issue where videos could not be downloaded on ARM64
> * Fixed an issue where 'Best' and 'Good' would download the same video quality
> * Improved design of the Logs dialog
> ![](ApNNswGXEDDtDPdyTsiZhhZB.png)

### Tagger [↗](https://flathub.org/apps/details/org.nickvision.tagger)

An easy-to-use music tag (metadata) editor.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Tagger [V2022.11.0](https://github.com/nlogozzo/NickvisionTagger/releases/tag/2022.11.0) has been released! In this release we introduced support for translations (with some language translations already available) and fixed some other issues.
> Here's a full changelog:
> * Fixed sizing issue for Advanced Search Information dialog
> ![](YANfmqOnZuvSXxqHKOAOtnKz.png)

### Komikku [↗](https://gitlab.com/valos/Komikku)

A manga reader for GNOME.

[Valéry Febvre (valos)](https://matrix.to/#/@valos:matrix.org) says

> Komikku, the manga reader (but not only).
> 
> Komikku is now in 1.4.0! Since 1.0.0, many exciting changes have been made. 
> 
> New Features
> * [Explorer] Added a Global search
> * [Servers] Added a Local server that allows to read comics in CBZ or CBR formats
> * [Servers] Added Comic Book Plus [EN]
> 
> Improvements
> * [Library] Improved loading: a waiting page with a progress bar has been added
> * [Library] Landscape covers are now converted to portrait
> * [Downloader] Improved download speed
> * [Reader] Improved rendering of Page numbering
> * [Reader] Improved Webtoon reading mode
> 
> Bug fixes
> * [Reader] Fixed detection of 'read' chapters
> ![](cbvdEMkjAhjcSSyWYgykESFJ.png)

### Crosswords [↗](https://gitlab.gnome.org/jrb/crosswords)

A simple Crossword player and Editor.

[jrb](https://matrix.to/#/@jblandford:matrix.org) announces

> GNOME Crosswords 0.3.5 was released, and is available on [flathub](https://flathub.org/apps/details/org.gnome.Crosswords). You can read the release announcement [here](https://blogs.gnome.org/jrb/). New in this release:
> 
> * Over 100 new puzzles available!
> * New Adaptive layout to handle a wide variety of screen sizes
> * New setting: switch-on-move
> * Window size restoration on restart
> * Define mime types for ipuz/jpz/puz files and loads jpz/puz files from the command line
> * .puz convertor imports circles and rebus puzzles
> * Support more complex enumeration use-cases
> * Improvements to cell shape contrast and color
> * HTML support for clues and metadata
> * Better display and placement of intro/notes fields
> {{< video src="FKXuoHbcsIzELTryLZOOZogT.webm" >}}

# Documentation

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> gi-docgen 2022.2, the new version of the documentation tool used to generate C API references from GObject introspection data, is now [available on PyPI](https://pypi.org/project/gi-docgen/2022.2/). New in this release:
> 
> * emblems are used to visually identify deprecated and unstable symbols
> * improved styling and responsiveness
> * improved the description of autogenerated descriptions
> * added the "serve" command, which will generate the reference and publish it using a local HTTP server

# GNOME Shell Extensions

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) says

> I have released the version 15 of Auto Activities. An extension that shows activities overview when there are no windows in the workspace.
> 
> I am pleased to announce this release which brings some news. The first is that I was talking to the owner of the original project, who at the moment was no longer interested in maintaining it for personal reasons. I asked him to transfer the repository to me, and now I am the new maintainer.
> 
> Changelog:
> * Add metadata support for GNOME 43
> * Modernize preference dialog using Libadwaita
> * Some bug fixes
> 
> [Follow the development](https://github.com/CleoMenezesJr/auto-activities/)
> [Get it on GNOME Extensions](https://extensions.gnome.org/extension/5500/auto-activities/)
> ![](ogMhjwrJBnNdpZROyDTMDZkY.png)
> {{< video src="LiiMeSgcKdAkATlagcQBBmZI.mp4" >}}

# Miscellaneous

[Thib](https://matrix.to/#/@thib:ergaster.org) says

> GNOME has been around the Fediverse for a while and we’re still active there. With recent events you may want to follow us on https://floss.social/@gnome

[niko](https://matrix.to/#/@nikodunk:fedora.im) reports

> Documented Gnome Shell with mobile patches ([link](https://blogs.gnome.org/shell-dev/2022/09/09/gnome-shell-on-mobile-an-update/)) and various Gnome 43 adaptive apps on a OnePlus 6 running PostmarketOS
> {{< youtube wOmRMg546UY >}}

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
