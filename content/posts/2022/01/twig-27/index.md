---
title: "#27 Borderless"
author: Felix
date: 2022-01-21
tags: ["gjs", "gnome-shell", "gnome-software", "gtk-rs", "webkitgtk", "phosh", "gnome-control-center"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from January 14 to January 21.<!--more-->

# Core Apps and Libraries

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Sam Hewitt](https://matrix.to/#/@snwh:gnome.org) announces

> The desktop Shell is getting a big visual refresh for GNOME 42! In addition to a palette update, elements throughout the shell have been given a rounder appearance. Panel menus have also gotten a major redesign, with a new style for sub-menus. The on-screen keyboard is getting big improvements to key visual feedback and word suggestions. Not to mention a tonne of other smaller fixes.
> ![](fe6994f7144b6452b385a0700d065ca9c5de95a7.png)
> ![](aef9fe5560f99b89709ca7d079be591dac2b2b03.png)
> ![](bc152f2225915e72a42026af17fef566b4b6c57a.png)
> ![](c7aa5be8c7a4746a41a512d971a7f16563d9f879.png)
> ![](3f68031263175f70ac32cf8cf0fd0e1028422e27.png)

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) says

> This week I ported the Online Accounts panel to GTK4, and landed redesigns of the Display, and the Applications panels, in Settings.
> ![](ee0152b5b4d0689dd9f23f1861681fe81b4f33c2.png)
> ![](596a42f50bc9726ef6f854dc51df901746bf78e5.png)
> ![](ca45e394a1c2f74516dda10c29a13ebc92047e88.png)
> ![](09f1efbfdf647213336a871fec80621b8ff6991e.png)
> ![](b65216c41aee0da8f555c3c6541544cd622ed594.png)

### WebKitGTK [↗](https://webkitgtk.org/)

GTK port of the WebKit rendering engine.

[adrian](https://matrix.to/#/@aperez:igalia.com) reports

> We have released [WebKitGTK 2.34.4](https://webkitgtk.org/2022/01/21/webkitgtk2.34.4-released.html), which includes a number of security fixes. While the release notes are spare, it is worth mentioning that it includes an important patch for the [Safari IndexedDB leaks vulnerability](https://safarileaks.com/) which has been recently disclosed.

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> Milan Crha has [improved the display of permissions](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/1161) needed by Flatseal in GNOME Software

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) announces

> In GJS this week:
> * GJS upgraded its underlying JS engine to SpiderMonkey 91, bringing lots of modern JS conveniences. This upgrade was done by Evan Welsh, Chun-wei Fan, and myself. Here's a sampler of what we get:
>   - `#privateFields` and `#methods()`
>   - The `??=`, `&&=`, and `||=` operators
>   - The `at()` method for arrays and strings, allowing indexing with negative numbers
>   - `Promise.any()`
>   - Error causes
>   - WeakRefs
>   - More locale-aware formatting features
> * Evan also added a standards-compliant `setTimeout()` and `setInterval()` to GJS, these can now be used as in web browsers, while still integrating with GLib's main loop.
> * Evan _also_ added overrides for `GObject.Object.new()` and GObject.Object.new_with_properties() to make them work with properties.
> * Previously, pressing Ctrl+D at the debugger prompt would print an error message instead of quitting. I fixed this.
> * I added column numbers to SyntaxError messages, to go along with the line number.
> * Yet more thanks to Evan for various other contributions.

# Circle Apps and Libraries

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> After months working on gtk-rs bindings, we finally made a new release! 🎉 The release comes with supporting various new APIs like
> * BuilderScope support in gtk4-rs, it means you can finally set function names on the UI file and define the callback in your Rust code
> * gdk3 wayland API bindings
> * A release of almost all the gir based Rust bindings in [World/Rust](https://gitlab.gnome.org/World/Rust)
> * Brand new GStreamer plugin that allows you to "stream" your pipeline to a GdkPaintable
> You can find more details on the release [blog post](https://gtk-rs.org/blog/2022/01/16/new-release.html) and on gstreamer bindings/plugins release [blog post](https://gstreamer.freedesktop.org/news/#2022-01-16T11:00:00Z)

# Third Party Projects

[Romain](https://matrix.to/#/@romainvigier:matrix.org) reports

> I wrote [UI Shooter](https://gitlab.com/rmnvgr/uishooter), a tool to make screenshots of GTK4 widgets from a UI file.
> 
> It allows loading CSS, resources and translations, setting scale and dark color scheme, and using libadwaita’s stylesheet. It’s mainly intended to be used in headless environments, so I provide a [container image](https://gitlab.com/rmnvgr/uishooter#headless) running the Weston compositor that can be used as is or extended at will.
> 
> I use it in [Metadata Cleaner](https://metadatacleaner.romainvigier.fr/)’s CI pipeline to [automatically take screenshots](https://gitlab.com/rmnvgr/metadata-cleaner/-/tree/HEAD/screenshots) of various widgets for the help pages when a translation is added or updated.
> ![](YuWNUmIyrsdjrvLhrYyBUzpY.png)

[Doomsdayrs](https://matrix.to/#/@doomsdayrs:matrix.org) announces

> Announcing, `gtk-kt` https://gitlab.com/gtk-kt/gtk-kt
> 
> `gtk-kt` is a Kotlin binding of the GTK API. Allowing developers who are familiar with Java / Kotlin to easily write a GTK application. 
> 
> It is also an easy safe way for new programmers to start creating GTK applications, only needing 10 lines & 154 characters to create a single window. Compare that to C which takes 26 lines and 602 characters, that is a whopping 75% less characters to make a simple window, imagine that for larger projects with more complex components.
> 
> It has neared its completion stages, with 97.49% of GTK classes wrapped in Kotlin, leading me to release the first alphas to https://maven.org .
> 
> Also being developed/planned is libadwaita (https://gitlab.com/gtk-kt/libadwaita-kt) support and xdg-portal (https://gitlab.com/gtk-kt/libportal-kt) support.
> ![](rDSOIbfxwjFtlmGLHNAuzpUP.png)

[Aaron Erhardt](https://matrix.to/#/@admin:matrix.aaron-erhardt.de) reports

> Relm4 0.4 was released this week with many improvements!
> The highlights include many macro improvements, type-safe actions, more flexibility at runtime and updated dependencies.
> The full release announcement can be found [here](https://aaronerhardt.github.io/blog/posts/announcing_relm4_v0.4/).
> ![](MhbZJvvNWrtORWPIGzuXBLJl.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@guido.gunther:talk.puri.sm) says

> phosh got a VPN quicksetting last week that toggles the last used VPN connection. On the compositor side ([phoc](https://gitlab.gnome.org/World/Phosh/phoc)) we updated to a newer [wlroots](https://gitlab.freedesktop.org/wlroots/wlroots) which allowed us to enable the xdg-foreign and viewporter wayland protocols (which help flatpaks to position file dialogs better and some video workloads respecitively).
> {{< video src="JsqFihQfiKhUwNYcrZFzIXJp.mp4" >}}

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
