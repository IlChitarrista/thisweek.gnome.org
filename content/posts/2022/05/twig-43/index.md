---
title: "#43 Foundation News"
author: Felix
date: 2022-05-13
tags: ["gtk", "workbench", "libadwaita", "gnome-software"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from May 06 to May 13.<!--more-->

# GNOME Foundation

[Thib](https://matrix.to/#/@thib:ergaster.org) reports

> The GNOME Foundation's Board of Directors would like to inform the members of the GNOME Foundation that regrettably, Alberto Ruiz submitted his resignation from the Board of Directors for a combination of work and personal reasons.
> 
> We are pleased to announce that we have appointed Martín Abente Lahaye to serve on the Board as per the bylaws. Martín is an active community member, the author of Flatseal and Portfolio, and has experience promoting and deploying FOSS in educational and impact settings through his previous work with One Laptop Per Child Paraguay, Sugar Labs and Endless. We believe Martín is a great asset to help us implementing our roadmap and are excited to start working together.
> 
> Full details in [this Discourse post](https://discourse.gnome.org/t/board-of-directors-resignation-appointment/9833). Welcome onboard Martín Abente Lahaye !

[Thib](https://matrix.to/#/@thib:ergaster.org) says

> The GNOME Foundation's Board of Directors has been working on a high-level roadmap to give the Foundation a clear direction. Rob posted [the three initiatives](https://discourse.gnome.org/t/evolving-a-strategy-for-2022-and-beyond/9759) that will keep us busy. Full details in the post, but the strategy is to:
> 
> 1. Make the newcomers initiative more sustainable, with paid contributors polishing the documentation and welcoming people on our platform
> 2. Make GNOME an appealing platform to develop for, by making Flathub support payments
> 3. Make GNOME a local-first platform to empower individuals by reducing their dependency on the cloud and more generally the Internet
> 
> In short: funnel more people to contribute to the GNOME project, make them able to earn money from the skills they developed, and allow them to create apps that have a strong impact on the world.

[Thib](https://matrix.to/#/@thib:ergaster.org) reports

> The GNOME Foundation's Board of Directors [has proposed an amendment to the bylaws of the Foundation](https://discourse.gnome.org/t/updated-bylaw-amendment-allow-non-members-to-stand-for-election/9761) to allow a limited number of non-members to run for the Board of Directors if they are backed by enough Foundation members.
> 
> The rationale behind this amendment is that our roadmap is about getting more people from outside our community to understand and support the GNOME Project. The initiatives we work on are ambitious and need to be funded properly to have the full impact we want them to have. Fundraising is not something most of the current board and Foundation members are very experienced with.
> 
> Given the initial feedback and concerns some members have raised, the Board has decided to submit this amendment to full vote. If you are a member of the Foundation, you should have received instructions to vote. If you haven't, please reach out to Andrea Veri.
> 
> In short: we want to open the Board of Directors to people with different skills than the one we have today, to maximise our chances to make GNOME useful for more people, with proper safeguards so it's still representative of the Foundation members.

# Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> Libadwaita has gained [`AdwPropertyAnimationTarget`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.PropertyAnimationTarget.html) for animating object properties in addition to `AdwCallbackAnimationTarget`:
> 
> ```c
> AdwAnimationTarget *target = adw_property_animation_target_new (G_OBJECT (widget), "opacity");
> g_autoptr (AdwAnimation) animation = adw_timed_animation_new (widget, 0, 1, 250, target);
> 
> adw_animation_play (animation);
> ```

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) reports

> GTK 4 has seen a few performance improvements to ListView and ColumnView scrolling. Additionally, a fix for unstable FPS after opening a popup menu has been finally merged. Both of these are already available on the gnome-nightly Flatpak runtime and should be included in GTK 4.6.4.

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> Basic webapps support has just landed in gnome-software, the culmination of a project by Phaedrus Leeds

# Third Party Projects

[flxzt](https://matrix.to/#/@followhollows:matrix.org) says

> Last week I released Rnote v0.5 . It now features an additional eraser mode, new shape types such as ellipses from foci and bezier curves. Additionally, the undo / redo system was overhauled, handwriting latency reduced and you can now enable automatic saving. There were also some UI tweaks, stability improvements and **a lot** of internal code refactoring
> ![](rnote.png)

[Fina](https://matrix.to/#/@felinira:matrix.org) announces

> I released the first version of Warp - A one-to-one file transfer application. Warp allows you to securely send files to each other via the Internet or local network by exchanging a word-based code.
> 
> To try it out visit https://flathub.org/apps/details/app.drey.Warp
> ![](VrwovVKerazbWKMzvdFaxWdM.png)

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) reports

> [Blueprint](https://jwestman.pages.gitlab.gnome.org/blueprint-compiler/) support has landed in Workbench. This new markup language from James Westman not only provides a much nicer experience for handwriting GTK, but also comes with a language server which will allow us to present educational hints. [GNOME Builder](https://wiki.gnome.org/Apps/Builder) already support Blueprint language server – definitely worth checking out.
> {{< video src="ElKfDGmTcMaFONsCzdJGAuTj.webm" >}}

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) announces

> lwildberg  made great progress on Vala support in Workbench. The sub-process can now compile, run, preview and update !
> We are making the architecture easy to use for additional language support. Stay tuned.
> {{< video src="pEDUTsNzLYnqVqSkMhQiVQgy.webm" >}}

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

