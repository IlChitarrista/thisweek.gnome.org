---
title: "#42 Numerous Emojis"
author: Felix
date: 2022-05-06
tags: ["workbench", "gnome-characters", "apostrophe"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from April 29 to May 06.<!--more-->

# Core Apps and Libraries

### Characters [↗](https://gitlab.gnome.org/GNOME/gnome-characters)

 A simple utility application to find and insert unusual characters.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> Characters now [supports](https://gitlab.gnome.org/GNOME/gnome-characters/-/merge_requests/73) composite emoji such as people with skin tone and gender modifiers, or country flags. It also displays emoji in the proper order instead of sorted by their codepoints.
> ![](dace8f2543f62093b85f40de1daafb119b485bd2.png)
> ![](cdf279922f02ae3f6493ba4c78817d1dfda33ca8.png)

# Circle Apps and Libraries

### Apostrophe [↗](https://gitlab.gnome.org/World/apostrophe)

A distraction free Markdown editor.

[Manu](https://matrix.to/#/@somas95:gnome.org) announces

> I've released a new version of Apostrophe with small bugfixes and improvements and updated translations. I'm also working on a GTK4 port, so I don't plan on releasing more features until that is complete.

# Third Party Projects

[ranfdev](https://matrix.to/#/@ranfdev:matrix.org) announces

> I'm releasing Geopard, a simple, colorful gemini client. Browse the space and reach hundreds of gemini capsules! It let's you read stories, download files, play games... https://flathub.org/apps/details/com.ranfdev.Geopard
> ![](pEbgEBNXbpnAHLPmyrQNJVBG.png)

[Maximiliano](https://matrix.to/#/@msandova:gnome.org) announces

> Introducing Citations, your new manager for BibTeX references, this is a (for now) very small app to manage your bibliography and get easy to copy cites for LaTeX and other formats. The app is in heavy development and Iin the next weeks I will improve the performance and add more features before  its stable release.
> 
> You can get Citations at [flathub](https://flathub.org/apps/details/org.gnome.World.Citations)
> ![](2e0759799761a66a5bbc965dfdf7703f04c95e88.png)

[Peter Eisenmann](https://matrix.to/#/@p3732:matrix.org) reports

> Announcing [OS-Installer](https://gitlab.gnome.org/p3732/os-installer): A generic OS-Installer that can be customized by distributions to install an OS. Translations welcome!
> {{< video src="vIthkhSXMDVDRMfyRczahmjU.webm" >}}

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) says

> During Linux App Summit Julian Sparber prototyped with docked GTK Inspector in [Workbench](https://github.com/sonnyp/Workbench). We exchanged lots of interesting ideas around developer tooling and closing the gap with Web inspectors.
> ![](KzSUjgwTNeIIIKajJwqHGoSz.png)

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) reports

> A new version of Workbench is out - featuring
> 
> **A brand new Library of examples** - including
> 
> * WebSocket client
> * Toasts
> * Application Window
> * Desktop notifications
> 
> Help needed and contributions welcome for new examples and demos.
> 
> **Platform tools** Adwaita Demo, GTK Demo, GTK Widget Factory and GTK Icon Browser for the current platform version are now available in the main menu.
> 
> **But also**
> 
> * The Console can be collapsed by resizing it
> * Prevent system.exit from quitting Workbench
> * Allow calling GObject.registerClass multiple times
> * Prevent crash when using non GtkBuildable objects
> * Allow using DBus and Gio.Application
> * Allow using network
> * Enable GtkWindow objects preview
> * Design improvements
> 
> [Get it on Flathub](https://beta.flathub.org/apps/details/re.sonny.Workbench)
> ![](YdSNmmCNEGFDIuXjCdNjjcqa.png)

# Miscellaneous

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) says

> Last weekend, many of us were at the [Linux App Summit](https://linuxappsummit.org/). It was fantastic! Thank you KDE, GNOME, organizers, volunteers, sponsors and everyone involved. Make sure to check out the talks, here are some of my favorites:
> 
> * [Flathub: now and next](https://www.youtube.com/watch?v=CBPefa0Ckq8&t=3300s) by Jorge Castro and Robert McQueen
> * [Integrating Progressive Web Apps in GNOME](https://www.youtube.com/watch?v=CBPefa0Ckq8&t=26070s) by Phaedrus Leeds
> * [Funding Flatpak Development on Open Collective](https://www.youtube.com/watch?v=CBPefa0Ckq8&t=29580s) by Phaedrus Leeds
> * [Fractal-next: The journey of rewriting a GTK Matrix client](https://www.youtube.com/watch?v=CBPefa0Ckq8&t=9105s) by Julian Sparber
> * [Bottles](https://www.youtube.com/watch?v=HxM15UOVmyA&t=9240s) by Francesco Masala, Mirko Brombin and Pietro di Caprio
> ![](jCWzSlddsQtopzBeXagrXESF.jpg)

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) announces

> I have registered the domain `drey.app` to make it publicly available for app ids. If you want to use `app.drey.<YourApp>` please [register it first](https://gitlab.gnome.org/sophie-h/app.drey).
> 
> The idea behind proving this domain is that `gnome.org` is [reserved for core apps](https://wiki.gnome.org/Foundation/SoftwarePolicy) and using app ids derived from repository hosting can be long, short-lived, and tied to one author. The name `app.drey` derives from the term for a [squirrel's nest](https://en.wikipedia.org/wiki/Drey).

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

